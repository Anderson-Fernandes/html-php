function alterarSala(sala, pass, priv, passProf){

	if(passProf.length <= 0){
		alert("Preencha a senha para alterar");
		return;
	}

	$.ajax({
		url:'salas/require/php/alterarSala.php',
		method:'POST',
		data:{sala:sala, senha:pass, priv:priv, confirm:passProf},
		success: function (data){
			if(data){
				alert(data);
			}else{
				location.reload();
			}
		}
	});
}

function excluirSala(sala,pass){
	if(confirm("Você realmente quer deleta a sala?")){
		if(confirm("A sala será apagada permanentemente")){

			if(pass.length <= 0){
				alert("Preencha a senha para excluir");
				return;
			}

			$.ajax({
				url:'salas/require/php/excluirSala.php',
				method:'POST',
				data:{sala:sala, confirm:pass},
				success: function (data){
					if(data){
						alert(data);
					}else{
						location.href="index.php";
					}
				}
			});
		}
	}

}