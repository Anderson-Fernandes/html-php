/**
 * Created by Anderson Fernandes on 05/05/2016.
 */

function porcentagemDe(num1, num2){
    num1=num1 * 100;
    var porcentagem = num1/num2;
    return porcentagem;
}



function getAvaliacaoImg(qtdAlunos ,avalicao){
    
    var porcentagem = porcentagemDe(avalicao,qtdAlunos);
    var imagem = document.getElementById("reputacaoDaSala").style;
    var caminhoDaImagem = "url('salas/require/img/rostos/";

    if(qtdAlunos >= 5) {
        if (porcentagem <= 20) {
            caminhoDaImagem += "horrivel";
        } else if (porcentagem <= 40) {
            caminhoDaImagem += "ruim";
        } else if (porcentagem <= 60) {
            caminhoDaImagem += "normal";
        } else if (porcentagem <= 80) {
            caminhoDaImagem += "bom";
        } else if (porcentagem > 80) {
            caminhoDaImagem += "otimo";
        }
    }else{
        caminhoDaImagem += "bom";
    }

    imagem.backgroundImage = caminhoDaImagem + ".svg')";
}

function writeAvaliacaoBalao(avaliacao, qtdAlunos){
    $('#reputacaoDaSala').balloon({ position: "bottom",
        contents: '<h2>'+ avaliacao + ' / ' + qtdAlunos +'</h2>'});
}


