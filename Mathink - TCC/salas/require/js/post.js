/**
 * Created by Anderson Fernandes on 05/05/2016.
 */



$(document).ready(function() {


    $("#mensagemPost").keydown(function (e) {
        if (e.keyCode == 27) {
            if (confirm("Você quer apaga a mensagem?")) {
                $("#tituloPost").fadeOut(function () {
                    $("#tituloPost").val("");
                    $("#mensagemPost").val("");
                });
                $("#mensagemPost").select().blur();
            }
        }
    });

    $("#tituloPost").keydown(function (e) {
        // 27 = ESC
        if (e.keyCode == 27) {
            if (confirm("Você quer apaga a mensagem?")) {
                $("#tituloPost").fadeOut(function () {
                    $("#tituloPost").val("");
                    $("#mensagemPost").val("");
                });
                $("#tituloPost").select().blur();
            }
        }
    });


    window.onbeforeunload = function () {
            var temConteudo = ( $("#tituloPost").val().length > 0 || $("#mensagemPost").val().length > 0);
            // For IE and Firefox prior to version 4
            if (temConteudo) {
               return "Tem uma mensagem para ser enviada!\nSe você sair, sua mensagem será apagada.";
            }


    };


});



function getPost(page, nomeDaSala){
    $.ajax({
        url:"salas/require/php/postPage.php",
        type:"GET",
        data:{pag:page, nome:nomeDaSala},
        success: function(post){
            if(post){
                if(post == "pagina incorreta"){
                    alert("Pagina de Post não encontrada, voltamos ao 0");
                    getPost(0,nomeDaSala);
                }
                else{
                    $("#listaPostDaSala").append(post);
                }
            }else{

                $("#postPage").remove();
                $("#listaPostDaSala").html("<h3 class='primeiraPostagem text-center'>Ainda sem postagem, seja o primeiro!</h3>");
            }
        }
    });
}

function createPost(titulo, mensagem, autoridade, autor ,sala){
    $.ajax({
        url:"salas/require/php/createPost.php",
        type:"POST",
        data:{titulo:titulo, mensagem:mensagem, autoridade:autoridade, autor:autor, sala:sala},
        success: function(att){
            if(att){
                console.log(att);
            }else{
                b = false;
                location.reload();
            }
        }
    });
}
