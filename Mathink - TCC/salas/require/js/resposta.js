/**
 * Created by Anderson Luiz on 09/05/2016.
 */

    function isResolved(resposta){
        $.ajax({
            url:"salas/require/php/isResolved.php",
            type:"POST",
            data:{id:resposta},
            success: function(data){
                if(data==1){
                    $("#responder").remove();
                }else{
                    console.log(data);
                }
            }
        })
    }

    function setResolved(resposta){
        if(confirm("Está certo que está resolvido? Não terá mais como responder esse post")){
            $.ajax({
                url:"salas/require/php/setResolved.php",
                type:"POST",
                data:{id:resposta},
                success: function(data){
                    if(!data){
                        location.reload();
                    }else{
                        console.log(data);
                    }
                }
            })
        }
    }

    function getResposta(resposta) {
        $.ajax({
            url: "salas/require/php/getResposta.php",
            type: "POST",
            data: {id: resposta},
            success: function (post) {
                if (post) {
                    $("#respostas").append(post);
                }
            }
        });
    }

    function createReposta(resposta,autor, msg) {
        resposta = resposta.trim();
        if(resposta.length >0){
            $.ajax({
                url: "salas/require/php/createResposta.php",
                type: "POST",
                data: {mensagem:resposta, autor:autor, id:msg},
                success: function (att) {
                    if (att) {
                        console.log(att);
                    } else {
                        location.reload();
                    }
                }
            });
        }else{
            alert("Digite uma mensagem");
        }

    }
