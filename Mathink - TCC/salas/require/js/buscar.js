/**
 * Created by Anderson Luiz on 09/05/2016.
 */

function setSolicitacao(sala,aluno){
    if(sala.includes("(Solicitado)")){
        if(confirm("Você deseja cancelar a solicitação")){
            cancelRoom(sala,aluno);
        }
    }else{
        if(confirm("Você deseja enviar uma solicitação a " + sala)){
            ajaxRoom(sala,aluno,null);
        }
    }
}

function cancelRoom(sala, aluno){
    $.ajax({
       url:"salas/require/php/cancelSala.php",
       type:"POST",
       data:{sala:sala,aluno:aluno},
       success:function(data){
           if(data){
               console.log(data);
           }else{
               location.reload();
           }
       }
    });
}

function ajaxRoom(sala,aluno,senha){
    $.ajax({
        url:"salas/require/php/salas.php",
        type:"POST",
        data:{sala:sala,aluno:aluno, senha:senha},
        success:function(data){
            if(data){
                if(data == "HAS_PASS"){
                    var senha = prompt("Essa sala possui senha\n Digite a senha:");
                    if(senha != null){
                        if(senha.trim() == ""){
                            alert("Senha Incorreta!");
                        }else{
                            ajaxRoom(sala,aluno,senha);
                        }
                    }
                }else if(data == "WRONG_PASS") {
                    alert("Senha Incorreta!");
                }else {
                    console.log(data);
                }
            }else{
               location.reload();
            }
        }
    });
}