/**
 * Created by Anderson Luiz on 04/06/2016.
 */

var id;

function novoDesafio(htmlId,post){
    id = "#" + htmlId;
    $(id).load('salas/action/lancaDesafio.php');
    $(id).css('display','block');
    $(".disableBackground").css('display','block');

    $(".disableBackground").click(disableNovoDesafio);
    $('html').css('position','fixed');

}


function disableNovoDesafio(){
    $(id).css('display','none');
    $(".disableBackground").css('display','none');
    $('html').css('position','relative');
}
