<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 29/05/2016
 * Time: 16:27
 */



require_once '../../../require/class/conDB.class.php';

$crud = new CRUD;

$id = $_POST['aluno'];
$sala = $_POST['sala'];

$sala = str_replace(" (Solicitado)","",$sala);

$idSala = $crud->select('cd_sala','tb_sala','WHERE nm_sala=?',array($sala));

foreach ($idSala as $dds){
    $idSala = $dds['cd_sala'];
}

$crud->delete("sala_aluno","WHERE cd_sala=? AND cd_usuario=?",array($idSala,$id));