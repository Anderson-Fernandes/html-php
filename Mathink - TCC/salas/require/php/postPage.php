<?php
/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 06/05/2016
 * Time: 11:08
 */

require_once '../../../require/class/conDB.class.php';
require_once '../../../perfil/require/php/marketItens.php';

$paginas = $_GET["pag"];
$nomeDaSala = $_GET['nome'];
$crud = new CRUD;






$post = array();
$content = array();

$infoPost =
    $crud->select("cd_mensagem, nm_titulo, ds_mensagem, sala_mensagem.dt_criacao, sala_mensagem.tm_criacao, nm_usuario, tb_usuario.cd_usuario","sala_mensagem inner join sala_aluno on sala_mensagem.cd_sala_aluno = sala_aluno.cd_sala_aluno inner join tb_sala on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on sala_aluno.cd_usuario = tb_usuario.cd_usuario inner join tb_aluno on tb_usuario.cd_usuario = tb_aluno.cd_usuario","WHERE nm_sala =? order by sala_mensagem.dt_criacao DESC, sala_mensagem.tm_criacao DESC LIMIT 6 OFFSET ".$paginas ,array($nomeDaSala));





if($infoPost->rowCount()>0){

    foreach ($infoPost as $dds){
        $content['idMsg'] = $dds['cd_mensagem'];
        $content['idAutor'] = $dds['cd_usuario'];
        $content['titulo'] = $dds['nm_titulo'];
        $content['mensagem'] = $dds['ds_mensagem'];
        $content['autor'] = $dds['nm_usuario'];
        $content['data'] = $dds['dt_criacao'];
        $content['tempo'] = $dds['tm_criacao'];
        $respostas = $crud->select('cd_mensagem','mensagem_resposta','WHERE cd_mensagem=?',array($dds['cd_mensagem']));
        $content['resposta'] = $respostas->rowCount();
        array_push($post,$content);
    }

}

$contagemDePagina = count($post);






// Horas e Datas do Servidor
date_default_timezone_set("Brazil/East");
$hours = date("H") * 60;
$minute = $hours + date("i");
$seconds =($minute * 60) + date("s");
$day = date("d");
$month = date('m');
$year = date('Y');






if($contagemDePagina > 0):
    if($contagemDePagina>5):
        $paginas+=5;
        $contagemDePagina--;
        ?>
        <script>
            $("#postPage").attr("onclick","getPost('<?php echo $paginas?>','<?php echo $nomeDaSala?>');");
        </script>
        <?php
    else:
        ?>
            <script>
                $("#postPage").remove();
            </script>
        <?php
    endif;
    for($i = 0; $i < $contagemDePagina; $i++):?>
        <div id="<?php echo $post[$i]["idMsg"]?>" class="row postDetals <?php echo $post[$i]['idAutor']?>">
            <div onclick="location.href='salas/post.php?p=' + '<?php  echo $post[$i]['idMsg'] ?>' + '&sala=<?php echo $nomeDaSala?>';" class="col-md-12 postDaSala">
                <div class="row">
                    <div class="col-md-3">
                        <?php

                        $perfilLampi = getPerfilImg($post[$i]['idAutor']);

                        ?>
                        <img src="perfil/require/img/equips/<?=$perfilLampi?>" alt="Lampi" />
                        <br /><br /><br />
                        <p>Por: <?php echo $post[$i]["autor"];?></p>
                    </div>
                    <div class="col-md-6">
                        <h3><?php echo htmlspecialchars($post[$i]["titulo"]); ?></h3>
                        <h5><?php
                            $mensagem = $post[$i]['mensagem'];

                            if(strlen($mensagem)> 230){
                                $mensagem = substr($mensagem,0,230) . " ...";
                            }

                            echo strip_tags(nl2br($mensagem),"<br>");?>
                        </h5>
                    </div>
                    <div class="col-md-3">
                        <p><?php
                            $hoursPost = substr($post[$i]['tempo'],0,2) * 60;
                            $minutePost = $hoursPost + substr($post[$i]['tempo'],3,2);
                            $secondsPost = ($minutePost * 60) + substr($post[$i]['tempo'],6,2);
                            $yearPost = substr($post[$i]['data'],0,4);
                            $monthPost = substr($post[$i]['data'],5,2);
                            $dayPost = substr($post[$i]['data'],8,2);


                            $secondsDiference = $seconds - $secondsPost;
                            $timeDiference = $minute - $minutePost;



                            if($secondsDiference < 60 && $day == $dayPost && $month == $monthPost && $year == $yearPost){
                                if($secondsDiference < 10){
                                    echo "Recente";
                                }else{
                                    echo "há ".$secondsDiference. " segundos";
                                }

                            }else{
                                if($timeDiference < 60 && $day == $dayPost && $month == $monthPost && $year == $yearPost){
                                    $pluralMinuto = (round($timeDiference) <= 1)?" minuto":" minutos";
                                    echo "há ".$timeDiference. $pluralMinuto;
                                }else{
                                    $timeDiference = $timeDiference / 60;
                                    if($timeDiference < 24 && $day == $dayPost && $month == $monthPost && $year == $yearPost){
                                        $pluralHora = (round($timeDiference) == 1)?" hora":" horas";
                                        echo "há " . round($timeDiference) . $pluralHora;
                                    }else{
                                        echo $post[$i]['data'];
                                    }
                                }
                            }

                            ?></p>
                        <p><?php
                            if($post[$i]['resposta'] == 1){
                                echo $post[$i]['resposta']. " Resposta";
                            }else if ($post[$i]['resposta']  <1){

                            }else{
                                echo $post[$i]['resposta']. " Respostas";
                            }
                           ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php endfor;
    else:
        if($paginas != 0){
            echo 'pagina incorreta';
        }
    endif;
?>