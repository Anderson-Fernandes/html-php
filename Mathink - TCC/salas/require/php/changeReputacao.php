<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 09/05/2016
 * Time: 19:28
 */

require_once '../../../require/class/conDB.class.php';
$crud = new CRUD;
session_start();
$id = $_SESSION['logado'];

$log = $crud->select("vl_avaliacao","sala_aluno","WHERE cd_aluno=? AND vl_avaliacao IS NOT NULL",array($id));

if($log->rowCount() > 0){
    $crud->update("sala_aluno","vl_avaliacao = NULL WHERE cd_aluno=?",array($id));
}else{
    $crud->update("sala_aluno","vl_avaliacao = 1 WHERE cd_aluno=?",array($id));
    echo 1;
}
