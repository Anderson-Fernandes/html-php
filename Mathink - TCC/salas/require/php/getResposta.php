<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 09/05/2016
 * Time: 10:22
 */



require_once '../../../require/class/conDB.class.php';

$crud = new CRUD;


$idMsg = $_POST['id'];

$resposta = array();
$content = array();

$infoResposta =$crud->select("ds_resposta, mensagem_resposta.cd_usuario","mensagem_resposta inner join sala_mensagem on sala_mensagem.cd_mensagem = mensagem_resposta.cd_mensagem inner join sala_aluno on sala_aluno.cd_sala_aluno = sala_mensagem.cd_sala_aluno inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario","WHERE sala_mensagem.cd_mensagem =? order by dt_resposta, tm_resposta",array($idMsg));

if($infoResposta->rowCount()>0){

    foreach ($infoResposta as $dds){
        $content['resposta'] = $dds['ds_resposta'];
        $nome = $crud->select("nm_usuario","tb_usuario","WHERE cd_usuario=?",array($dds['cd_usuario']));
        foreach ($nome as $dds){
            $content['nome'] = $dds['nm_usuario'];
        }
        array_push($resposta,$content);
    }
}


$contagemDeRespostas = count($resposta);



if($contagemDeRespostas>0):
    for($i = 0; $i < $contagemDeRespostas; $i++):?>
        <div class="row" style="margin-bottom: 3em">
            <div class="col-md-offset-2 col-md-8 backgroundResposta">
               <h4 class="text-center"><?php echo $resposta[$i]['nome']?></h4>
             <p><?php echo htmlspecialchars(nl2br($resposta[$i]['resposta']))?></p>
            </div>
        </div>
    <?php
    endfor;
endif;
?>
