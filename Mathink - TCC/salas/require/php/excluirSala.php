<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 14/06/2016
 * Time: 02:40
 */

require_once '../../../require/class/conDB.class.php';
require_once '../../../require/php/logado.php';
$crud = new CRUD;
$cripto = new Cripto;


$confirm = $_POST['confirm'];
$sala = $_POST['sala'];

//select tb_usuario.cd_senha
// from tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario
// WHERE cd_autoridade = 5 AND tb_sala.cd_sala = 21;


$confirm = $cripto->setCripto($confirm);
$auth = $crud->select("tb_sala.cd_sala","tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario", "WHERE cd_autoridade = ? AND tb_sala.nm_sala = ? AND tb_usuario.cd_senha =?",array(5,$sala,$confirm));

if($auth->rowCount() > 0) {
    $crud->delete("tb_usuario","WHERE cd_usuario = ?",array($_SESSION['logado']));
    session_destroy();
}else{
    echo "Senha incorreta";
}




