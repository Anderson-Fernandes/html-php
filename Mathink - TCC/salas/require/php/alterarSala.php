<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 07/06/2016
 * Time: 08:37
 */



require_once '../../../require/class/conDB.class.php';
require_once '../../../require/php/logado.php';

$crud = new CRUD;
$vs = new ValidaSenha;
$cripto = new Cripto;



$senha = (strlen($_POST['senha']) > 0)?$_POST['senha']:null;
$sala = $_POST['sala'];
$id = $_SESSION['logado'];
$isPublic = $_POST['priv'];
$confirm = $_POST['confirm'];

 $isPublic = ($isPublic=="true")?true:false;


$confirm = $cripto->setCripto($confirm);





$auth = $crud->select("tb_sala.cd_sala","tb_sala inner join sala_Aluno on tb_sala.cd_sala = sala_aluno.cd_sala
inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario","WHERE nm_sala = ? AND cd_autoridade = ? AND tb_usuario.cd_senha = ?",array($sala,5,$confirm));



if($auth->rowCount() >0){

    $senhaValid=true;
    if(strlen($senha) > 0 ){
        $senhaValid = $vs->setValidaSenha($senha);
        if($senhaValid != $senha){
            echo $senhaValid;
            $senhaValid=false;
        }else{
            $senha = $cripto->setCripto($senha);
        }
    }




    if($senhaValid){
        if($isPublic){
            $logDds = $crud->select('nm_sala, ic_privacidade','sala_aluno inner join tb_sala on sala_aluno.cd_sala = tb_sala.cd_sala','WHERE cd_usuario=? AND cd_autoridade = 5 AND ic_privacidade=? AND cd_senha IS NULL',array($id,1));
            if($logDds->rowCount() > 2){
                echo 'Você so pode criar 3 salas publicas!';
            }else{
                $a = $crud->update("tb_sala","ic_privacidade=?,cd_senha=? WHERE nm_sala=?",array(1,$senha,$sala));

            }
        }else{
            $crud->update("tb_sala","ic_privacidade=?,cd_senha=? WHERE nm_sala=?",array(0,$senha,$sala));

        }
    }

}else{
    echo "Senha de confirmação incorreta";
}
//select * from tb_sala inner join sala_Aluno on tb_sala.cd_sala = sala_aluno.cd_sala WHERE cd_autoridade = 5;














