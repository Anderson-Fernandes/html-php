<?php
/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 02/06/2016
 * Time: 09:16
 */


require_once "../../../../require/class/conDB.class.php";

$crud = new CRUD;

$titulo = $_POST['titulo'];
$exercicio = $_POST['exercicio'];
$privacidade = ($_POST['privacidade'] == "true")?true:false;

if(isset($_POST['alternativas'])){
    $alternativa = $_POST['alternativas'];
    $correta = $_POST['resposta'];
    session_start();
    $id = $_SESSION['logado'];
    $crud->insert("tb_exercicio_professor","nm_titulo=?,ds_exercicio=?,ic_privacidade=?,nm_alternativas=?,cd_resposta_correta=?, cd_usuario = ?",array($titulo,$exercicio,$privacidade,$alternativa,$correta,$id));

}else{

}