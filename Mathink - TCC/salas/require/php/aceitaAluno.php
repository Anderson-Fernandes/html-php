<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 30/05/2016
 * Time: 19:45
 */


require_once '../../../require/class/conDB.class.php';
$crud = new CRUD;

$idNotification = $_POST['msgId'];

$log = $crud->select("cd_sala_aluno","tb_notificacao_sala","WHERE cd_notificacao = ?",array($idNotification));

foreach ($log as $dds){
    $id = $dds['cd_sala_aluno'];
}

 $crud->delete("tb_notificacao_sala",'WHERE cd_notificacao = ?',array($idNotification));
 $crud->update("sala_aluno","cd_autoridade = 1 WHERE cd_sala_aluno = ?",array($id));