<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 09/05/2016
 * Time: 13:05
 */


require_once '../../../require/class/conDB.class.php';

$crud = new CRUD;
$cripto = new Cripto;
$salaNome = $_POST['sala'];
$aluno= $_POST['aluno'];
$senha = $_POST['senha'];

$log = $crud->select("cd_sala, cd_senha","tb_sala","WHERE nm_sala=?",array($salaNome));



foreach ($log as $dds){
    $sala = $dds['cd_sala'];
    $pass = $dds['cd_senha'];
}


if($pass == null){
    $crud->insert("sala_aluno","cd_usuario=?, cd_sala=?, dt_entrada=curdate()",array($aluno,$sala));
}else{
    if($senha == ""){
        echo "HAS_PASS";
    }else{
        $senha = $cripto->setCripto($senha);
        $auth = $crud->select("cd_sala","tb_sala","WHERE cd_sala=? AND cd_senha=?",array($sala,$senha));
        if($auth->rowCount()>0){
            $crud->insert("sala_aluno","cd_usuario=?, cd_sala=?, cd_autoridade = 1, dt_entrada=curdate()",array($aluno,$sala));
        }else{
            echo "WRONG_PASS";
        }
    }
}
