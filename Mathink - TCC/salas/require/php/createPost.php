<?php
/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 08/05/2016
 * Time: 20:13
 */

require_once '../../../require/class/conDB.class.php';

$crud = new CRUD;
$titulo = $_POST['titulo'];
$mensagem = $_POST['mensagem'];
$autoridade = $_POST['autoridade'];
$autor = $_POST['autor'];
$idSala = $crud->select("cd_sala","tb_sala","WHERE nm_sala =?",array($_POST['sala']));

foreach ($idSala as $dds){
    $idSala = $dds['cd_sala'];
}

$idAutor = $crud->select('cd_sala_aluno','sala_aluno','WHERE cd_usuario=? AND cd_sala=?',array($autor,$idSala));
foreach ($idAutor as $dds){
    $idSalaAutor = $dds['cd_sala_aluno'];
}

if($autoridade > 0){
    $isAproved = 1;
}else{
    $isAproved = 0;
}

// insert into sala_mensagem set nm_titulo="MITO", ds_mensagem="ALLAH" ,cd_sala_aluno = 12, dt_criacao = curdate(), tm_criacao = curtime();

$crud->insert('sala_mensagem','nm_titulo=?, ds_mensagem=?,cd_sala_aluno=?, dt_criacao = curdate(), tm_criacao = curtime()',array($titulo,$mensagem,$idSalaAutor));