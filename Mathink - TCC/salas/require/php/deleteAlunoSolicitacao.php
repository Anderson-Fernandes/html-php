<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 30/05/2016
 * Time: 20:08
 */


require_once '../../../require/class/conDB.class.php';
$crud = new CRUD;

$idNotification = $_POST['msgId'];

$log = $crud->select("cd_sala_aluno","tb_notificacao_sala","WHERE cd_notificacao = ?",array($idNotification));

foreach ($log as $dds){
    $id = $dds['cd_sala_aluno'];
}

$crud->delete("tb_notificacao_sala",'WHERE cd_notificacao = ?',array($idNotification));
$crud->delete("sala_aluno","WHERE cd_sala_aluno",array($id));