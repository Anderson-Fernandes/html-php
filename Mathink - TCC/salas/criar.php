<?php
require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';
?>


<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <base href="..">
    <meta charset="UTF-8">
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <title>Mathink</title>
    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/criarSala.js"></script>
    <script src="require/js/upTime.js"></script>
</head>

<body>

<script>
    $(document).ready(function(){
        setLoginCookie(0);
        $('#menu #login #flutuante').fadeOut();
    });
</script>
<?php require_once '../menu.php'; ?>
<div id="notice">
    <i class="fa fa-graduation-cap"></i><h1>Criando a Sala</h1><p>Complete as informações para criar a sua sala</p>
</div>
</header>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <center><h2>Criar sala</h2></center>
            <div class="cadastro text-center">
                <form method="POST" action="javascript:void(0)">
                    <p id="erroCriarSala"></p>
                    Nome<input type="text" placeholder="Digite seu nome" id="nome" >
                    <div class="row">
                        <div class="col-md-6 text-center">
                            Senha
                        </div>
                        <div class="col-md-6 text-center">
                            Confirmar senha
                        </div>
                    </div>
                    <div class="row center-block" style="width: 100%">
                        <div class="col-md-6 text-center">
                            <input type="password" placeholder="Digite sua senha" id="senha">
                        </div>
                        <div class="col-md-6 text-center">
                            <input type="password" placeholder="Confime a sua senha" id="senha2">
                        </div>
                    </div>

                    <div class="text-center" style="margin-top: 1em">
                        <p>Aparecer na lista de busca</p>
                        <label class="radio-inline">
                            <input type="radio" name="privacidadeOptions" id="radioPrivacidade1" value="1" checked style="margin-left: -2em;margin-top: -0.001em"> Sim
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="privacidadeOptions" id="radioPrivacidade0" value="0" style="margin-left: -2em;margin-top: -0.001em"> Não
                        </label>
                    </div>
                    <br />
                    <button style="margin-right:3em;float:right;margin-left: 0em;width: 88%" onclick="fctCriaSala()">Confirmar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>