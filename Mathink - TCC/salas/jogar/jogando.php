<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 04/06/2016
 * Time: 19:51
 */

require_once '../../require/class/conDB.class.php';
require_once '../../require/php/logado.php';

if(!isset($_GET['exp']) || !isset($_GET['game']) || !isset($_GET['sl'])){
    header("Location: ../lista.php");
}


$crud = new CRUD;



$game = base64_decode($_GET['game']);
$sala = base64_decode($_GET['sl']);



$exists = $crud->select('*','desafio_aluno','WHERE cd_desafio = ? AND cd_aluno=?',array($game,$_SESSION['logado']));

if($exists->rowCount() > 0){
    header("Location: ../lista.php");
}

$logUser = $crud->select("cd_sala_aluno","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala","WHERE sala_aluno.cd_usuario = ? AND nm_sala=?",array($_SESSION['logado'],$sala));


$logGame = $crud->select("nm_desafio, nm_exercicio","tb_desafio","WHERE cd_desafio=? AND dt_expira >= curdate()",array($game));

if($logGame->rowCount() > 0){
    foreach ($logGame as $dds){
        $nomeGame = $dds['nm_desafio'];
        $exercicios = $dds['nm_exercicio'];
    }
}else{
    header("Location: ../lista.php");
}



$idExercicios = json_decode($exercicios);

$exercicios = array();

for($i = 0; $i < count($idExercicios) ; $i++){
    $logExercicio = $crud->select("ds_exercicio,nm_alternativas,cd_resposta_correta","tb_exercicio_professor","WHERE cd_exercicio=?",array($idExercicios[$i]));

    if($logExercicio->rowCount() > 0){
        foreach ($logExercicio as $dds){
            $dadosDoExercicio['descricao'] = $dds['ds_exercicio'];
            $dadosDoExercicio['alternativas'] = $dds['nm_alternativas'];
            // $alternativas = $dds['nm_alternativas'];
            //  $alternativas = json_decode($alternativas);
            //  $dadosDoExercicio['alternativas'] = $alternativas;
            $dadosDoExercicio['correta'] = $dds['cd_resposta_correta'];
            array_push($exercicios,$dadosDoExercicio);
        }
    }

}



if($logUser->rowCount() < 1){
    header("Location: ../lista.php");
}else{
    foreach ($logUser as $dds){
        $sala_aluno = $dds['cd_sala_aluno'];
    }

    $logUser = $crud->select('*',"desafio_aluno","WHERE cd_sala_aluno=? AND cd_desafio=?",array($sala_aluno,$game));

    if($logUser->rowCount() > 0){
        header("Location: ../lista.php");
    }

}

$acertos = 0;

$restante = count($exercicios)-1;

$index = 0;



?>


<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <base href="../.."/>
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>

    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <link rel="stylesheet" href="jogos/require/css/questionario.css">
    <script src="require/js/sketch.js"></script>
    <script src="jogos/sairPagina.js"></script>
    <script src="require/js/upTime.js"></script>





    <script>
        var acerto = 4;
        var erro = 0;
        var restante = parseInt('<?=$restante?>');
        
        
            function pergunta(index){
                $.ajax({
                    url:'salas/jogar/require/php/estastic.php',
                    method:'POST',
                    data:{game:'<?=$game?>',sl:'<?=$sala?>',i:index},
                    success: function(data){
                        if(data){
                            $('#resposta').html(data);

                            $('.resposta').click(function () {
                                var userResposta = $(this).val();

                                if (resposta == userResposta) {
                                    console.log('acertou');
                                    $(this).css('background-color', 'green');
                                    $('.resposta').attr('disabled', 'disabled');
                                    fctAcertou(true);

                                } else {
                                    console.log('errou');
                                    $(this).css('background-color', 'red');
                                    $('.resposta').attr('disabled', 'disabled');
                                    fctAcertou(false);
                                }
                                restante--;
                                if(restante >= 0 ){
                                    setTimeout(function(){
                                        index++;
                                        pergunta(index);
                                        <?php $index++?>
                                        $('#pergunta').html("<h1> <?= $exercicios[$index]['descricao']?></h1>");
                                        $("#vidaP").html("<h4>Restantes:"+restante+"</h4>");
                                    },1000);

                                }else{

                                    setTimeout(createHTMLAcabou,1000);

                                }

                            })

                        }else{
                            console.log('Sem dados');
                        }
                    }
                })
            }


        function fctAcertou(boolean){
            $.ajax({
               url:'salas/jogar/require/php/correto.php',
               method:'POST',
               data:{desafio: <?=$game?>, acertou:boolean},
               success:function(data){
                   if(data){
                       console.log(data);
                   }
               }
            });
        }




        function createHTMLAcabou(){
            window.onbeforeunload = function (e) {
            };

            var	htmlComplete =
                '<div class="row">'+
                '<div class="col-md-push-1 col-md-2 col-sm-2 col-xs-2 col-lg-3">'+
                '<br /><br /><br /><img style="width: 72%" class="img-responsive" src="require/img/lampi/Lampi.png" alt="Lampi" />'+
                '</div>'+
                '<div class="col-xs-9 col-sm-9 col-md-10 col-lg-8">'+
                '<div id="pergunta" style="margin-bottom: 4em;">'+
                '<div class="row">'+
                '<div class="col-md-12">';


            // FRASE EDITAVEL
                var pagina = "href='javascript:window.location.href="+'"salas/aula.php?<?=$sala?>"'+"'";
                htmlComplete+=

                    '<h1>Parabens você completou a Tarefa!</h1>'+
                    '<br />'+
                    '<a ' +pagina+'><h1>Voltar para a sala</h1></a>';


            // FIM FRASE EDITAVEL


            htmlComplete+=
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>';
            document.getElementById("jogando").innerHTML = htmlComplete;
        }



        $(document).ready(function(){
                pergunta(0);
                $('#pergunta').html("<h1> <?= $exercicios[0]['descricao']?></h1>");

                /*var htmlRespostas = "";
                var alternativas = '<?= $exercicios[0]['alternativas']?>';
                alternativas = JSON.parse(alternativas);


                for(var i = 0 ; i < alternativas.length; i++){
                    var valor = alternativas[i];
                    htmlRespostas += "<div class='row'>" +
                        "<div class='col-md-offset-3 col-md-9'>" +
                        "<button class='resposta text-center' value='"+i+"'>" +valor+ " </button>"+
                        "</div>" +
                        "</div>";

                }

                $('#resposta').html(htmlRespostas);*/


            });



    </script>


</head >
<body>

<?php

require_once "../../menu.php";
?>
</header>



<div class="container base">


    <div class="row">
        <div class="container-fluid">
            <div class="col-md-5">
                <h4> <?php echo $nomeGame ?> </h4>
            </div>

            <div id="scoreP" class=" col-md-3">
                <h4> Acertos: <?= $acertos ?></h4>
            </div>
            <div id="vidaP" class="col-md-push-1 col-md-3">
                <h4>Restantes: <?= $restante?></h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div id="progressbar"></div>
            </div>
        </div>
    </div>
    <div class="row"><br></div>

    <!--****************** DESIGN JOGANDO *******************-->
    <div id="jogando" >
        <div class="row">
            <div class="col-md-push-1 col-md-2 col-sm-2 col-xs-2 col-lg-3">
                <br /><br /><br /><img style="width: 72%" class="img-responsive" src="require/img/lampi/Lampi.png" alt="Lampi" />
            </div>
            <div class="col-xs-9 col-sm-9 col-md-10 col-lg-8">
                <div id="pergunta">
                    <h4>A questão a ser exibida neste balão será inserida dentro de um p. Este texto é apenas um teste para ver o quão grande o balão pode ser.</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div id="resposta">
                    <!--PERGUNTAS ESCRITA NO writeResposta-->
                </div>
                <div class="row botoesAcoes">


                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <button onclick="errou(true);">Pular&nbsp <i class="fa fa-arrow-right"></i></button>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 hidden-xs">
                <div class="row">
                    <div class="col-md-12">
                        <div class="teste">
                            <canvas class="lousa" id="tools_sketch"></canvas>
                            <div style="position:absolute;font-size: 3em">
                                <a style="color:white" href="#tools_sketch" data-tool="marker">Desenhar</a>
                                <a style="color:white" href="#tools_sketch" data-tool="eraser">Apagar</a>
                            </div>
                            <script>
                                $(function() {
                                    $('#tools_sketch').sketch({defaultColor: "#FFF"});
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--****************** FIM DESIGN JOGANDO *******************-->





</div>




</div>

</body>

</body>
</html>



