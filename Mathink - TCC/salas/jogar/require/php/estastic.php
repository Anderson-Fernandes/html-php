<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 05/06/2016
 * Time: 17:53
 */

require_once '../../../../require/class/conDB.class.php';


$crud = new CRUD;
$game = $_POST['game'];
$sala = $_POST['sl'];
$index = $_POST['i'];

$logGame = $crud->select("nm_desafio, nm_exercicio","tb_desafio","WHERE cd_desafio=? AND dt_expira >= curdate()",array($game));

if($logGame->rowCount() > 0){
    foreach ($logGame as $dds){
        $nomeGame = $dds['nm_desafio'];
        $exercicios = $dds['nm_exercicio'];
    }
}else{
    header("Location: ../lista.php");
}



$idExercicios = json_decode($exercicios);

$exercicios = array();

for($i = 0; $i < count($idExercicios) ; $i++){
    $logExercicio = $crud->select("ds_exercicio,nm_alternativas,cd_resposta_correta","tb_exercicio_professor","WHERE cd_exercicio=?",array($idExercicios[$i]));

    if($logExercicio->rowCount() > 0){
        foreach ($logExercicio as $dds){
            $dadosDoExercicio['descricao'] = $dds['ds_exercicio'];
           // $dadosDoExercicio['alternativas'] = $dds['nm_alternativas'];
             $alternativas = $dds['nm_alternativas'];
              $alternativas = json_decode($alternativas);
              $dadosDoExercicio['alternativas'] = $alternativas;
            $dadosDoExercicio['correta'] = $dds['cd_resposta_correta'];
            array_push($exercicios,$dadosDoExercicio);
        }
    }

}

?>

<script> var resposta = <?=$exercicios[$index]['correta']?>;
    acerto++;
    console.log(acerto);

</script>

<?php for($i = 0 ; $i <= count($exercicios[$index]) ; $i++):
    ?>

    <div class='row'>
        <div class='col-md-offset-3 col-md-9'>
            <button class='resposta text-center' value='<?=$i?>'><?= $exercicios[$index]['alternativas'][$i]?> </button>
        </div>
    </div>


<?php endfor; ?>