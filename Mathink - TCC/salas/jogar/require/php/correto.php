<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 05/06/2016
 * Time: 18:38
 */

require_once '../../../../require/class/conDB.class.php';
session_start();

$desafio = $_POST['desafio'];
$acerto = $_POST['acertou'];
$id = $_SESSION['logado'];
$crud = new CRUD;

$acerto = ($acerto == 'true')?true:false;
var_dump($acerto);

$exists = $crud->select('*','desafio_aluno','WHERE cd_desafio = ? AND cd_aluno=?',array($desafio,$id));


if($acerto){
    if($exists->rowCount()>0){
        $crud->update("desafio_aluno","qtd_acertos = qtd_acertos + 1, dt_concluido = CURDATE(), tm_concluido = CURTIME() WHERE cd_desafio=? AND cd_aluno=?",array($desafio,$id));
    }else{
       $crud->insert("desafio_aluno","cd_desafio=?,cd_aluno=?,qtd_acertos = 1, qtd_erros = 0, dt_concluido = CURDATE(), tm_concluido= CURTIME()",array($desafio,$id));
    }
}else{
    if($exists->rowCount()>0){
        $crud->update("desafio_aluno","qtd_erros= qtd_erros + 1, dt_concluido = CURDATE(), tm_concluido = CURTIME() WHERE cd_desafio=? AND cd_aluno=?",array($desafio,$id));

    }else{
        $crud->insert("desafio_aluno","cd_desafio=?,cd_aluno=?,qtd_erros = 1, qtd_acertos = 0, dt_concluido = CURDATE(), tm_concluido= CURTIME()",array($desafio,$id));

    }
}