<?php
require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <base href="..">
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>
</head>
<body>

<?php require_once '../menu.php' ?>

<div id="banner" class="banner-img">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-offset-1 col-md-11">
                      <h1>Surpreenda seus alunos</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                      <h3>Ensine com metodos incriveis, tornando facil o aprendizado com jogos!</h3>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-lg-offset-1 col-md-8">
                         <button class="btn btn-default" onclick="window.location.href='salas/criar.php'">Quero criar a sala!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</header>

<div id="conteudoNovaSala" class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center conteudo-titulo novaSalaH2">Benefício da sala</h1>
        </div>
    </div>

    <div  class="row">

        <div class="col-md-4">

            <div class="row">
                <div class="col-md-12 text-center">
                   <img src="salas/require/img/book.svg"/><h2>Conteudo Imenso</h2>
                </div>
            </div>

            <div class="row">
                <div class="text-center col-md-12">
                    <p>Vários exércicios para que seu aluno possa aprender com mais facilidade!</p>
                </div>
            </div>
        </div>


        <div class="col-md-4">

            <div class="row">
                <div class="col-md-12 text-center">
                   <img src="salas/require/img/schoolatention.svg" ><h2>Mantenha a Ordem</h2>
                </div>
            </div>

            <div class="row">
                <div class="text-center col-md-12">
                    <p>Tenha atenção de todos seus alunos!</p>
                </div>
            </div>

        </div>


        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12 text-center">
                   <img src="salas/require/img/uprank.svg"/> <h2>Aumente o Rendimento</h2>
                </div>
            </div>

            <div class="row">
                <div class="text-center col-md-12">
                    <p>O rendimento do seus alunos irá evoluir incrivelmente</p>
                </div>
            </div>


        </div>
    </div>

    <div  class="row conteudoNovaSala2">

        <div class="col-md-4">

            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="salas/require/img/schoolteach.svg"/><h2>Lance Desafios</h2>
                </div>
            </div>

            <div class="row">
                <div class="text-center col-md-12">
                    <p>Crie uma maratona de exercicio para seus alunos</p>
                </div>
            </div>
        </div>


        <div class="col-md-4">

            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="salas/require/img/pen.svg" ><h2>Não Perca Tempo</h2>
                </div>
            </div>

            <div class="row">
                <div class="text-center col-md-12">
                    <p>Exercicios automaticamente corrigidos</p>
                </div>
            </div>

        </div>


        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="salas/require/img/schoolquestion.svg"/> <h2>Tire as Duvidas</h2>
                </div>
            </div>

            <div class="row">
                <div class="text-center col-md-12">
                    <p>Veja os maiores erros de seus alunos</p>
                </div>
            </div>


        </div>
    </div>

    <div id="entrarPaginaInicial" class="row">
        <div class="col-md-12 text-center">
            <h2>Crie a sala agora!</h2>
            <button class="center-block btn btn-warning" onclick="window.location.href='salas/criar.php'">Criar sala</button>
        </div>
    </div>
</div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>