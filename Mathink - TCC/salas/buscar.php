<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 09/05/2016
 * Time: 12:15
 */


require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';
?>

<?php

$crud = new CRUD;
$id = $_SESSION['logado'];

$salas = array();;

$dados = array();

//select * FROM tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario WHERE tb_usuario.cd_usuario <> 2 AND sala_aluno.cd_autoridade = 5  AND ic_privacidade =1;
$logSalas = $crud->select("tb_sala.cd_sala,nm_sala,nm_usuario","tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario","WHERE tb_usuario.cd_usuario <> ? AND sala_aluno.cd_autoridade = ?  AND ic_privacidade =?",array($id,5,1));

if($logSalas->rowCount() >0){
    foreach ($logSalas as $dds){
        $isDaSala = $crud->select('cd_autoridade','tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario','WHERE tb_sala.cd_sala = ? AND tb_usuario.cd_usuario = ? AND cd_autoridade IS NOT NULL', array($dds['cd_sala'],$id));
        if($isDaSala->rowCount() <1){
            $dados['sala'] = $dds['nm_sala'];
            $dados['professor'] = $dds['nm_usuario'];
            $solicitacao = $crud->select('cd_autoridade','tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario','WHERE tb_sala.cd_sala = ? AND tb_usuario.cd_usuario = ? AND cd_autoridade IS NULL', array($dds['cd_sala'],$id));
            $solicitacao = ($solicitacao->rowCount() >0)?true:false;
            $dados['solicitacao'] = $solicitacao;
            array_push($salas,$dados);
        }
    }
}

$quantidadeDeSalas = count($salas);

?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <base href="..">
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>
    <script src="salas/require/js/buscar.js"></script>

    <script>
        $(document).ready(function () {
            $('.listaSala').click(function(){
                var id = $(this).find('td:first-child').text();

                setSolicitacao(id,'<?php echo $id ?>');
            });
        });
    </script>

</head>
<body>
<?php if(isset($_SESSION['logado'])){
    if(isset($_SESSION['isPrimeiraVez']) && $_SESSION['isPrimeiraVez'] == 1){
        require_once 'tutorial/inicio.html' ;
    }
}?>
<?php require_once '../menu.php' ?>
<div id="notice">
    <i class="fa fa-university"></i><h1>Sala!</h1><p>Entre em uma dessas salas</p>
</div>
</header>

<div class="container-fluid">
    <div id="salas" class="row">
        <div class="col-sm-9 col-md-9">
            <div class="table-responsive"style="overflow:auto; " >
                <table class="table table-hover tabela-comun">
                    <thead>
                    <tr>
                        <td>
                            <h1>Nome</h1>
                        </td>
                        <td>
                            <h1>Professor</h1>
                        </td>
                    </tr>
                    </thead>
                    <tbody style="cursor:pointer;">
                    <?php
                    if($quantidadeDeSalas > 0):
                        for($i=0; $i < $quantidadeDeSalas; $i++): ?>
                            <tr class="listaSala">
                                <td ><?php
                                    echo $salas[$i]['sala'];
                                    echo ($salas[$i]['solicitacao'])?" <b>(Solicitado)</b>":"";
                                    ?></td>
                                <td><?php echo $salas[$i]['professor']; ?></td>
                            </tr>
                        <?php endfor;
                    else:?>
                        <tr>
                            <td class="text-center" colspan="2">
                               Não há salas disponíveis
                            </td>
                        </tr>
                    <?php endif; ?>

                    </tbody>

                </table>
            </div>
        </div>
        <a href="salas/lista.php">
            <div  class="col-sm-3 col-md-3 laminaDescubra" style="min-height: 20em;">
                <div class="row" style="margin-top: 50%;margin-bottom: 50%">
                    <i class="fa fa-sign-in"></i><h1>Voltar</h1><p>Veja as salas que você participa</p>
                </div>
            </div>
        </a>
    </div>
</div>
<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>