<?php
/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 08/05/2016
 * Time: 22:26
 */
require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';

$id = $_SESSION['logado'];
$idMsg = $_GET['p'];
$nomeDaSala = $_GET['sala'];




$crud = new CRUD;

$logMensagem = $crud->select("*","sala_mensagem","WHERE cd_mensagem =?",array($idMsg));

if($logMensagem->rowCount() > 0){
    foreach ($logMensagem as $dds){
        $titulo = htmlspecialchars($dds['nm_titulo']);
        $mensagem = htmlspecialchars($dds['ds_mensagem']);
        $autorId =  htmlspecialchars($dds['cd_sala_aluno']);
        $logAutorId = $crud->select("cd_usuario","sala_aluno","WHERE cd_sala_aluno = ?",array($autorId));
        foreach ($logAutorId as $intoDds){
            $autorId = $intoDds['cd_usuario'];
        }

    }
}else{
    header('Location: lista.php');
}


$ddsAutor = $crud->select("nm_usuario","tb_usuario","WHERE cd_usuario=?", array($autorId));

foreach ($ddsAutor as $dds){
    $nomeAutor = $dds['nm_usuario'];
}





$isDaSala = $crud->select("cd_sala_aluno, cd_autoridade","tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala","WHERE cd_usuario=1 AND nm_sala='PSDB'AND cd_autoridade IS NOT NULL",array($id,$nomeDaSala));

if($isDaSala->rowCount() <1){
   // header('Location: lista.php');
}else{
    foreach ($isDaSala as $dds){
        $idAlunoNaSala = $dds['cd_sala_aluno'];
        $poder = $dds["cd_autoridade"];
    }
}
?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <base href="..">
        <meta charset="UTF-8">
        <meta name="description"  content="Home" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
        <title>Mathink</title>
        <script src="require/js/jquery.js"></script>
        <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
        <script src="require/boot/js/bootstrap.js"></script>
        <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
        <link rel="icon" href="require/img/logo/mathink.ico">
        <link rel="stylesheet" href="require/css/master.css">
        <script src="require/js/loginForm.js"></script>
        <link rel="stylesheet" href="require/css/newMaster.css" />
        <script src="require/js/criarSala.js"></script>
        <script src="require/js/upTime.js"></script>
        <script src="require/js/jquery-elastic-1.6.10/jquery.elastic.source.js"></script>
        <script src="salas/require/js/resposta.js"></script>

        <script>

            window.onbeforeunload = function (e) {
                e = e || window.event;
                var temConteudo = ( $("#tituloPost").val().length > 0 || $("#mensagemPost").val().length > 0);
                // For IE and Firefox prior to version 4
                if (temConteudo && e.keyCode != 13) {
                    e.returnValue = "Tem uma mensagem para ser enviada!\nSe você sair, sua mensagem será apagada.";
                }

                // For Safari
                if (temConteudo && e.keyCode != 13) {
                    return "Tem uma mensagem para ser enviada!\nSe você sair, sua mensagem será apagada.";
                }

            };

            $(document).ready(function(){
                var mensagem = '<?php echo $idMsg?>';
                isResolved(mensagem);
                getResposta(mensagem);

                
            });
        </script>
    </head>

    <body>
    <?php require_once '../menu.php' ?>
    
    <div class="container-fluid">
        <div class="row">
            <div style="border-bottom: 4em solid #1b6d85;" class="col-md-12 backgrounSalas">
               <h2 style="text-transform: uppercase" class="text-center"><?php echo $titulo?></h2>
                <h5 class="text-right">Enviada por: <?php echo $nomeAutor?></h5>
                <h4><?php echo nl2br($mensagem)?></h4>
            </div>
        </div>
        <div id="respostas">

        </div>
        
        <div id="responder" class="row" style="margin-bottom: 10em">
           <div class="col-md-offset-2 col-md-8 backgroundResposta">
               <h3 class="text-center"> <?php echo ($id == $autorId)?"Responda seu post":"Ajude o " . $nomeAutor ?></h3>
                <textarea placeholder="Escreav aqui!" col="" class="form-control" id="resposta"  title="Pressione ENTER para enviar o post"></textarea>
                
               <button onclick="createReposta($('#resposta').val(),'<?php echo $autorId?>','<?php echo $idMsg?>')">Enviar</button>
               <?php if($id == $autorId):?>
                  <button onclick="setResolved('<?php echo $idMsg?>');">Resolvido</button>
               <?php endif;?>
           </div>
        </div>
    </div>
    
    </body>


    <footer>
        <div class="footer-copy">
            <div class="container">
                <div class="row">

                    <div id="copyright" class="col-md-3">
                        <div class="row center-block">
                            <div class="col-md-10" id="imagemFooter">
                                <a href="index.php"></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="text-left">© Todos os direitos reservados!</h4>
                            </div>
                        </div>
                    </div>

                    <div id="mapaSite" class="col-md-8">
                        <nav>
                            <a href="index.php">Inicio</a>
                            <a href="index.php">Desafio</a>
                            <a href="index.php">Contato</a>
                            <a href="index.php">Sobre</a>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </footer>

</html>
