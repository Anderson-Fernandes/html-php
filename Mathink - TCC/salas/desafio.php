<?php
/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 31/05/2016
 * Time: 00:52
 */

require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';

if(!isset($_GET['nome'])){
    header('Location: lista.php');
}


if(isset($_GET['p'])){
    $paginas = intval($_GET['p']);
}else{
    $paginas = 0;
}

$crud = new CRUD;

$nomeDaSala = $_GET['nome'];


$id = $_SESSION['logado'];

$logProfessor =  $crud->select("nm_usuario, nm_nickname","tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario inner join tb_aluno on tb_aluno.cd_usuario = tb_usuario.cd_usuario","WHERE nm_sala = ? AND cd_autoridade =? ",array($nomeDaSala, 5));

foreach ($logProfessor as $dds){
    $nomeProfessor = $dds['nm_usuario'];
    $nickProfessor = $dds['nm_nickname'];
}

$logAlunos = $crud->select("tb_aluno.cd_usuario, nm_nickname","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala INNER JOIN tb_aluno ON tb_aluno.cd_usuario = sala_aluno.cd_usuario","WHERE nm_sala = ? AND cd_autoridade <> ?",array($nomeDaSala,5));

$avaliadores = $crud->select("vl_avaliacao","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala","WHERE nm_sala  =? AND vl_avaliacao IS NOT NULL",array($nomeDaSala));

$qtdAlunos = $logAlunos->rowCount();

if($qtdAlunos>0){
    $alunos = array();
    foreach ($logAlunos as $dds){
        $dados['nome'] = $dds['nm_nickname'];
        $dados['codigo'] = $dds['cd_usuario'];
        array_push($alunos,$dados);
    }
}

$avaliadores = $avaliadores->rowCount();





$logExercicios = $crud->select("nm_titulo,ds_exercicio,nm_alternativas","tb_exercicio_professor","WHERE cd_usuario = ? ORDER BY nm_titulo DESC",array($id));

$exercicios = array();
foreach ($logExercicios as $dds){
    $info['titulo'] = $dds['nm_titulo'];
    array_push($exercicios,$info);
}


$logDesafio = $crud->select("cd_desafio,nm_desafio,dt_desafio,dt_expira","tb_desafio inner join sala_aluno on sala_aluno.cd_sala_aluno = tb_desafio.cd_sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala ","WHERE nm_sala=? AND dt_expira < curdate()",array($nomeDaSala));
$desafio = array();
if($logDesafio->rowCount() > 0){
    foreach ($logDesafio as $dds){
        $infoDesafio['codigo'] = $dds['cd_desafio'];
        $infoDesafio['nome'] = $dds['nm_desafio'];
        $infoDesafio['data'] = $dds['dt_desafio'];
        $infoDesafio['expira'] = $dds['dt_expira'];
        array_push($desafio,$infoDesafio);
       
    }
}

$logCurrentChallenger = $crud->select("cd_desafio,nm_desafio,dt_desafio,dt_expira","tb_desafio inner join sala_aluno on sala_aluno.cd_sala_aluno = tb_desafio.cd_sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala ","WHERE nm_sala=? AND dt_expira >= curdate()",array($nomeDaSala));
$desafioAtual = array();
if($logCurrentChallenger->rowCount () > 0){
    foreach ($logCurrentChallenger as $dds){
        $desafioAtual['codigo'] = $dds['cd_desafio'];
        $desafioAtual['nome'] = $dds['nm_desafio'];
        $desafioAtual['data'] = $dds['dt_desafio'];
        $desafioAtual['expira'] = $dds['dt_expira'];
      
    }
}







$isDaSala = $crud->select('tb_sala.cd_sala, cd_autoridade',"tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala","WHERE nm_sala=? AND cd_usuario=? AND cd_autoridade IS NOT NULL",array($nomeDaSala,$id));
if($isDaSala->rowCount()<1){
    header('Location: lista.php');
}else{
    foreach ($isDaSala as $dds){
        $idSala = $dds['cd_sala'];
        $poder = $dds['cd_autoridade'];
    }

    if($poder != 5){
        header('Location: aula.php?nome='.$nomeDaSala);
    }
}






?>




<!DOCTYPE html>
<html lang="pt-BR" xmlns="http://www.w3.org/1999/html">
<head>
    <base href="..">
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>
    <script src="require/js/ballon.js"></script>
    <script src="salas/require/js/avaliacao_da_sala.js"></script>
    <script src="require/contextMenu/contextMenu.js"></script>
    <link rel="stylesheet" href="require/contextMenu/contextMenu.css" />
    <script src="require/js/notification.js"></script>
    <script src="salas/require/js/novoDesafio.js"></script>
    <script src="require/js/moment.js"></script>


    <script>

        $(document).ready(function(){
            writeAvaliacaoBalao(<?php echo $avaliadores?>,<?php echo $qtdAlunos ?>);
            getAvaliacaoImg(<?php echo $qtdAlunos ?>, <?php echo $avaliadores?>);
            getCountNotification('<?= $nomeDaSala ?>','sala','notificationSala');

            var countFormulaClick = false;

            function inArray(needle, haystack) {
                var length = haystack.length;
                for(var i = 0; i < length; i++) {
                    if(haystack[i] == needle) return true;
                }
                return false;
            }


                $('#formulaExercicio').click(function(){
                    if(!countFormulaClick) {
                        alert("O exercicio dinamico está em fase de teste, se a resposta prevista estiver errada, por favor, utilize o estatico!");
                        countFormulaClick = true;
                    }
                });

                $('#formulaExercicio').focusout(function(){
                    var enunciado = numerosDinamicos(false);
                    var numeros = enunciado[1];
                    var countNumeros = numeros.length;
                    var restantes =  new Array();
                    var retornoRestantes = "";
                    enunciado = enunciado [0];
                    var resposta = formulaDinamica(enunciado);
                   

                    for(var i = 0; i <= countNumeros ; i++){
                        if(resposta[1][i]){
                             for(var y = 0; y < countNumeros ; y++){
                                if(resposta[1][i] == numeros[y]){
                                    restantes.push(resposta[1][i])
                                    resposta[1][i] = '';
                                }else{

                                }
                             }

                            /*
                            if(inArray(resposta[1][i],numeros)){
                                restantes.push(resposta[1][i]);

                            }else{
                                retornoRestantes += resposta[1][i] + ",";
                            }*/
                        }
                    }

                    console.log(numeros);
                    console.log(restantes);

                    if(countNumeros != restantes.length){
                        $('#previewResposta').html('Existe numeros dinamicos que não estão sendo utilizados');
                    }else{
                        $('#previewResposta').html('Resposta: ' + resposta[0]);
                    }
                });


                var formulaDinamica = function(enunciado){
                   // var formula = $('#formulaExercicio').val();
                    var dinamico = enunciado.split(/[^0-9\.]+/);

                    //var formulaNumeros = dinamico.split(/[^0-9\.]+/);


                    var retorno = [eval(dinamico),dinamico];

                    return retorno;
                }

                var numerosDinamicos = function( exibirButton ){
                   var exercicio = $('#exercicio').val();
                    var enunciado = "";
                    var numero = '';
                    var numberCount = 0;
                    var numeros = new Array();

                   for(var i = 0; i <  exercicio.length ; i++){
                       var char = exercicio.charAt(i);
                      if(isNaN(char)){
                          if(char == ',' && numberCount > 0 || char == '.' && numberCount > 0){
                              if(char != " "){
                                  numero = numero + '.';
                                  numberCount ++;

                              }
                          }else{
                              if(numberCount  > 0){
                                  numero = (exibirButton)?Math.round(Math.random() * 5):numero;
                                if(!exibirButton){
                                       if(confirm("Esse numero é dinamico: " + numero + " ?")){

                                      enunciado = enunciado.substr(0, enunciado.length-1) + '{|}' + numero + '{|}';
                                  }else{

                                      enunciado = enunciado.substr(0, enunciado.length-1) + numero;
                                  }
                                }else{
                                     enunciado = enunciado.substr(0, enunciado.length-1) + '{|}' + numero + '{|}';
                                }
                                  
                               
                                  numeros.push(numero);
                                  numero = "";
                                  numberCount = 0;
                                  enunciado += " " + char;

                              }else{
                                  enunciado += char;
                              }
                          }
                       }else{
                          if(char != " "){
                              numero = numero + '' + char;
                              numberCount ++;

                          }else{
                              enunciado += char;
                          }
                      }

                   }
                    console.log(enunciado);
                    var retorno = new Array();
                    retorno[0] = enunciado;
                    retorno[1] = numeros;
                    return retorno;

                }


        function gerarExercicio(){
           var enunciado = numerosDinamicos(true);
           var f = formulaDinamica(enunciado[0]);
            console.log(f[0]);
            console.log(f[1])
        }




                $('#gerarExercicio').click(function(){
                    gerarExercicio();
                })


        });


       
        function setReputacao(){
            <?php
            $avaliadores = $crud->select("vl_avaliacao","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala","WHERE nm_sala = ? AND vl_avaliacao IS NOT NULL",array($nomeDaSala));
            $avaliadores = $avaliadores->rowCount();
            ?>
            $.ajax({
                url:"salas/require/php/changeReputacao.php",
                success: function(post){

                    if(post ==1){
                        console.log('1');
                        writeAvaliacaoBalao(<?php echo $avaliadores+1?>,<?php echo $qtdAlunos ?>);
                    }else{
                        console.log('2');
                        writeAvaliacaoBalao(<?php echo $avaliadores?>,<?php echo $qtdAlunos ?>);
                    }
                }
            });
        }



    </script>

<script>

    var qtdAlternativas = function(qtd){
        var html = "";

        for(var i = 1; i <= qtd; i++){
            html +='<input id="alternativa-'+i+'" type="text" class="form-control" placeholder="Alternativa '+i+'">'
        }


        $("#input-alterantivas").html(html);

        html = ' <h4>Resposta correta</h4> ';

        for(var i = 1; i <=qtd; i++){
            html += '<label style="font-size:2em; margin-right: 0.3em" > '+i+' <input type="radio" name="correta"  value="'+i+'" class="inputRespostaCorreta"/></label>';
        }



        $("#respostaCorreta").html(html);

    }

    function isDinamic(is){
        if(is){
            $('#formula-form').fadeIn();
            $('#input-alterantivas').fadeOut();
            $('#respostaCorreta').fadeOut();
            $('#ex-sim').attr('checked','true');
            $('#ex-nao').attr('checked','false');

        }else{
            $('#formula-form').fadeOut();
            $('#input-alterantivas').fadeIn();
            $('#respostaCorreta').fadeIn();
            $('#ex-sim').attr('checked','false');
            $('#ex-nao').attr('checked','true');

        }
    }

    $(document).ready(function(){
        qtdAlternativas(4);

        $('#qtd-alterantivas').change(function(){
            qtdAlternativas($(this).val());
        });

    })
</script>

<script>
    function salvar(){
       var nome =  $('#nome-exercicio');
        var enunciado = $('#exercicio');

        var perguntas;
        var qtdPerguntas =  $('#qtd-alterantivas').val();
        var privacidade = $("#pub-sim").is(":checked");




        if(!$("#ex-sim").is(":checked")){
            perguntas = new Array();
            for(var i = 1 ; i <= qtdPerguntas ; i++){
                if($('#alternativa-'+i).val().length > 0){
                     perguntas.push($('#alternativa-'+i).val());
                }
            }
        }

    if(!isFill(nome,"nome do exercicio") || !isFill(enunciado,"enunciado do exercicio")){
        return;
    }

        if(!$("#ex-sim").is(":checked")){
           if (qtdPerguntas > perguntas.length) {
               alert('Preencha todas alternativas');
               return;
           }else{

               if($(".inputRespostaCorreta").is(":checked")){
                   var resposta = $(".inputRespostaCorreta").filter(":checked").val();
                   estaticoSave(nome.val(),enunciado.val(),privacidade,perguntas,resposta);
               }else{
                   alert('Selecione a resposta correta');
               }
           }
         }
    }


    function estaticoSave(titulo,exercicio,privacidade,alternativas,resposta){

        alternativas = JSON.stringify(alternativas);

        $.ajax({
            url:"salas/require/php/exercicio/exercicio.php",
            type:"POST",
            data:{titulo:titulo,exercicio:exercicio,privacidade:privacidade,alternativas:alternativas, resposta:resposta},
            success:function(dados){
                if(dados){
                    console.log(dados);
                }else{
                    location.reload();
                }
            }
        });
    }

    function isFill(input, msg){
        if(input.val().length < 1){
            input.focus();
            alert("Preencha o campo: " + msg);
            return false;
        }
        return true;
    }
</script>


</head>
<body>
<?php require_once '../menu.php' ?>

</header>

<div class="disableBackground"></div>
<div id="novoDesafio"></div>

<div class="container-fluid">
    <div id="topoSalaDeAula" class="row">
        <div class="col-md-12">
            <i class="fa fa-university"></i>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h1>SALA - <href style="cursor: pointer;" onclick="location.href='salas/aula.php?nome=' + '<?=$nomeDaSala?>'"><?php echo $nomeDaSala ?> </href> </h1>
                    <div class="row"  >
                        <div class="col-md-offset-1 col-md-10" >
                            <h2>Professor: <href style="cursor: pointer;" onclick="location.href='perfil.php?nome=' + '<?= $nickProfessor?>';"><?php echo $nomeProfessor ?></href></h2>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($poder == 5): ?>
                <div id="reputacaoDaSala"></div>
            <?php else: ?>
                <div onclick="setReputacao()" id="reputacaoDaSala"></div>
            <?php endif; ?>
        </div>
    </div>

    <div id="topoConfigDeAula" class="row">
        <div class="col-md-12 text-center">

            <img src="salas/require/img/alunos.svg" onclick="location.href='salas/alunos.php?nome='+'<?=$nomeDaSala?>'"/>
            <img src="salas/require/img/challenger.svg" onclick="location.href='salas/desafio.php?nome='+'<?=$nomeDaSala?>'"/>
            <img src="salas/require/img/notificacao.svg" onclick="location.href='salas/notificacao.php?nome='+'<?=$nomeDaSala?>'"/> <span id="notificationSala">0</span>
            <img src="salas/require/img/config.svg" onclick="location.href='salas/config.php?nome='+'<?=$nomeDaSala?>'"/>


        </div>
    </div>

    <div class="row">
        <div class="col-md-3 backgrounSalas">
            <div id="desafioSala" class="col-md-12 text-center">
                <?php if(count($desafioAtual) < 1):?>

                <div class="row">
                    <h3 class="text-center">SEM DESAFIO LANÇADO!</h3>
                </div>

                <div class="row">
                    <img src="salas/require/img/maths-teacher-class-teaching-on-whiteboard.svg" alt="Sem Desafio"/>
                </div>

                <div class="row">
                    <a style="cursor: pointer" onclick="novoDesafio('novoDesafio','<?= $idSala?>');"><h4>LANCE UM NOVO DESAFIO</h4></a>
                </div>
                <hr style="border:0.2em solid blue; opacity: 0.2">

           <?php else: ?>

                    <div class="row">
                        <h3 class="text-center">DESAFIO ATUAL: </h3><h3> <?= $desafioAtual['nome']?></h3>
                    </div>

                    <div class="row">
                        <h1><?php $valor = 1;

                            echo ($valor < 10)?"0".$valor:$valor;
                            ?></h1>
                        <h3>Concluido</h3>
                    </div>

                    <h5>Data de entrega: <?php

                        $data = explode("-",$desafioAtual['expira']);

                        echo "<br />" . $data[2] . " / " . $data[1] . " / " . $data[0];

                        ?>
                    </h5>

                    <hr style="border:0.2em solid blue; opacity: 0.2">



                <?php endif; ?>


            </div>

                <table class="table table-hover aulaTable">
                    <thead>
                        <tr>
                            <th class="text-center">
                                Desafios Antigos
                            </th>
                        </tr>
                    </thead>
                    <tbody style="cursor: pointer;">
                    <?php if(count($desafio) < 1):
                    ?>
                        <tr>
                            <td>
                                Sem Desafios
                            </td>
                        </tr>
                    <?php else:
                        for($i = 0; $i < count($desafio);$i++):
                    ?>

                        <tr class="desafioTable" id="<?= $desafio[$i]['nome']?>">
                            <td>
                                <?= $desafio[$i]['nome']?>
                            </td>
                        </tr>

                    <?php
                            endfor;
                        endif;
                    ?>

                    </tbody>
                </table>

        </div>
        <div class="col-md-6 backgroundDesafio">
            <h1 class="text-center">Crie Exercicios</h1>

            <form class="form-group text-center" action="javascript:void(0);">
                <h3>Nome do exercicio</h3>
                <input id="nome-exercicio" type="text" class="form-control" />
                <h3>Enunciado do exercicio</h3>
                <textarea id="exercicio" class="form-control"></textarea>

                <p id="previewResposta"></p>
                <h4>Exercicio dinamico</h4>
                <label class="labelDesafio">
                    Não
                    <input id="ex-nao" type="radio" name="dinamico" class="radio-inline" value="nao" onclick="isDinamic(false)">
                </label >
                <label class="labelDesafio">
                  Sim
                  <input id="ex-sim" type="radio" name="dinamico" class="radio-inline" value="sim" checked="true" onclick="isDinamic(true)">
                </label>


                <div id="formula-form">
                    <h3>Formula do exercicio</h3>
                    <input id="formulaExercicio" type="text" class="form-control" />
                </div>

                <h4>Quantidade de alternativas</h4>
                <select id="qtd-alterantivas"  class="form-control">
                    <?php for($i = 4; $i < 11; $i++){
                        printf('<option  value="'.$i.'">'.$i.'</option>');
                    }?>
                </select>

                <div id="input-alterantivas" style="display: none;">
                    <input type="text" class="form-control" placeholder="Alternativas">
                </div>

                <div id="respostaCorreta" style="display: none">
                    <h4>Resposta correta</h4>
                    <?php for($i = 1; $i < 5; $i++){
                        printf('
                    <label style="font-size:2em;"> '.$i.'
                    <input type="radio" name="correta"  value="'.$i.'" />
                    </label>');
                    }?>
                </div>


                <h4>Exercicio publico</h4>
                <label class="labelDesafio">
                    Não
                    <input id="pub-nao" type="radio" name="publico" class="radio-inline" value="nao" onclick="
                     $('#pub-sim').attr('checked','false');
                     $('#pub-nao').attr('checked','true');
                    ">
                </label>
                <label class="labelDesafio">
                    Sim
                    <input id="pub-sim" type="radio" name="publico" class="radio-inline" value="sim" checked onclick="
                     $('#pub-sim').attr('checked','true');
                     $('#pub-nao').attr('checked','false');
                    ">
                </label>


                <button class="form-control" id="gerarExercicio">Exibir</button>
                <button class="form-control" onclick="salvar()">Salvar</button>

            </form>
        </div>

        <div class="col-md-2 ">

            <table  class="table table-hover aulaTable">
                <thead>
                <tr>
                    <th class="text-center">
                        Exercicios Criados
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php if(count($exercicios) <= 0 ): ?>
                <tr class="alunoTable">
                    <td>
                       Nenhum exercicio criado
                    </td>
                </tr>
                <?php
                    else:
                        for ($i=0;$i < count($exercicios);$i++):
                ?>

                <tr class="alunoTable">
                    <td>
                        <?= $exercicios[$i]["titulo"]; ?>
                    </td>
                </tr>

                <?php endfor;
                    endif;
                ?>

                </tbody>
            </table>
        </div>
    </div>

</div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>