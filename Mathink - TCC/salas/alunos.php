<?php
/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 30/04/2016
 * Time: 19:50
 */

require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';

if(!isset($_GET['nome'])){
    header('Location: lista.php');
}


if(isset($_GET['p'])){
    $paginas = intval($_GET['p']);
}else{
    $paginas = 0;
}

$crud = new CRUD;

$nomeDaSala = $_GET['nome'];

$id = $_SESSION['logado'];

$logProfessor =  $crud->select("nm_usuario, nm_nickname","tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario inner join tb_aluno on tb_aluno.cd_usuario = tb_usuario.cd_usuario","WHERE nm_sala = ? AND cd_autoridade =? ",array($nomeDaSala, 5));

foreach ($logProfessor as $dds){
    $nomeProfessor = $dds['nm_usuario'];
    $nickProfessor = $dds['nm_nickname'];
}

$logAlunos = $crud->select("tb_aluno.cd_usuario, nm_nickname","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala INNER JOIN tb_aluno ON tb_aluno.cd_usuario = sala_aluno.cd_usuario","WHERE nm_sala = ? AND cd_autoridade <> ?",array($nomeDaSala,5));

$avaliadores = $crud->select("vl_avaliacao","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala","WHERE nm_sala  =? AND vl_avaliacao IS NOT NULL",array($nomeDaSala));

$qtdAlunos = $logAlunos->rowCount();

if($qtdAlunos>0){
    $alunos = array();
    foreach ($logAlunos as $dds){
        $dados['nome'] = $dds['nm_nickname'];
        $dados['codigo'] = $dds['cd_usuario'];
        array_push($alunos,$dados);
    }
}

$avaliadores = $avaliadores->rowCount();



$isDaSala = $crud->select('cd_autoridade',"tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala","WHERE nm_sala=? AND cd_usuario=? AND cd_autoridade IS NOT NULL",array($nomeDaSala,$id));
if($isDaSala->rowCount()<1){
    header('Location: lista.php');
}else{
    foreach ($isDaSala as $dds){
        $poder = $dds['cd_autoridade'];
    }
}

if($poder != 5){
    header('Location: aula.php?nome='.$nomeDaSala);
}

$acertos = 10;
$erros = 5;
$comp = 0;
$inc = 0;


$media =  round(($acertos * 100) / ($acertos + $erros),2) ;



?>



<!DOCTYPE html>
<html lang="pt-BR" xmlns="http://www.w3.org/1999/html">
<head>
    <base href="..">
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>
    <script src="require/js/ballon.js"></script>
    <script src="salas/require/js/avaliacao_da_sala.js"></script>
    <script src="require/contextMenu/contextMenu.js"></script>
    <link rel="stylesheet" href="require/contextMenu/contextMenu.css" />
    <script type="text/javascript" src="require/js/graficos.js"></script>

    <script>

        $(document).ready(function(){
            writeAvaliacaoBalao(<?php echo $avaliadores?>,<?php echo $qtdAlunos ?>);
            getAvaliacaoImg(<?php echo $qtdAlunos ?>, <?php echo $avaliadores?>);









        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {
            var data = new google.visualization.arrayToDataTable([
                ['', ''],
                ["Total de Alunos", <?=$qtdAlunos?>],
                ["Acertos", <?=$acertos?>],
                ["Erros", <?=$erros?>],
                ["Tarefas completas", <?=$comp?>],
                ["Tarefas incompletas", <?=$inc?>]
            ]);

            var options = {
                width: 500,
                legend: { position: 'none' },
                bars: 'horizontal', // Required for Material Bar Charts.
                bar: { groupWidth: "90%" }
            };

            var chart = new google.charts.Bar(document.getElementById('top_x_div'));
            chart.draw(data, options);
        };

        });

        function setReputacao(){
            <?php
            $avaliadores = $crud->select("vl_avaliacao","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala","WHERE nm_sala = ? AND vl_avaliacao IS NOT NULL",array($nomeDaSala));
            $avaliadores = $avaliadores->rowCount();
            ?>
            $.ajax({
                url:"salas/require/php/changeReputacao.php",
                success: function(post){

                    if(post ==1){
                        console.log('1');
                        writeAvaliacaoBalao(<?php echo $avaliadores+1?>,<?php echo $qtdAlunos ?>);
                    }else{
                        console.log('2');
                        writeAvaliacaoBalao(<?php echo $avaliadores?>,<?php echo $qtdAlunos ?>);
                    }
                }
            });
        }

    </script>
</head>
<body>
<?php require_once '../menu.php' ?>

</header>

<div class="container-fluid">
    <div id="topoSalaDeAula" class="row">
        <div class="col-md-12">
            <i class="fa fa-university"></i>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h1>SALA - <href style="cursor: pointer;" onclick="location.href='salas/aula.php?nome=' + '<?=$nomeDaSala?>'"><?php echo $nomeDaSala ?> </href> </h1>
                    <div class="row"  >
                        <div class="col-md-offset-1 col-md-10" >
                            <h2>Professor: <href style="cursor: pointer;" onclick="location.href='perfil.php?nome=' + '<?= $nickProfessor?>';"><?php echo $nomeProfessor ?></href></h2>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($poder == 5): ?>
                <div id="reputacaoDaSala"></div>
            <?php else: ?>
                <div onclick="setReputacao()" id="reputacaoDaSala"></div>
            <?php endif; ?>
        </div>
    </div>

    
    <?php if($poder == 5):?>
    <div id="topoConfigDeAula" class="row">
        <div class="col-md-12 text-center">

            <img src="salas/require/img/alunos.svg" onclick="location.href='salas/alunos.php?nome='+'<?=$nomeDaSala?>'"/>
            <img src="salas/require/img/challenger.svg" onclick="location.href='salas/desafio.php?nome='+'<?=$nomeDaSala?>'"/>
            <img src="salas/require/img/notificacao.svg" onclick="location.href='salas/notificacao.php?nome='+'<?=$nomeDaSala?>'"/> <span id="notificationSala">0</span>
            <img src="salas/require/img/config.svg" onclick="location.href='salas/config.php?nome='+'<?=$nomeDaSala?>'"/>


        </div>
    </div>
    <?php endif; ?>

<div class="row">
    <div class="col-md-4 ">
        <table  class="table table-hover aulaTable">
            <thead>
            <tr>
                <th class="text-center">
                    Alunos
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if($qtdAlunos > 0):
                for($i = 0; $i < $qtdAlunos;$i++):
                    ?>

                    <tr class="alunoTable" id="<?= $alunos[$i]['nome']?>">
                        <td>
                            <?= $alunos[$i]['nome']?>
                        </td>
                    </tr>

                    <?php
                endfor;
            else: ?>

                <tr class="alunoTable">
                    <td>
                        Sem alunos, adicione!
                    </td>
                </tr>

            <?php endif;?>
            </tbody>
        </table>
    </div>
    <div class="col-md-7 backgrounSalas">
        <h2 class="text-center">Estastistica Geral</h2>
        <div class="row">
            <div class="col-md-12">
                <div id="top_x_div" style="width: 100%; height: 100%;"></div>
            </div>
        </div>
        <div class="row" style="margin-top: 5em">
            <div class="col-md-4" >
                <div class="row">
                    <h3  class="text-center">Acertos</h3>
                </div>
                <div class="row">
                    <h3 class="text-center"><?= $acertos ?></h3>
                </div>
            </div>

            <div class="col-md-4">
                <div class="row">
                    <h3  class="text-center">Erros</h3>
                </div>
                <div class="row">
                    <h3 class="text-center"><?= $erros ?></h3>
                </div>
            </div>

            <div class="col-md-4">
                <div class="row">
                    <h3  class="text-center">Média</h3>
                </div>
                <div class="row">
                    <h3 class="text-center"><?= $media ?>%</h3>
                </div>
            </div>

        </div>
    </div>
</div>

</div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>