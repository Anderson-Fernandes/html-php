<?php
require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';
?>

<?php

$crud = new CRUD;
$id = $_SESSION['logado'];
$idSala = array();
$sala = array();
$nomeDoProfessor = array();


$salas = $crud->select("nm_sala","tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala","WHERE cd_usuario=? AND cd_autoridade = ?",array($id,5));
$quantidadeDeSalasDoUsuario = $salas->rowCount();
$i = 0;
if($quantidadeDeSalasDoUsuario > 0){
    foreach ($salas as $dds){
        $sala[$i] = "<b>" . $dds['nm_sala'] ."</b>";
        $nomeDoProfessor[$i] = "<b>Você</b>";
        $i++;
    }
}


$salas = $crud->select("nm_sala, nm_usuario","tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario","WHERE sala_aluno.cd_usuario = ? AND cd_autoridade <> ?",array($id,5));

$quantidadeDeSalasQueEntrou = $salas->rowCount();
if($quantidadeDeSalasQueEntrou > 0){
    foreach ($salas as $dds){
        $sala[$i] = $dds['nm_sala'];
        $nomeDoProfessor[$i] = $dds['nm_usuario'];
        $i++;
    }
}

$quantidadeDeSalas = $quantidadeDeSalasDoUsuario + $quantidadeDeSalasQueEntrou;

?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <base href="..">
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>

    <script>
        $(document).ready(function () {
           $('.listaSala').click(function(){
              var id = $(this).find('td:first-child').text();
              window.location.href = "salas/aula.php?nome=" + id;
           });
        });
    </script>

</head>
<body>
<?php if(isset($_SESSION['logado'])){
    if(isset($_SESSION['isPrimeiraVez']) && $_SESSION['isPrimeiraVez'] == 1){
        require_once 'tutorial/inicio.html' ;
    }
}?>
    <?php require_once '../menu.php' ?>
        <div id="notice">
            <i class="fa fa-university"></i><h1>Sala!</h1><p>Escolha a sala que deseja visitar</p>
        </div>
    </header>

    <div class="container-fluid">
        <div id="salas" class="row">
            <div class="col-sm-9 col-md-9">
                <div class="table-responsive"style="overflow:auto; " >
                    <table class="table table-hover tabela-comun">
                        <thead>
                            <tr>
                                <td>
                                    <h1>Nome</h1>
                                </td>
                                <td>
                                    <h1>Professor</h1>
                                </td>
                            </tr>
                        </thead>
                        <tbody style="cursor:pointer;">
                            <?php
                            if($quantidadeDeSalas > 0):
                                for($i=0; $i < $quantidadeDeSalas; $i++): ?>
                                <tr class="listaSala">
                                   <td ><?php echo $sala[$i]; ?></td>
                                   <td><?php echo $nomeDoProfessor[$i]; ?></td>
                                </tr>
                            <?php endfor;
                                    else:?>
                                <tr>
                                    <td class="text-center" colspan="2">
                                        Você ainda não participa de nenhuma sala!
                                    </td>
                                </tr>
                            <?php endif; ?>
                             <tr>
                                <td class="text-center" colspan="2" onClick="window.location = 'salas/nova.php'">
                                        <i class="fa fa-plus"></i>Criar sala
                                </td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>
            <a href="salas/buscar.php">
                <div  class="col-sm-3 col-md-3 laminaDescubra" style="min-height: 20em;">
                    <div class="row" style="margin-top: 50%;margin-bottom: 50%">
                       <i class="fa fa-sign-in"></i><h1>Descubra!</h1><p>Entre em novas salas!</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>