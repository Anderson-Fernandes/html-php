<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 04/06/2016
 * Time: 13:00
 */


require_once '../../../../require/class/conDB.class.php';
require_once  '../../../../require/php/logado.php';

$crud = new CRUD;

$nome = $_POST['nome'];
$exercicios = $_POST['exercicios'];
$id = $_SESSION['logado'];
$expira = $_POST['expira'];

$dono = $crud->select("cd_sala_aluno","sala_aluno",'WHERE cd_usuario =? AND cd_autoridade = ?',array($id,5));


if($dono->rowCount() > 0){

    foreach ($dono as $dds){
        $codigo = $dds['cd_sala_aluno'];
    }

    $crud->insert("tb_desafio","cd_sala_aluno=?, dt_desafio=curdate(),nm_desafio=?, nm_exercicio=?, dt_expira=?",array($codigo,$nome,$exercicios,$expira));
}


