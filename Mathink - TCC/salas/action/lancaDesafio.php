<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 04/06/2016
 * Time: 10:48
 */



require_once '../../require/class/conDB.class.php';
require_once '../../require/php/logado.php';

$crud = new CRUD;

$id = $_SESSION['logado'];

$logExercicios = $crud->select("cd_exercicio,cd_usuario, nm_titulo,ds_exercicio,nm_alternativas","tb_exercicio_professor","WHERE cd_usuario = ? OR ic_privacidade = ? ORDER BY cd_usuario = ? DESC, nm_titulo DESC",array($id,1,$id));


$exercicios = array();
foreach ($logExercicios as $dds){
    $info ['usuario'] = $dds['cd_usuario'];
    $info ['codigo'] = $dds['cd_exercicio'];
    $info['titulo'] = ($info['usuario'] == $id)?'<b>'.$dds['nm_titulo'].'</b>':$dds['nm_titulo'];
    array_push($exercicios,$info);
}

?>

<style>
    .selected{
        background-color: #9BA9C7;
    }

    #desafio-container{
        font-size:2em;
    }

    #tabela-container{
        position:relative;
        overflow-y: scroll;
        height: 250px;
    }

</style>

<script>
    function lancaDesafio(sala){
        var objSelecionado =$("#tabelaDeDesafios .selected");
        var exercicios = new Array();

        var date = $('#entrega').val();
        date = date.split("/");



        $.each(objSelecionado,function(i,html){
            if($(html).hasClass('selected')){
                exercicios.push($(html).attr('id'));
            }
        });

        var desafio = $("#nomeDesafio").val();
        if(desafio.length < 1){
            alert('Preencha o nome do desafio');
            $("#nomeDesafio").focus();
            return;
        }


        if(date.length != 3){
            alert("Data invalida");
            return
        }else{
            moment.locale('pt-BR');
            var expira = date[2];
            expira += "-" + date[1];
            expira += "-" + date[0];

            if(moment(expira).isValid()){
                var dataAtual = moment().format("YYYY-MM-DD");

                if(moment(dataAtual).isSameOrAfter(expira)){
                    alert("A data de entrega deve ser 1 dia depois do dia do lançamento");
                    return;
                }
            }else{
                alert("Data invalida");
                return
            }


        }


       if(exercicios.length > 0){
           exercicios = JSON.stringify(exercicios);
           $.ajax({
               url:'salas/action/require/php/novoDesafio.php',
               method:"POST",
               data:{id:sala,nome:desafio, exercicios:exercicios, expira:expira},
               success:function(data){
                   if(data){
                       console.log(data);
                   }else{
                       location.reload();
                   }
               }
           });
       }else{
           alert("Nenhum exercicio selecionado, por favor, selecione");
           return;
       }

    }
</script>

<script async src="require/js/jquery.mask.js"></script>
<script src="require/js/moment.js"></script>

<div id="desafio-container" class="col-md-12 text-center">
    <div class="row">
        <h2 class="text-center">Novo desafio</h2>
        <label>
            Nome do desafio
            <input id="nomeDesafio" class="form-control" type="text">
        </label>
    </div>

    <div class="row">
        <h4>Selecione os exercicos</h4>
        <p>Os desafios serão compostos por os exercicios selecionado</p>
        <div class="col-md-6 col-md-offset-3">
            <div id="tabela-container">
                <table id="tabelaDeDesafios" class="table table-hover">
                    <tbody style="cursor: pointer;">
                    <?php if(count($exercicios) < 0 ): ?>
                        <tr>
                            <td>
                                Sem exercicios
                            </td>
                        </tr>
                        <?php
                    else:
                        for ($i=0;$i < count($exercicios);$i++):
                            ?>

                            <tr  id="<?= $exercicios[$i]["codigo"] ?>">
                                <td>
                                    <?= $exercicios[$i]["titulo"]; ?>
                                </td>
                            </tr>

                        <?php endfor;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <label> Data de entrega
        <input type="text" id="entrega" class="form-control" value="
        <?php
           date_default_timezone_set("America/Sao_Paulo");
            echo date("dmY",mktime(0,0,0,date('m'),date('d')+1,date('Y')));

        ?>
        ">
    </label>
    <div class="row">
        <button class="btn btn-default" onclick="location.reload();">Cancelar</button>
        <button class="btn btn-default" onclick="lancaDesafio()">Lançar</button>
    </div>


</div>


<script>

    $("#tabelaDeDesafios tbody tr").click(function(){
        $(this).toggleClass("selected");
    });

    $('#entrega').mask('00/00/0000');
</script>
