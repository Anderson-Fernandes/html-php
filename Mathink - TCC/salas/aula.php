<?php
/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 30/04/2016
 * Time: 19:50
 */

require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';

if(!isset($_GET['nome'])){
    header('Location: lista.php');
}


if(isset($_GET['p'])){
    $paginas = intval($_GET['p']);
}else{
    $paginas = 0;
}

$crud = new CRUD;

$nomeDaSala = $_GET['nome'];

$id = $_SESSION['logado'];

$logProfessor =  $crud->select("nm_usuario, nm_nickname","tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala inner join tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario inner join tb_aluno on tb_aluno.cd_usuario = tb_usuario.cd_usuario","WHERE nm_sala = ? AND cd_autoridade =? ",array($nomeDaSala, 5));

foreach ($logProfessor as $dds){
    $nomeProfessor = $dds['nm_usuario'];
    $nickProfessor = $dds['nm_nickname'];
}

$logAlunos = $crud->select("tb_aluno.cd_usuario, nm_nickname","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala INNER JOIN tb_aluno ON tb_aluno.cd_usuario = sala_aluno.cd_usuario","WHERE nm_sala = ? AND cd_autoridade <> ?",array($nomeDaSala,5));

$avaliadores = $crud->select("vl_avaliacao","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala","WHERE nm_sala  =? AND vl_avaliacao IS NOT NULL",array($nomeDaSala));

$qtdAlunos = $logAlunos->rowCount();

if($qtdAlunos>0){
    $alunos = array();
    foreach ($logAlunos as $dds){
        $dados['nome'] = $dds['nm_nickname'];
        $dados['codigo'] = $dds['cd_usuario'];
        array_push($alunos,$dados);
    }
}

$avaliadores = $avaliadores->rowCount();


$logCurrentChallenger = $crud->select("cd_desafio,nm_desafio,dt_desafio,dt_expira","tb_desafio inner join sala_aluno on sala_aluno.cd_sala_aluno = tb_desafio.cd_sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala ","WHERE nm_sala=? AND dt_expira >= curdate()",array($nomeDaSala));
$desafioAtual = array();
if($logCurrentChallenger->rowCount () > 0){
    foreach ($logCurrentChallenger as $dds){
        $desafioAtual['codigo'] = $dds['cd_desafio'];
        $desafioAtual['nome'] = $dds['nm_desafio'];
        $desafioAtual['data'] = $dds['dt_desafio'];
        $desafioAtual['expira'] = $dds['dt_expira'];
		
    }


}



$isDaSala = $crud->select('cd_autoridade',"tb_sala inner join sala_aluno on tb_sala.cd_sala = sala_aluno.cd_sala","WHERE nm_sala=? AND cd_usuario=? AND cd_autoridade IS NOT NULL",array($nomeDaSala,$id));
if($isDaSala->rowCount()<1){
    header('Location: lista.php');
}else{
    foreach ($isDaSala as $dds){
        $poder = $dds['cd_autoridade'];
    }
}




?>



<!DOCTYPE html>
<html lang="pt-BR" xmlns="http://www.w3.org/1999/html">
<head>
    <base href="..">
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>
    <script src="require/js/ballon.js"></script>
    <script src="salas/require/js/avaliacao_da_sala.js"></script>
    <script src="salas/require/js/post.js"></script>
    <script src="require/js/textAreaAutoSize.js"></script>
    <script src="require/contextMenu/contextMenu.js"></script>
    <link rel="stylesheet" href="require/contextMenu/contextMenu.css" />
    <script src="require/js/notification.js"></script>
    <script src="salas/require/js/novoDesafio.js"></script>

    <script>

        $(document).ready(function(){
            var nomeDaSala = "<?php echo $nomeDaSala ?>";
            getPost(<?php echo $paginas ?>, nomeDaSala);
            writeAvaliacaoBalao(<?php echo $avaliadores?>,<?php echo $qtdAlunos ?>);
            getAvaliacaoImg(<?php echo $qtdAlunos ?>, <?php echo $avaliadores?>);

            setInterval(function(){
                getCountNotification('<?= $nomeDaSala ?>','sala','notificationSala');
            },1000);
        });

        function setReputacao(){
            <?php
            $avaliadores = $crud->select("vl_avaliacao","sala_aluno INNER JOIN tb_sala ON sala_aluno.cd_sala = tb_sala.cd_sala","WHERE nm_sala = ? AND vl_avaliacao IS NOT NULL",array($nomeDaSala));
            $avaliadores = $avaliadores->rowCount();
            ?>
            $.ajax({
                url:"salas/require/php/changeReputacao.php",
                success: function(post){

                    if(post ==1){
                        console.log('1');
                        writeAvaliacaoBalao(<?php echo $avaliadores+1?>,<?php echo $qtdAlunos ?>);
                    }else{
                        console.log('2');
                        writeAvaliacaoBalao(<?php echo $avaliadores?>,<?php echo $qtdAlunos ?>);
                    }
                }
            });
        }

    </script>


    <script>
        $(document).ready(function(){
        
            $("#formPost").keydown(function(e){
                // 13 = ENTER
                var mensagem = $("#mensagemPost").val();
                mensagem.trim("\n");
                var titulo = $("#tituloPost").val();
                titulo.trim();

                if(e.keyCode == 13 && !e.shiftKey){
                    if(mensagem.length < 1){
                        alert("Digite uma mensagem");
                        $("#mensagemPost").focus();
                    }else if(titulo.length < 1){
                        alert("Digite um titulo");
                        $("#tituloPost").focus();

                    }else{
                        var titulo = $("#tituloPost").val();
                        var mensagem = $("#mensagemPost").val();

                        createPost(titulo,mensagem,'<?php echo $poder ?>','<?php echo $id ?>','<?php echo $nomeDaSala?>');

                    }
                }
            });

            var id;

            var setId = function(newId){
                id = newId;
            }


            var trocaPagina = function(){
                location.href="perfil.php?nome=" + id;
            }

            var menu = [{name:"Sem Opções"}];
            $('.alunoTable').contextMenu(menu,{triggerOn:'contextmenu'});


            <?php if($poder == 5):?>
            $(".alunoTable").contextmenu(function(){
                     setId($(this).attr('id'));
                     menu = [{
                        name: 'Ver Perfil',
                        title: 'Visualizar perfil do aluno',
                        fun: function () {
                           trocaPagina();
                        }
                    }, {
                        name: 'Ver Progresso',
                        title: 'Visualizar estatística do aluno',
                        fun: function () {
                            alert('Em construção')
                        }
                    },{
                        name: 'Remover Aluno',
                        title:"Remover o aluno desta sala",
                        fun: function(){
                            alert('Em Construção')
                        }
                    }, {
                        name: '',
                        title: ''
                    },{
                        name:'Novo Aluno',
                        title:'Adicionar novo aluno',
                        fun:function(){
                            alert("em construção");
                        }
                    }];


                $('.alunoTable').contextMenu(menu,{triggerOn:'contextmenu'});
                });
            <?php endif; ?>



        });
    </script>

</head>
<body>
<?php require_once '../menu.php' ?>

</header>

<div class="disableBackground"></div>
<div id="novoDesafio"></div>

<div class="container-fluid">
   <div id="topoSalaDeAula" class="row">
       <div class="col-md-12">
                <i class="fa fa-university"></i>
                <div class="row">
                    <div class="col-md-offset-1 col-md-10">
                        <h1>SALA - <href style="cursor: pointer;" onclick="location.href='salas/aula.php?nome=' + '<?=$nomeDaSala?>'"><?php echo $nomeDaSala ?> </href> </h1>
                       <div class="row"  >
                           <div class="col-md-offset-1 col-md-10" >
                              <h2>Professor: <href style="cursor: pointer;" onclick="location.href='perfil.php?nome=' + '<?= $nickProfessor?>';"><?php echo $nomeProfessor ?></href></h2>
                           </div>
                       </div>
                    </div>
                </div>
           <?php if($poder == 5): ?>
             <div id="reputacaoDaSala"></div>
           <?php else: ?>
            <div onclick="setReputacao()" id="reputacaoDaSala"></div>
           <?php endif; ?>
       </div>
    </div>

    <?php if($poder == 5):?>

    <div id="topoConfigDeAula" class="row"  >
        <div class="col-md-12 text-center" >

            <img src="salas/require/img/alunos.svg" onclick="location.href='salas/alunos.php?nome='+'<?=$nomeDaSala?>'"/>
            <img src="salas/require/img/challenger.svg" onclick="location.href='salas/desafio.php?nome='+'<?=$nomeDaSala?>'"/>
            <img src="salas/require/img/notificacao.svg" onclick="location.href='salas/notificacao.php?nome='+'<?=$nomeDaSala?>'"/> <span id="notificationSala">0</span>
            <img src="salas/require/img/config.svg" onclick="location.href='salas/config.php?nome='+'<?=$nomeDaSala?>'"/>


        </div>
    </div>
    <?php endif;?>

    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div style="min-height: 80em" class="col-md-offset-1 col-md-10 backgrounSalas">
                    <form id="formPost">
                        <input id="tituloPost" class="inputFiltroSala hiddenTitulo" type="text" placeholder="Adicione um assunto"/>
                        <textarea col="" class="form-control" id="mensagemPost" placeholder="Deixe uma mensagem para seus alunos." title="Pressione ENTER para enviar o post"></textarea>

                    </form>

                    

                    <div id="listaPostDaSala">

                    </div>
                    <button id="postPage" class="center-block" onclick="getPost('<?php echo $paginas?>','<?php echo $nomeDaSala?>');">Carregar mais post</button>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="col-md-offset-1 col-md-10">
                <div class="row backgrounSalas">
                    <div id="desafioSala" class="col-md-12 text-center">
                        <?php if($poder == 5): ?>
                            <?php if(count($desafioAtual) < 1):?>
                        <div class="row">
                           <h3 class="text-center">Desafio</h3>
                        </div>

                        <div class="row">
                          <h3 class="text-center">SEM DESAFIO LANÇADO!</h3>
                        </div>

                        <div class="row">
                            <img src="salas/require/img/maths-teacher-class-teaching-on-whiteboard.svg" alt="Sem Desafio"/>
                        </div>

                        <div class="row">
                            <a style="cursor: pointer" onclick="novoDesafio('novoDesafio','<?= 'a'?>')"><h4>LANCE UM NOVO DESAFIO</h4></a>
                        </div>

                        <div class="row">
                            <a style="cursor: pointer" onclick="location.href='salas/desafio.php?nome=<?=$nomeDaSala?>'" class="text-center">Desafio anteriores</a>
                        </div>
                                <?php else: ?>


                                <div class="row">
                                    <h3 class="text-center">DESAFIO ATUAL: </h3><h3> <?= $desafioAtual['nome']?></h3>
                                </div>

                                <div class="row">
                                    <h1><?php
                                        $valor = $crud->select("*","desafio_aluno","WHERE cd_desafio=?",array($desafioAtual['codigo']))->rowCount();

                                        echo ($valor < 10)?"0".$valor:$valor;
                                        ?></h1>
                                    <h3>Concluido</h3>
                                </div>

                                <h5>Data de entrega: <?php

                                    $data = explode("-",$desafioAtual['expira']);

                                    echo "<br />" . $data[2] . " / " . $data[1] . " / " . $data[0];

                                    ?>
                                </h5>

                                <hr style="border:0.2em solid blue; opacity: 0.2">



                            <?php endif; ?>

                    <?php else: 
					
						if (count($desafioAtual) > 0):?>

                            <div class="row">
                                <h3 class="text-center">Desafio</h3>
                            </div>

                            <div class="row">
                                <h3 class="text-center"><?= $desafioAtual['nome']?></h3>
                            </div>



                            <div class="row">

                                <?php

                                $jaJogou = $crud->select("*","desafio_aluno","WHERE cd_desafio=? AND cd_aluno=?",array($desafioAtual['codigo'],$_SESSION['logado']));

                                if($jaJogou->rowCount() <1):

                                ?>
                                <img style="cursor:pointer;" src="salas/require/img/play.svg" alt="Jogar" onclick="location.href='salas/jogar/jogando.php?game=<?php

                                $id = base64_encode($desafioAtual['codigo']);
                                $expira = base64_encode($desafioAtual['data']);
                                $sala = base64_encode($nomeDaSala);
                                echo $id;

                                ?>&exp= <?=$expira?>&sl=<?=$sala?>'"/>
                            </div>

                            <div class="row">
                                <a style="cursor: pointer" onclick="location.href='salas/jogar/jogando.php?game=<?php

                                $id = base64_encode($desafioAtual['codigo']);
                                $expira = base64_encode($desafioAtual['data']);
                                $sala = base64_encode($nomeDaSala);
                                echo $id;

                                ?>&exp= <?=$expira?>&sl=<?=$sala?>'"><h4>Jogar</h4></a>

                                <?php else:

                                   foreach ($jaJogou as $ddsDoJogo){
                                       $acertosAntigo = $ddsDoJogo['qtd_acertos'];
                                   }

                                    ?>
                                    <h2>Você acertou:</h2>
                                    <h1 style="font-size: 9em"><?=$acertosAntigo?></h1>
                                    <h4>Espera o proximo desafio</h4>
                                <?php endif;?>
                            </div>

                            <div class="row">

                                <?php

                                $valor = $crud->select("*","desafio_aluno","WHERE cd_desafio=?",array($desafioAtual['codigo']))->rowCount(); ?>
                                <h5 class="text-center">Pessoas que ja concluiram: <?= $valor ?></h5>
                            </div>

                    <?php else: ?>
					
					 <div class="row">
                           <h3 class="text-center">SEM DESFIO</h3>
                        </div>

                        <div class="row">
                          <h3 class="text-center">Primeiro desafio ainda não foi lançado</h3>
                        </div>

                        <div class="row">
                            <img src="salas/require/img/maths-teacher-class-teaching-on-whiteboard.svg" alt="Sem Desafio"/>
                        </div>

                     
					
						<?php endif;
					endif; ?>

                    </div>
                </div>

                <div class="row backgrounSalas">
                    <h3 class="text-center">
                        <?= ($poder == 5)?"Gestão de Alunos":"Colegas de classe";?>
                        </h3>
                        <table class="table table-hover aulaTable">
                            <thead>
                                <tr>
                                    <th class="text-center">
                                        Alunos
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if($qtdAlunos > 0):
                                      for($i = 0; $i < $qtdAlunos;$i++):
                            ?>

                                <tr class="alunoTable" id="<?= $alunos[$i]['nome']?>">
                                    <td>
                                        <?= $alunos[$i]['nome']?>
                                    </td>
                                </tr>

                            <?php
                                 endfor;
                               else: ?>

                            <tr class="alunoTable">
                                <td>
                                    Sem alunos, adicione!
                                </td>
                            </tr>

                            <?php endif;?>
                            </tbody>
                        </table>
                </div>
            </div>

        </div>
    </div>

</div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                            <a href="index.php">Inicio</a>
                            <a href="index.php">Desafio</a>
                            <a href="index.php">Contato</a>
                            <a href="index.php">Sobre</a>
                    </nav>
            </div>

        </div>
    </div>
    </div>
</footer>
</body>
</html>