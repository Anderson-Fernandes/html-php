<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 09/05/2016
 * Time: 18:32
 */


require_once 'require/php/logado.php';
require_once 'require/class/conDB.class.php';
require_once 'perfil/require/php/marketItens.php';

$crud = new CRUD;
$pessoa = '';

if(isset($_GET['nome'])){
    $logPerfil = $_GET['nome'];
    $logPerfil = $crud->select('tb_usuario.cd_usuario, nm_nickname, nm_usuario,nr_level,vl_points,nm_color','tb_aluno inner join tb_usuario on tb_aluno.cd_usuario = tb_usuario.cd_usuario','WHERE nm_nickname = ?',array($logPerfil));
}else{
    $logPerfil = $_SESSION['logado'];
    $logPerfil = $crud->select('tb_usuario.cd_usuario, nm_nickname, nm_usuario,nr_level,vl_points,nm_color','tb_aluno inner join tb_usuario on tb_aluno.cd_usuario = tb_usuario.cd_usuario','WHERE tb_usuario.cd_usuario = ?',array($logPerfil));
}
$cor = null;
if($logPerfil->rowCount() > 0){
    foreach ($logPerfil as $dds){
        $id = $dds['cd_usuario'];
        $nomeCompleto = $dds['nm_usuario'];
        $nickname = $dds['nm_nickname'];
        $level = $dds['nr_level'];
        $pontos = $dds['vl_points'];
        $cor = $dds['nm_color'];
    }
}else{
    header("Location: perfil.php?ERRO");
}
$idPerfil = $id;
if($id == $_SESSION['logado']){
    $pessoa = 'dono';
}


if($pessoa != 'dono' ){
    $logAmigo = $crud->select('*','tb_amigo','WHERE cd_usuario_amigo = ? AND cd_amigo_usuario= ? OR cd_usuario_amigo= ? AND cd_amigo_usuario = ?',array($id,$_SESSION['logado'],$_SESSION['logado'],$id));
    if($logAmigo->rowCount() > 0){
        $pessoa="amigo";
    }
}

$perfilLampi = getPerfilImg($id);


?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>
    <script src="require/js/romano.js"></script>
    <script src="require/js/perfil/post.js"></script>
    <script src="perfil/require/js/amizade.js"></script>


    <?php
        if($cor != null){
            printf('<link rel="stylesheet" href="require/css/perfil/cores/' . $cor . '.css" />');
        }else{
            require_once "tutorial/cores.html";
        }
    ?>


    <?php if(isset($_GET['ERRO'])): ?>
    <script>
        alert('Perfil não encontrado!');
    </script>
    <?php endif; ?>



    <script>
        $(document).ready(function(){
            
           getResposta('<?= $id ?>','<?= $pessoa ?>');


            $("#escreverMensagemPerfil").keydown(function(e){
                // 13 = ENTER
                var mensagem = $("#escreverMensagemPerfil").val();
                mensagem.trim("\n");

                if(e.keyCode == 13 && !e.shiftKey){
                    if(mensagem.length < 1){
                        alert("Digite uma mensagem");
                        $("#escreverMensagemPerfil").focus();
                    }else{
                        createReposta(mensagem,'<?php echo $_SESSION['logado'] ?>','<?php echo $id?>');

                    }
                }
            });


            




        });





        



    </script>



</head>
<body>
    <?php require_once 'menu.php' ?>
    </header>

<div id="perfil" class="container-fluid">
    <div id="bannerPerfil" class="row">
        <div class="col-md-12">
            <img id="lampiPerfil" class="img-responsive" src="perfil/require/img/equips/<?=$perfilLampi?>" />
            <h2 id="nomePerfil">
                <?php
                    echo $nickname;
                    if($pessoa!='dono'):
                        if($pessoa=='amigo'):
                            printf('<i style="margin-left:1em;" class="fa fa-user" aria-hidden="true"></i>');
                        else:
							?>
                            <i style="margin-left:1em;cursor:pointer" class="fa fa-user-plus" aria-hidden="true" onclick="setSolicitacaoAmigo('<?= $idPerfil ?>','<?= $_SESSION['logado'] ?>','<?='enviar'?>');"></i>
                        <?php endif;
                    endif;
                ?></h2>
            <h2 id="nomeCompleto"> <?php  echo htmlspecialchars($nomeCompleto); ?></h2>
        </div>
    </div>

    <?php if($pessoa == 'dono'):?>
        <div id="topoConfigDePerfil" class="row">
            <div class="col-md-12 text-center">

              <!--  <img src="perfil/require/img/amigo.svg" onclick="location.href='perfil/amigo.php'"/> -->
                <img src="perfil/require/img/market.svg" onclick="location.href='perfil/market.php'"/>
                <img src="salas/require/img/notificacao.svg" onclick="location.href='perfil/notificacao.php'"/> <span id="notificationSala">0</span>
                <img src="salas/require/img/config.svg" onclick="location.href='perfil/config.php'"/>


            </div>
        </div>
    <?php endif; ?>




    <div class="row container-fluid">
        <div class="col-md-6">
            <div id="perfilEsquerdo" class="row backgroundPerfil">
                <div id="mensagemPostPerfil">

                </div>


                <div class="row center-block" >
                    <div class="col-md-12">
                        <textarea id="escreverMensagemPerfil" class="form-control" placeholder="Escreva no perfil de <?= $nickname ?>"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-offset-1 col-md-5 text-center backgroundPerfil">
            <div class="row">
                <h3>LEVEL</h3>
                <h1 id="levelRomanoPerfil"></h1>
                <p id="pontosPerfil">Pontos: <?= $pontos ?></p>
            </div>

            <script>
                var romano = romanize(<?php echo $level ?> );
                var divId = document.getElementById("levelRomanoPerfil");
                divId.innerHTML = romano;
            </script>

            <hr class="linhaPerfil">
            <div class="row">
                <h3>Desempenho</h3>
                <div id="proeza">

                </div>
            </div>
        </div>
    </div>
</div>

<footer style="margin-top: 4em">
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>