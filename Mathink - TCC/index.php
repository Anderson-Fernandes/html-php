<?php 
session_start();
require_once 'require/class/conDB.class.php';
     require_once 'perfil/require/php/marketItens.php';

     
?>


<?php
$crud = new CRUD;

$listaUsuario =  $crud->select("tb_usuario.cd_usuario, nm_usuario, vl_points, nr_level, nm_nickname"
							  ,"tb_aluno INNER JOIN tb_usuario ON tb_aluno.cd_usuario = tb_usuario.cd_usuario",
								"WHERE nm_nickname is not null ORDER BY vl_points desc limit 3",array());

$i = 0;
foreach ($listaUsuario as $logListaUsuario){
		$usuario[$i]['id'] = $logListaUsuario['cd_usuario'];
		$usuario[$i]["nome"] = $logListaUsuario['nm_nickname'];
		$usuario[$i]["level"] = $logListaUsuario['nr_level'];
		$usuario[$i]["pontos"] = $logListaUsuario['vl_points'];
		$i += 1;
}

$fotoPerfil = ['jogadores/img/user2.png','jogadores/img/user1.png','jogadores/img/user3.png'];

$posicao = ['jogadores/img/first-place-medal.svg','jogadores/img/medal-with-number-two.svg','jogadores/img/medal.svg'];

?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8" />
	<meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
	<title>Mathink</title>
    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>
    
				
	
</head>
<body>
<?php if(isset($_SESSION['logado'])){
    if(isset($_SESSION['isPrimeiraVez']) && $_SESSION['isPrimeiraVez'] != 0){
        require_once 'tutorial/inicio.html' ;
    }else{
		require_once 'menu.php';
	}
}?>

<div class="row" style="background-color:#9AA3D6">
			
			<div class="col-md-12">
				<?php if(!isset($_SESSION['logado'])):?>
				<button id="entrarInicial" onclick="location.href='cadastro.php'" class="btn btn-default" style=
				"float:right;margin-right:4em;	font-size:3em; background-color:#e1e1e1"> Entre agora </button>
				<?php else: ?>
				<button id="entrarInicial" onclick="location.href='logoff.php'" class="btn btn-default" style=
				"float:right;margin-right:4em;	font-size:3em; background-color:#e1e1e1">Sair</button>
				<?php endif;?>
			</div>
		</div>
                <div id="banner">
                    <img class="img-responsive" src="require/img/banner.jpg" alt="Banner" />
                </div>
	</header>
    
	<div id="conteudoPaginaIncial" class="container-fluid">
		
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center conteudo-titulo">Quer aprender de modo fácil?</h2>
            </div>
        </div>
        <div  class="row">

            <div class="col-md-4">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <i class="fa fa-gamepad san "></i><h2>Aprenda jogando!</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="text-center col-md-12">
                        <p>No Mathink você aprende jogando, se divertindo e competitindo!</p>
                    </div>
                </div>
            </div>


            <div class="col-md-4">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <i class="fa fa-trophy san"></i><h2>Seja o melhor!</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="text-center col-md-12">
                        <p>Será que você domina a Matemática? Entre e veja se você é o melhor do mundo.</p>
                    </div>
                </div>

            </div>


            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <i class="fa fa-users san"></i><h2> Desafie seus amigos!</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="text-center col-md-12">
                        <p>Veja quem domina a Matemática! Desafie seus amigos para uma batalha de números!</p>
                    </div>
                </div>


            </div>
        </div>

        <div id="rankingInicial" class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center conteudo-titulo ">Fique entre os melhores</h2>
                    </div>
                </div>


                                 <div id="melhoresJogadores" class="row">
                    
                    <?php for($i = 1; $i < 3; ): ?>
                    <div class="col-md-4">

                        <div class="informacaoJogador">
                            <div class="row">
								<div class=" col-md-12 fotoJogador">
									<?php printf ('<img src="'.$posicao[$i].'" alt="Foto"><img src="perfil/require/img/equips/'.getPerfilImg($usuario[$i]['id']).'" alt="Foto">');?>
								</div>
							</div>
							<div class="row ">
                                <div class=" col-md-12 ">
                                    <?php printf ('<h3 class="text-center">'.$usuario[$i]['nome'].'</h3>');?>
                                    <div class="row">
                                        <div id="levelPaginaInicio" class="col-md-12 text-center">
                                            <?php printf ('<p style="font-size:3.5em; padding-top:0.2em;">Level '.$usuario[$i]['level'].'</p>');?>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <p>Pontuação:<?=$usuario[$i]['pontos'];?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
						if($i == 1){
							$i = 0; 
						}else if($i == 0){
							$i = 2;
						}else if ($i == 2){
							break;
						}
					endfor; ?>
                </div>
            </div>
        </div>
		<?php if(!isset($_SESSION['logado'])): ?>
        <div id="entrarPaginaInicial" class="row">
            <div class="col-md-12">
                <h2>Vamos aprender matemática!</h2>
                <button class="center-block btn btn-warning" onclick="window.location.href='cadastro.php'">Entre agora!</button>
            </div>
        </div>
		<?php endif; ?>
    </div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>