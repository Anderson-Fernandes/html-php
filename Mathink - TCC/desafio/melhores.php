<?php
require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';

?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <base href="..">
	<meta charset="UTF-8">
	<meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
	<title>Mathink</title>
        <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
	<!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
	<link rel="icon" href="require/img/logo/mathink.ico">
	<link rel="stylesheet" href="require/css/master.css">
	<script src="require/js/loginForm.js"></script>
	<script src="require/js/popup.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>



    <style>
        .lamina i{
            font-size: 1em;
        }

    </style>


</head>
<body>

<?php require_once '../menu.php' ?>
		<div id="notice">
			<i class="fa fa-trophy"></i><h1>Usuários!</h1><p>Verifique sua posição no ranking! Verifique se seus alunos estão online para lançar uma prova!</p>
		</div>
	</header>
    
    <div class="container-fluid">
        <div id="escolha" class="row">
             <a class="aNoStyle" href="desafio/online.php">
                <div class="col-md-4 laminaSelected">
                     <h1><i class="fa fa-users"></i>&nbsp Jogadores online</h1>
                 </div>
            </a>
            <a class="aNoStyle" href="desafio/melhores.php">
                <div class="col-md-4 laminaNoHover">
                    <h1><i class="fa fa-align-left"></i>&nbsp Ranking</h1>
                </div>
            </a>
            <a class="aNoStyle" href="desafio/amigos.php">
                <div class="col-md-4 laminaSelected">
                    <h1><i class="fa fa-user"></i>&nbsp Amigos</h1>
                </div>
            </a>
        </div>
        
	</div>
    
        <div id="conteudo-tabela">
            <?php require_once 'tabelas/tableRank.php' ?>
        </div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>