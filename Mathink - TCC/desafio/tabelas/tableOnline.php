<?php
require_once '../require/class/ConDB.class.php';

$crud = new CRUD();
$jogador = $_SESSION['logado'];

$salasDoUser = $crud->select("cd_sala","tb_sala","WHERE cd_dono=?",array($jogador));
$salasQueOUserEntrou = $crud->select("cd_sala","sala_aluno","WHERE cd_aluno=?",array($jogador));

$salas = [];
$usuario = [];


function guardaNaSala($sql, $salas){
    if($sql->rowCount() > 0){
        foreach ($sql as $dds){
            array_push($salas,$dds['cd_sala']);
        }
        return $salas;
    }

        return $salas;
}


$salas = guardaNaSala($salasDoUser, $salas);
$salas = guardaNaSala($salasQueOUserEntrou,$salas);




function sameRoom($idAmigo, $salasDoJogadorOnline){
    $crud = new CRUD();
    $listaSalas = $crud->select("cd_sala","sala_aluno","WHERE cd_aluno=?",array($idAmigo));

    if($listaSalas->rowCount() > 0){

        foreach ($listaSalas as $dds){
            $salas = $dds['cd_sala'];
        }

        if(in_array($salas,$salasDoJogadorOnline)){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }


}
$listaUsuario =  $crud->select("tb_usuario.cd_usuario, nm_nickname, nr_level","tb_usuario inner join tb_aluno on tb_usuario.cd_usuario = tb_aluno.cd_usuario","WHERE curtime()-tm_online > -60 AND curtime()-tm_online <60 ORDER BY tm_online;",array());

$i = 0;
foreach ($listaUsuario as $logListaUsuario){
    $usuario[$i]['id'] = $logListaUsuario['cd_usuario'];
    $usuario[$i]["nome"] = ($usuario[$i]['id'] != $jogador)?$logListaUsuario['nm_nickname']:"<b>".$logListaUsuario['nm_nickname']."</b>";
    $usuario[$i]["level"] = $logListaUsuario['nr_level'];
    $i += 1;
}


function amigo($idAmigo){
    $idUser = $_SESSION['logado'];
    $crud = new CRUD;
    $retorno = false ;
    $isFriend = $crud->select("*","tb_amigo","WHERE cd_usuario_amigo =? OR cd_amigo_usuario=?",array($idUser,$idUser));
    foreach ($isFriend as $logHasFriend) {
        if ($idUser != $logHasFriend['cd_usuario_amigo']) {
            if ($logHasFriend['cd_usuario_amigo'] == $idAmigo) {
                return true;
            } else {
                $retorno = false;
            }
        } else if ($idUser != $logHasFriend['cd_amigo_usuario']) {
            if ($logHasFriend['cd_amigo_usuario'] == $idAmigo) {
                return true;
            } else {
                $retorno = false;
            }
        }
    }
    return $retorno;
}


?>

<div id="desafio">
    <div class="table-responsive"style="overflow:auto; " >
        <table class="table table-hover tabela-comun">
            <thead>
                <tr>
                    <th width=80%>
                        <h1>Nome</h1>
                    </th>
                    <th>
                        <h1>Nivel</h1>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(count($usuario) > 0):
                      for($i=0;$i<count($usuario);$i++):?>
                        <tr onClick='location.href="perfil.php?nome="+"<?=strip_tags($usuario[$i]['nome'])?>";' style="cursor:pointer">
                            <td>
                                <?php
                                if($usuario[$i]['id'] != $jogador){
                                    $friend = (amigo($usuario[$i]['id']))?"fa fa-user":"fa fa-user-plus";
                                    $room =(sameRoom($usuario[$i]['id'],$salas))?"fa fa-university":"fa fa-globe";

                                }else{
                                    $friend = "";
                                    $room = "";

                                }

                                printf('<p>'.$usuario[$i]['nome'].'<i class="'.$friend.'"></i></p>');?>
                            </td>
                            <td>
                                <?php printf('<p>'.$usuario[$i]['level'].'<i class="'.$room.'"></i></p>');?>

                            </td>
                         </tr>
                <?php endfor;
                    else:$frase = "'Em contrução...'";
                    printf('<tr onClick="alert('.$frase.');" style="cursor: pointer">
                                    <td colspan="2" style="text-align: center"> <h2> Ninguem Online! </h2>	</td>
                                </tr>');
                    endif;?>
            </tbody>
        </table>
    </div>
</div>
