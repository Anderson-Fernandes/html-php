<?php
require_once '../require/class/conDB.class.php';


$crud = new CRUD;
$jogador = $_SESSION['logado'];

$listaUsuario =  $crud->select("tb_usuario.cd_usuario, nm_usuario, vl_points, nr_level, nm_nickname"
							  ,"tb_aluno INNER JOIN tb_usuario ON tb_aluno.cd_usuario = tb_usuario.cd_usuario",
								"WHERE nm_nickname is not null ORDER BY vl_points desc",array());


$i = 0;
foreach ($listaUsuario as $logListaUsuario){
		$usuario[$i]['id'] = $logListaUsuario['cd_usuario'];
		$usuario[$i]["nome"] = ($usuario[$i]['id'] != $jogador)?$logListaUsuario['nm_nickname']:"<b>".$logListaUsuario['nm_nickname']."</b>";
		$usuario[$i]["level"] = $logListaUsuario['nr_level'];
		$usuario[$i]["pontos"] = $logListaUsuario['vl_points'];
		$i += 1;
}




function verClass(){
	$statsClass = rand(0,2);
	if($statsClass == 1){
		return "fa fa-level-up";
	}else if($statsClass == 0){
		return "fa fa-level-down";
	}else{
		return "";
	}

}


$salasDoUser = $crud->select("cd_sala","tb_sala","WHERE cd_dono=?",array($jogador));
$salasQueOUserEntrou = $crud->select("cd_sala","sala_aluno","WHERE cd_aluno=?",array($jogador));

$salas = [];


function guardaNaSala($sql, $salas){
	if($sql->rowCount() > 0){
		foreach ($sql as $dds){
			array_push($salas,$dds['cd_sala']);
		}
		return $salas;
	}

	return $salas;
}


$salas = guardaNaSala($salasDoUser, $salas);
$salas = guardaNaSala($salasQueOUserEntrou,$salas);


function sameRoom($idAmigo, $salasDoJogadorOnline){
	$crud = new CRUD();
	$listaSalas = $crud->select("cd_sala","sala_aluno","WHERE cd_aluno=?",array($idAmigo));

	if($listaSalas->rowCount() > 0){

		foreach ($listaSalas as $dds){
			$salas = $dds['cd_sala'];
		}

		if(in_array($salas,$salasDoJogadorOnline)){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}


}



function amigo($idAmigo){
	$idUser = $_SESSION['logado'];
	$crud = new CRUD;
	$retorno = false ;
	$isFriend = $crud->select("*","tb_amigo","WHERE 	cd_usuario_amigo =? OR cd_amigo_usuario=?",array($idUser,$idUser));
	foreach ($isFriend as $logHasFriend) {
		if ($idUser != $logHasFriend['cd_usuario_amigo']) {
			if ($logHasFriend['cd_usuario_amigo'] == $idAmigo) {
				return true;
			} else {
				$retorno = false;
			}
		} else if ($idUser != $logHasFriend['cd_amigo_usuario']) {
			if ($logHasFriend['cd_amigo_usuario'] == $idAmigo) {
				return true;
			} else {
				$retorno = false;
			}
		}
	}
	return $retorno;
}

?>



<div id="desafio">
	<div class="table-responsive"style="overflow:auto; " >
		<table id="amigoTable" class="table table-hover tabela-comun">
			<thead>
			<tr>
				<th width="5%">
					<h1>Rank</h1>
				</th>
				<th width=70%>
					<h1>Nome</h1>
				</th>
				<th>
					<h1>Nivel</h1>
				</th>
				<th>
					<h1>Pontuação</h1>
				</th>
			</tr>
			</thead>
			<tbody>
			<?php for($i=0; $i < count($usuario); $i++):?>
				<tr onClick='location.href="perfil.php?nome="+"<?=strip_tags($usuario[$i]['nome'])?>";' style="cursor:pointer">
				<td>
					<?php printf('<p style="text-align: center">'.($i+1).'</p>');	?>
				</td>
				<td>
					<?php
					if($usuario[$i]['id'] != $jogador){
						$friend = (amigo($usuario[$i]['id']))?"fa fa-user":"fa fa-user-plus";
						$room =(sameRoom($usuario[$i]['id'],$salas))?"fa fa-university":"fa fa-globe";

					}else{
						$friend = "";
						$room = "";

					}




					printf('<p>'.$usuario[$i]['nome'].'<i class="'.$friend.'"></i></p>');?>
				</td>
				<td>
					<?php printf('<p>'.$usuario[$i]['level'].'<i class="'.$room.'"></i></p>');?>

				</td>
				<td>
					<?php printf('<p>'.$usuario[$i]['pontos'].'<i class="'.verClass().'"></i></p>');	?>
				</td>
				</tr><?php endfor; ?>
			</tbody>
		</table>
	</div>
</div>
