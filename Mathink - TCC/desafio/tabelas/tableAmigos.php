<?php
require_once '../require/class/conDB.class.php';

// Caputrando salas

$salasDoUser = $crud->select("cd_sala","tb_sala","WHERE cd_dono=?",array($_SESSION['logado']));
$salasQueOUserEntrou = $crud->select("cd_sala","sala_aluno","WHERE cd_aluno=?",array($_SESSION['logado']));

$salas = [];

function guardaNaSala($sql, $salas){
	if($sql->rowCount() > 0){
		foreach ($sql as $dds){
			array_push($salas,$dds['cd_sala']);
		}
		return $salas;
	}

	return $salas;
}


$salas = guardaNaSala($salasDoUser, $salas);
$salas = guardaNaSala($salasQueOUserEntrou,$salas);

/*		CAPTURANDO AMIGOS		*/
$crud = new CRUD;
$logAmigos = $crud->select('*','tb_amigo', 'WHERE cd_usuario_amigo =? or cd_amigo_usuario = ?',array($id,$id));

$i=0;
// Capturando ID
$idAmigo = array();
foreach ($logAmigos as $idAmigoLog){
	$idAmigo[$i] = ($id != $idAmigoLog['cd_usuario_amigo'])?$idAmigoLog ['cd_usuario_amigo']:$idAmigoLog['cd_amigo_usuario'];
	$i+=1;
}

// Capturando Nome
$nomeAmigo = array ();
$lvlAmigo = array ();
for($i=0; $i < count($idAmigo); $i++){
	$logAmigos = $crud->select('nm_usuario','tb_usuario','WHERE cd_usuario=?', array($idAmigo[$i]));
	foreach ($logAmigos as $nomeAmigoLog){
		array_push($nomeAmigo,$nomeAmigoLog['nm_usuario']);
	}

	$logAmigos = $crud->select('nr_level','tb_aluno','WHERE cd_usuario=?',array($idAmigo[$i]));
	foreach ($logAmigos as $lvlAmigoLog){
		$lvlAmigo[$i] = $lvlAmigoLog['nr_level'];
	}
}

function sameRoom($idAmigo, $salasDoJogadorOnline){
	$crud = new CRUD();
	$listaSalas = $crud->select("cd_sala","sala_aluno","WHERE cd_aluno=?",array($idAmigo));

	if($listaSalas->rowCount() > 0){

		foreach ($listaSalas as $dds){
			$salas = $dds['cd_sala'];
		}
		if(in_array($salas,$salasDoJogadorOnline)){

			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}


}

?>



<div id="desafio">
	<div class="table-responsive"style="overflow:auto; " >
		<table id="amigoTable" class="table table-hover tabela-comun">
			<thead>
			<tr>
				<th width=80%>
					<h1>Nome</h1>
				</th>
				<th>
					<h1>Nivel</h1>
				</th>
			</tr>
			</thead>
			<tbody>
			<?php
				if(count($idAmigo)>=1):
					for($i=0; $i < count($idAmigo); $i++):?>
						<tr onClick='location.href="perfil.php?nome="+"<?=strip_tags($usuario[$i]['nome'])?>";' style="cursor:pointer">
							<td>
								<?php
									if(sameRoom($idAmigo[$i],$salas)){
										$room = "fa fa-university";
									}else{
										$room = "fa fa-globe";
									}

								printf('<p>'.$nomeAmigo[$i].'<i class="fa fa-user"');?>
							</td>
							<td>
								<?php printf('<p>'.$lvlAmigo[$i].'<i class="'.$room.'"></i></p>');?>
							</td>
						</tr>
					<?php endfor;

			    else:$frase = "'Em contrução...'";
					printf('<tr onClick="alert('.$frase.');" style="cursor: pointer">
								<td colspan="2" style="text-align: center"> <h2> Sem amigos! </h2>	</td>
							</tr>');
				endif;?>
			</tbody>
		</table>
	</div>
</div>
