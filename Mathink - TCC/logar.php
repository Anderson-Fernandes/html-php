<script src="require/js/login.js"></script>

<?php require_once 'require/class/conDB.class.php' ?><!-- Class -->

<div id="login">
    <!-- Verificação online -->
	<?php if (!isset($_SESSION['logado'])): ?>
	   <img src="require/img/menu/login.png" alt="logar">
	<?php else: ?>
	   <img src="require/img/menu/logon.png" alt="logar">
	<?php endif ?>
    <!-- Verificação online -->
			
		<!-- Form LOGAR (OFF) -->
    <?php if (!isset($_SESSION['logado'])): ?>
	   <div id="flutuante" class="balao logar">
		   <div class="col-md-12">

			   <div class="row">
					<div class="col-md-12">
						<p>Login </p>
					</div>
				</div>

			   	<div class="row">
					<div class="col-md-12">
						<form method="POST" action="javascript:void(0)">
							<div class="form-group">
								<input type="email" name="email" placeholder="Email" class="form-control"id="Femail">
								<input type="password" name="senha" placeholder="Senha" class="form-control"id="Fsenha">
								<br>
								<button class="loginForumlario" onclick="fctLoginFloat()">Entrar</button>
							</div>
						</form>
					</div>
				</div>

			   	<div class="row " >
					<div class="col-md-12 ">
							<a href="cadastro.php" style="margin-left: 30%;margin-right: auto">Cadastra-se</a>
					</div>
				</div>

			   	<div class="row">
					<div class="col-md-12">
						<a href="#" style="margin-left:15%;margin-right: auto">Esqueceu a Senha?</a>
					</div>
				</div>
		   </div>

		</div>
        <!-- Form Logar (OFF) -->
        <!-- Form Logar (ON) -->
		<?php else:
		
		// Capitura de Dados

		require_once 'require/class/conDB.class.php';
		$id = $_SESSION['logado'] ;
		$crud = new CRUD;
		$log = $crud->select('nm_usuario','tb_usuario','WHERE cd_usuario =?',array($id));
		if($log->rowCount() >0)
		{
			foreach ($log as $dds) {
			$nome = $dds['nm_usuario'];
			}
		}

		$log = $crud->select('nr_level, vl_cash, vl_exp','tb_usuario, tb_aluno', 'WHERE tb_usuario.cd_usuario =? and tb_aluno.cd_usuario =?',array($id,$id));
		if($log->rowCount()>0)
		{
			foreach ($log as $dds) {
				$levelAluno = $dds['nr_level'];
				$piMoneyAluno = $dds['vl_cash'];
				$experienciaAluno = $dds['vl_exp'];
			}
		}

		$log = $crud->select('nr_level, vl_exp','tb_usuario, tb_professor', 'WHERE tb_usuario.cd_usuario =? and tb_professor.cd_usuario =?',array($id,$id));
		if($log->rowCount()>0)
		{
			foreach ($log as $dds) {
				$levelProfessor = $dds['nr_level'];
				$experienciaProfessor = $dds['vl_exp'];
			}
		}
			// Aluno ou Professor  (Default Maior Lvl)
		$escolha =0;

		if($escolha == 0)
		{
			if(!isset($levelProfessor)){
				$escolha = 1;
			}else{
				$escolha = $levelAluno >= $levelProfessor? 1: 2;
			}
		}

			if($escolha == 1)
			{
				$level = $levelAluno;
				$cash = $piMoneyAluno;
				$exp = $experienciaAluno;
				
			}
			else if ($escolha == 2)
			{
				$level = $levelProfessor;
				$cash = 'z2x';
				$exp = $experienciaProfessor;
			}
		
			// Numero de Amigos
			$logAmigos = $crud->select('*','tb_amigo', 'WHERE cd_usuario_amigo =? OR cd_amigo_usuario=?',array($id,$id));
			$amigos = $logAmigos -> rowCount();

           //Fim Informações
		?>
		<div id="flutuante" class="balao logado">
			<h3>Bem Vindo,<br> <a style="font-size: 0.9em; cursor:pointer" href="perfil.php"><?php print($nome); ?></a></h3>
			<br />
			<p>Level:</p> <?php printf('<a href="#">'.$level.'</a>'); ?>
			<br><br>
			<?php if($cash != 'z2x'): ?><p>π: </p><?php printf('<a href="#">'.$cash.'</a>');endif; ?>
			<br><br>
			<p>Amigos:</p> <?php printf('<a href="desafio/amigos.php">' .$amigos.'</a>'); ?>
			<br><br>
			<p>Conquistas:</p> <?php print('<a href="#"> 5% </a>'); ?>
			<br />
			<a style="font-size: 0.8em" class="logadoLink" href="#">Configuração</a>
			<a  style="font-size: 0.8em; margin-left: 3em" class="logadoLink" href="logoff.php">Sair</a>
		</div>
		<script src="require/js/notification.js"></script>
		<?php endif ?>
     <!-- Form Logar (ON) -->
		
</div>