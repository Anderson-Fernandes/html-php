<?php 
session_start();
require_once 'require/class/conDB.class.php';
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
	<title>Mathink</title>
    	<script src="require/js/jquery.js"></script>
            <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
	<!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
	<link rel="icon" href="require/img/logo/mathink.ico">
	<link rel="stylesheet" href="require/css/master.css">

	<script src="require/js/loginForm.js"></script>
	<script src="require/js/popup.js"></script>

     <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>


</head>
<body>

<?php require_once 'menu.php' ?>
		<a class="aNoStyle" href="selecionarSolo.php">
		<div id="notice">
			<i class="fa fa-bullseye"></i><h1>Sozinho!</h1><p>Escolha o modo de jogo</p>
		</div>
		</a>
	</header>

	

	<div class="container-fluid">
        <div id="conteudo-escolhas" class="row">
            <a href="selecionarTreino.php" class="aNoStyle">
                <div class="col-md-4 lamina">
                    <i class="fa fa-indent"></i><h1>Treino</h1><p>Soma, Multiplicação, Divisão.<br />Você escolhe o conteúdo que quer treinar!</p>
                </div>
            </a>
            <a onclick="alert('Em Construção...');" class="aNoStyle">
                <div class="col-md-4 lamina">
                   <span>
                       <i class="fa fa-sun-o" style="margin:0%"></i>
                       <i class="fa fa-clock-o" style="margin:0%"></i>
                    </span>
                    <h1>Diário</h1><p>Veja se melhorou nos conteúdos em que apresenta maior dificuldade!</p>
                </div>
            </a>
            <a onclick="alert('Em Construção...');" class="aNoStyle">
                <div class="col-md-4 lamina">
                    <i class="fa fa-eye"></i><h1>Desvendando o Mathink!</h1><p>Passe pelos níveis criados para testar sua capacidade lógica!</p>
                </div>
            </a>
        </div>
    </div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>