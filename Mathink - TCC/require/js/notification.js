/**
 * Created by Anderson Fernandes on 30/04/2016.
 */



function finalyNotification(id, tipo, htmlId) {
    setVisualizado(id,tipo,0);;
    jQuery.ajax({
        url: "require/php/finallyNotification.php",
        type: "POST",
        data:{id:id,tipo:tipo},
        success: function (dados) {
            if (dados.trim()) {
                document.getElementById(htmlId).innerHTML = dados;
            } else {
                document.getElementById(htmlId).innerHTML = "<h2 class='text-center'>Sem notificações</h2>";
            }
        }
    });
}

function getCountNotification(id, tipo, htmlId){
    jQuery.ajax({
        url:"require/php/countNotification.php",
        type: "POST",
        data:{id:id,tipo:tipo},
        success: function (dados) {
            if (dados) {
                if(dados ==0){
                    $("#"+htmlId).fadeOut();
                }else{
                    $("#"+htmlId).fadeIn();
                    document.getElementById(htmlId).innerHTML = dados;
                }
            } else {
                document.getElementById(htmlId).innerHTML = "<h2 class='text-center'>Sem notificações</h2>";
            }
        }
    });
}

function setVisualizadoGeral(id, tipo, nivelDeVisualizacao){
    jQuery.ajax({
        url:"require/php/setVisualizado.php",
        type: "POST",
        data:{id:id,tipo:tipo, visualizado:nivelDeVisualizacao},
        success: function (dados) {
            if (dados) {
                console.log(dados);
            }
        }
    });
}

function setVisualizado(id, msgId, tipo){
    jQuery.ajax({
        url:"require/php/setVisualizado.php",
        type: "POST",
        data:{id:id, msgId: msgId, tipo:tipo},
        success: function (dados) {
            if (dados) {
                console.log(dados);
            }
        }
    });
}

function loadFinallyActionNotification(nomeSala,tipo, msgId){
    setVisualizado(nomeSala,msgId,tipo)

   if(tipo == 'sala'){
       if(confirm("Deseja aceitar a entrada desse aluno?")){
           jQuery.ajax({
               url:"salas/require/php/aceitaAluno.php",
               type: "POST",
               data:{msgId:msgId},
               success: function (dados) {
                   if (dados) {
                       console.log(dados);
                   }else{
                       location.reload();
                   }
               }
           });
       }else{
           if(confirm("Remover solicitação do aluno?")){
               jQuery.ajax({
                   url:"salas/require/php/deleteAlunoSolicitacao.php",
                   type: "POST",
                   data:{msgId:msgId},
                   success: function (dados) {
                       if (dados) {
                           console.log(dados);
                       }else{
                           location.reload();
                       }
                   }
               });
           }
       }
   }


    if(tipo == 'amigo'){
        if(confirm("Deseja aceitar a entrada desse aluno?")){
            jQuery.ajax({
                url:"perfil/require/php/aceitarAmigo.php",
                type: "POST",
                data:{msgId:msgId},
                success: function (dados) {
                    if (dados) {
                        console.log(dados);
                    }else{
                        location.reload();
                    }
                }
            });
        }else{
            if(confirm("Remover solicitação do aluno?")){
                jQuery.ajax({
                    url:"perfil/require/php/deletarAmigoSolicitacao.php",
                    type: "POST",
                    data:{msgId:msgId},
                    success: function (dados) {
                        if (dados) {
                            console.log(dados);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }
    }
}
