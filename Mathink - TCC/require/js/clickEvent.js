/**
 * Created by Anderson Luiz on 26/05/2016.
 */

function clickEvent(htmlId, functionShortClick, shortTime, functionMiddleClick, functionLongClick, longTime,effectsMouseDown, effectsMouseUp){

    const SHORT_CLICK = 1;
    const MIDDLE_CLICK = 2;
    const LONG_CLICK = 3;

    var button = document.getElementById(htmlId);

    var timeStart, timeEnd;

    var timer;

    var timeStatus = SHORT_CLICK;



    function statusClick(){

        clearTimeout(timer);
        effectsMouseUp();
        if(timeEnd - timeStart >= longTime){
            timeStatus = LONG_CLICK;
        }else if(timeEnd - timeStart <= shortTime){
            timeStatus = SHORT_CLICK;
        }else if (!isNaN((timeEnd - timeStart))){
            timeStatus = MIDDLE_CLICK;
        }else{
            timeStatus = "ERRO";
            console.log("Tempo indefinido");
        }

        console.log("Start = "+ timeStart+ " \nEnd = " + timeEnd + "\ndiff " + (timeEnd - timeStart) + "\nStatus " + timeStatus);


        timeStart = NaN;
        timeEnd = NaN;
    }

    button.onmousedown = function(){
        timeStart = new Date().getTime();
        effectsMouseDown();
        timer = setTimeout(functionLongClick
            ,1000);
    }

    button.onmouseup = function(){
        timeEnd = new Date().getTime();
        statusClick();
        action();
    }

    button.onmouseout = function(){
        
        statusClick();
    }

    function action(){
        if(timeStatus == SHORT_CLICK){
            functionShortClick();
        }

        if(timeStatus == MIDDLE_CLICK){
            functionMiddleClick();
        }

        if(timeStatus == LONG_CLICK){
            functionLongClick();
        }
    }

}
