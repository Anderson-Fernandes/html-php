/**
 * Created by Anderson Luiz on 26/05/2016.
 */


function setCookie(cnome,cvalor,tempo){
    var data = new Date();
    data.setTime(data.getTime() + (tempo*24*60*60*1000));
    var expira = "expires=" + data.toGMTString();
    document.cookie = cnome+"="+cvalor+"; expires="+expira +";path=/";

}

function getCookie(cnome){
    var nome = cnome + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++){
        var c = ca[i];
        while(c.charAt(0) == ' '){
            c = c.substring(1);
        }

        if(c.indexOf(nome) == 0){
            return c.substring(nome.length, c.length);
        }
    }
    return "";
}

function checkCookie(cnome){
    var valor = getCookie(cnome);
    if(valor != ""){
        console.log("Cookie '" + cnome + "' = " + valor);
        return true;
    }else{
        console.log("Cookie '"+ cnome +"' não existe");
        return false;
    }
}

function getAllCookie(){
    return document.cookie;
}