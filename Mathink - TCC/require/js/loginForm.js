$(document).ready(function(){
	var loginForm = getLoginCookie();
	(loginForm)?$('#menu #login #flutuante').css('display', 'block')
		       :$('#menu #login #flutuante').css('display','none');
	$('#menu #login img').click(function(){
		if ($('#menu #login #flutuante').css('display') == 'none'){
			setLoginCookie(1);
			$('#menu #login #flutuante').fadeIn();
		}else {
			setLoginCookie(0);
			$('#menu #login #flutuante').fadeOut();
		}
	});
});

function setLoginCookie(value) {
	var tempo =new Date();
	var hoje = tempo.getTime();
	tempo.setTime(tempo.getTime() + (1 * 24 * 60 * 60 * 1000));
	var expirar = tempo.toUTCString();
	document.cookie = "loginForm="+value + ";" + expirar;
}

function getLoginCookie(){
	var separadorCookie =  document.cookie.split(';');
	for(var i=0; i<separadorCookie.length;i++){
		var c = separadorCookie[i];
		while(c.charAt(0) == ' '){
			c = c.substring(1);
		}
		if(c.indexOf("loginForm") == 0){
			var res =  c.substring();
			if(res == "loginForm=1"){
				return true;
			}
		}
		return false;
	}
}