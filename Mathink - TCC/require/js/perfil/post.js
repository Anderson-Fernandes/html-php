/**
 * Created by Anderson Luiz on 16/05/2016.
 */



function getResposta(usuario, tipoUsuario) {

    console.log('carregando');

    $.ajax({
        url: "require/php/perfil/getResposta.php",
        type: "POST",
        data: {id: usuario, tipoUsuario: tipoUsuario},
        success: function (post) {
            console.log('carregou');
            if (post) {
                $("#mensagemPostPerfil").append(post);
            }else{
                console.log('sem retorno');
            }
        }
    });
}

function createReposta(resposta,autor, remetente) {
    resposta = resposta.trim();
    if(resposta.length >0){
        $.ajax({
            url: "require/php/perfil/createResposta.php",
            type: "POST",
            data: {mensagem:resposta, autor:autor, id:remetente},
            success: function (att) {
                if (att) {
                    console.log(att);
                } else {
                    location.reload();
                }
            }
        });
    }else{
        alert("Digite uma mensagem");
    }
}

function deleteResposta(mensagem){
    if(confirm('Você deseja deletar essa mensagem?')){
        $.ajax({
            url: "require/php/perfil/deletaResposta.php",
            type: "POST",
            data:{id:mensagem},
            success:function(data){
                if(data){
                    console.log(data);
                }else{
                    location.reload();
                }
            }
        })
    }
}