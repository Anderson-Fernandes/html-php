<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 16/05/2016
 * Time: 19:55
 */


require_once '../../../require/class/conDB.class.php';
require_once '../../../perfil/require/php/marketItens.php';

$crud = new CRUD;


$idUser = $_POST['id'];
$tipoUsuario = $_POST['tipoUsuario'];


$resposta = array();
$content = array();

$infoResposta =$crud->select("cd_resposta,ds_resposta, nm_nickname, tb_aluno.cd_usuario","mensagem_perfil inner join tb_usuario on mensagem_perfil.cd_usuario = tb_usuario.cd_usuario inner join tb_aluno on tb_aluno.cd_usuario = tb_usuario.cd_usuario","WHERE mensagem_perfil.cd_remetente=? order by dt_resposta, tm_resposta ",array($idUser));


if($infoResposta->rowCount()>0){

    foreach ($infoResposta as $dds){
        $content['id'] = $dds['cd_resposta'];
        $content['resposta'] = $dds['ds_resposta'];
        $content['nome'] = $dds['nm_nickname'];
        $content['userId'] = $dds['cd_usuario'];
        array_push($resposta,$content);
    }
}


$contagemDeRespostas = count($resposta);



if($contagemDeRespostas>0):
    for($i = 0; $i < $contagemDeRespostas; $i++):?>
        <div class="row">
            <div class="tituloMensagemPerfil col-md-offset-3 col-md-9">
                <h4>
                    <a href="perfil.php?nome=<?= $resposta[$i]['nome'] ?>">
                        <?php echo $resposta[$i]['nome']?>
                    </a>
                </h4>
            </div>
        </div>
        <div class="row center-block">
            <div class="col-md-2 amigoFotoPerfil ">
                <a href="perfil.php?nome=<?= $resposta[$i]['nome'] ?>">

                    <?php

                    $perfilLampi = getPerfilImg($resposta[$i]['userId']);

                    ?>
                    <img class="img-responsive" src="perfil/require/img/equips/<?=$perfilLampi?>">
                </a>
               
            </div>
            <div  id="<?= $resposta[$i]['id'] ?>" class="col-md-9 mensagemAmigoPerfil">
                <p style="color:#323232;"><?php echo htmlspecialchars(nl2br($resposta[$i]['resposta']))?></p>
                 <span id="men-<?= $resposta[$i]['id'] ?>" class="delButtonPerfil" onclick="deleteResposta('<?= $resposta[$i]['id'] ?>')">X</span>
            </div>
        </div>
        <?php
    endfor;
    else:
        ?>
        <div class="row">
            <div class="col-md-offset-3 col-md-9 tituloMensagemPerfil">
                <h4>Seja o primeiro a comentar</h4>
            </div>
        </div>
        <?php
endif;


if($tipoUsuario == 'dono'):
?>


<script>
    $('.mensagemAmigoPerfil').mouseenter(function(){
        var id  = $(this).attr('id');
        $('#men-'+id).css('display','block');
        console.log(id);
    });

        $('.mensagemAmigoPerfil').mouseleave(function(){
        var id  = $(this).attr('id');
        $('#men-'+id).css('display','none');
        console.log(id);
    });
</script>


<?php endif; ?>
