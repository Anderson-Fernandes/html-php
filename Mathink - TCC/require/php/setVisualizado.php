<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 29/05/2016
 * Time: 21:01
 */

require_once"../class/conDB.class.php";


$crud = new CRUD;
$tipo = $_POST['tipo'];

if($tipo == 'sala') {
    $nome = $_POST['id'];
    $visualizado = $crud->select("cd_notificacao",'tb_notificacao_sala INNER JOIN sala_aluno on tb_notificacao_sala.cd_sala_aluno = sala_aluno.cd_sala_aluno INNER JOIN tb_sala on tb_sala.cd_sala = sala_aluno.cd_sala','WHERE nm_sala =? AND ic_visualizou is null',array($nome));
    foreach ($visualizado as $dds){
        $crud->update('tb_notificacao_sala','ic_visualizou = 0 WHERE cd_notificacao=?',array($dds['cd_notificacao']));
    }
}