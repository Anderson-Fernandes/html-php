<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 29/05/2016
 * Time: 20:10
 */
require_once"../class/conDB.class.php";
require_once"../php/logado.php";


$crud = new CRUD;
$count = 0;
$tipo = $_POST['tipo'];
$notificacao = array();

if($tipo == 'sala') {
    $nome = $_POST['id'];
    $id = $nome;
    $solicitacaoSala = $crud->select("cd_notificacao, ic_visualizou, nm_usuario", 'tb_notificacao_sala INNER JOIN sala_aluno on tb_notificacao_sala.cd_sala_aluno = sala_aluno.cd_sala_aluno INNER JOIN tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario INNER JOIN tb_sala on tb_sala.cd_sala = sala_aluno.cd_sala', 'WHERE nm_sala =?', array($nome));
    $count += $solicitacaoSala->rowCount();

    if ($solicitacaoSala->rowCount() > 0) {
        foreach ($solicitacaoSala as $dds) {
            $dados['codigo'] = $dds['cd_notificacao'];
            $dados['titulo'] = 'Solicitação de entrada na sala';
            $dados['tipo'] = 'sala';
            $dados['nome'] = $dds['nm_usuario'];
            $dados['visualizado'] = $dds['ic_visualizou'];
            $dados['frase'] = 'quer ser seu aluno';
            array_push($notificacao, $dados);
        }
    }
}

if($tipo == 'amigo'){

    $id = $_POST['id'];
    $solicitacaoSala = $crud->select("cd_notificacao, ic_visualizou, nm_usuario", 'tb_notificacao_Amigo inner join tb_amigo on tb_notificacao_Amigo.cd_amizade = tb_amigo.cd_amizade inner join tb_usuario on tb_usuario.cd_usuario = tb_amigo. cd_amigo',"WHERE tb_amigo.cd_usuario=?",array($_SESSION['logado']));
	//var_dump($solicitacaoSala);
    $count += $solicitacaoSala->rowCount();

    if ($solicitacaoSala->rowCount() > 0) {
        foreach ($solicitacaoSala as $dds) {
            $dados['codigo'] = $dds['cd_notificacao'];
            $dados['titulo'] = 'Solicitação de amizade';
            $dados['tipo'] = 'amigo';
            $dados['nome'] = $dds['nm_usuario'];
            $dados['frase'] = 'quer ser seu amigo';
            $dados['visualizado'] = $dds['ic_visualizou'];
            array_push($notificacao, $dados);
        }
    }
}
?>

<?php
    if($count > 0):
        for($i= 0; $i < $count ; $i++):
?>
    <div onclick="loadFinallyActionNotification('<?=$id?>','<?=$notificacao[$i]["tipo"];?>','<?= $notificacao[$i]["codigo"]; ?>')" class="notificacao <?php echo ($notificacao[$i]['visualizado'] != 1)?'n-visualizado':''?>">
        <h2><?= $notificacao[$i]['titulo'];?></h2>
        <p><b><?= $notificacao[$i]['nome'];?></b> <?=$notificacao[$i]['frase'];?> </p>
    </div>
<?php
        endfor;
    endif;
?>




