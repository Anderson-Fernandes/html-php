<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 29/05/2016
 * Time: 20:41
 */

require_once"../class/conDB.class.php";


$crud = new CRUD;
$count = 0;
$tipo = $_POST['tipo'];
$notificacao = array();

if($tipo == 'sala'){
    $nome = $_POST['id'];
    $solicitacaoSala = $crud->select("cd_notificacao, ic_visualizou, nm_usuario",'tb_notificacao_sala INNER JOIN sala_aluno on tb_notificacao_sala.cd_sala_aluno = sala_aluno.cd_sala_aluno INNER JOIN tb_usuario on tb_usuario.cd_usuario = sala_aluno.cd_usuario INNER JOIN tb_sala on tb_sala.cd_sala = sala_aluno.cd_sala','WHERE nm_sala =? AND ic_visualizou is null',array($nome));
    $count += $solicitacaoSala->rowCount();
}

if($tipo == 'amigo'){
    $id = $_POST['id'];
    $solicitacaoSala = $crud->select("cd_notificacao, ic_visualizou, nm_usuario",'tb_notificacao_amigo INNER JOIN tb_amigo on tb_amigo.cd_amizade = tb_notificacao_amigo.cd_amizade','WHERE cd_usuario =? AND ic_visualizou is null',array($id));
    $count += $solicitacaoSala->rowCount();
}

echo $count;