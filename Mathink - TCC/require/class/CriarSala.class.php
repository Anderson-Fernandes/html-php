<?php

/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 30/04/2016
 * Time: 16:25
 */
class CriarSala
{
    private $id, $nome, $senha,$crud, $cpt,$vs;

    public function canCreate($crud, $nomeDaSala, $privacidade){

        $logDds = $crud->select('nm_sala, ic_privacidade','tb_sala','WHERE nm_sala=?',array($nomeDaSala));
        if($logDds->rowCount() > 0){
            return 'Nome de sala já existente';
        }else{
            if($privacidade == 1){
                $logDds = $crud->select('nm_sala, ic_privacidade','sala_aluno inner join tb_sala on sala_aluno.cd_sala = tb_sala.cd_sala','WHERE cd_usuario=? AND cd_autoridade = 5 AND ic_privacidade=? AND cd_senha IS NULL',array($this->id,$privacidade));
                if($logDds->rowCount() > 2){
                    return 'Você so pode criar 3 salas publicas!';
                }
            }
        }
    }

    private function hasPass($senha){
        if(strlen($senha) > 0){
            return true;
        }else{
            return false;
        }
    }


    private function inserirSala($nome,$senha,$privacidade,$crud){
        if($senha != null){
            $this->cpt = new Cripto;
            $this->senha = $this->cpt->setCripto($senha);
        }else{

        }


        session_start();
        $this->id = $_SESSION['logado'];


        $canCreate = $this->canCreate($crud,$nome,$privacidade);
        if($canCreate){
            return $canCreate;
        }else{
            $nome = strip_tags($nome);
            $nome = str_replace("'","",$nome);
            $nome = str_replace("\"","",$nome);
            $criacao = date("Y-m-d");
            $crud->call('pr_sala_professor(?,?,?,?)',array($nome,$this->id,$privacidade,$this->senha));
           // $crud->insert('tb_sala','nm_sala=?,cd_dono=?,ic_privacidade=?,cd_senha=?',array($nome,$this->id,$privacidade,$this->senha));
        }

    }

    public function setSala($nome, $senha, $senha2, $privacidade)
    {

        $this->crud = new CRUD;

        $this->vs = new ValidaSenha;

        $this->nome = ucfirst($nome);

        if (empty($this->nome))
        {
            return 'Preencha o nome';
        }
        else
        {
            if ($this->hasPass($senha) || $this->hasPass($senha2))
            {
                $this->senha = $this->vs->setValidaSenha($senha);
                if ($this->senha != $senha)
                {

                    return $this->senha;
                }
                else
                {
                    if ($this->senha != $senha2)
                    {
                        return 'Senhas diferentes';
                    }
                    else
                    {
                        $insereSala =  $this->inserirSala($this->nome,$this->senha,$privacidade,$this->crud);
                        if($insereSala){
                            return $insereSala;
                        }
                    }
                }
            }
            else
            {
                $insereSala =  $this->inserirSala($this->nome,null,$privacidade,$this->crud);
                if($insereSala){
                    return $insereSala;
                }

            }
        }
    }
}