<?php

/**
 * Created by PhpStorm.
 * User: PC-CASA
 * Date: 23/04/2016
 * Time: 03:39
 */
class Session
{
    const SESSION_STARTED = TRUE;
    const SESSION_NOT_STARTED = FALSE;

    //O estado da sessão
    private $sessionState = self::SESSION_NOT_STARTED;

    // A única instância da classe
    private static $instance;




    /**
     *    Retorna a instância de 'Session' .
     *    A sessão é automaticamente inicializado se não estiver
     *
     *    @return    object
     **/

    public static function getInstance()
    {
        if ( !isset(self::$instance))
        {
            self::$instance = new self;
        }

        self::$instance->startSession();

        return self::$instance;
    }



    /**
     *    (Re)starta a Session
     *
     *    @return    bool    TRUE se a sessão ja estiver incializada, se não FALSE.
     **/

    public function startSession()
    {
        if ( $this->sessionState == self::SESSION_NOT_STARTED )
        {
            $this->sessionState = session_start();
        }

        return $this->sessionState;
    }


    /**
     *    Grava dados na Session
     *    Exemplo: $instance->foo = 'bar';
     *
     *    @param    name   Nome dos dados.
     *    @param    value    Seus dados.
     *    @return    void
     **/

    public function __set( $name , $value )
    {
        $_SESSION[$name] = $value;
    }


    /**
     *    Pega os dados da Session
     *    Exemplo: echo $instance->foo;
     *
     *    @param    name    Nome do dado caputrado.
     *    @return    mixed    Dado da Session.
     **/

    public function __get( $name )
    {
        if ( isset($_SESSION[$name]))
        {
            return $_SESSION[$name];
        }
    }


    public function __isset( $name )
    {
        return isset($_SESSION[$name]);
    }


    public function __unset( $name )
    {
        unset( $_SESSION[$name] );
    }


    /**
     *    Destroe a Session atual
     *
     *    @return    bool    TRUE se a sessão foi deletada, se não FALSE.
     **/

    public function destroy()
    {
        if ( $this->sessionState == self::SESSION_STARTED )
        {
            $this->sessionState = !session_destroy();
            unset( $_SESSION );

            return !$this->sessionState;
        }

        return FALSE;
    }
}
?>



}