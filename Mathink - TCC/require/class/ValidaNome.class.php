<?php

/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 09/05/2016
 * Time: 07:58
 */
class ValidaNome
{
    public function setValidaNome($nome){
        $nome = trim($nome);
        $nome = ltrim($nome);
        $nome = rtrim($nome);
        $nome = ucfirst($nome);
        if(empty($nome)){
            return "Preencha seu nome";
        }
        if(!ereg("^([A-Z,a-z, ,.,ã,á,à,â,ê,í,ú,õ,é,ü,-]){0,50}$",$nome)){
           return "Seu nome so pode conter letras";
        }
        
        return $nome;
    }
}
