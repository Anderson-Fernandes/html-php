<?php 

class ValidaSenha
{
	public function setValidaSenha($senha)
	{
		if(strlen($senha)<1)
		{
			return 'Informe a senha';
		}
		else
		{
			if(strlen($senha)>4 && strlen($senha)<20)
			{
				return $senha;
			}
			else
			{
				return 'Senha invalida, deve conter de 5 a 20 caracteres';
			}
			
		}
	}
}

?>