<?php 

class Cadastro
{
	private $fd, $ve, $vs,$vn ,$cpt, $crud, $email, $senha, $nome;

	public function setCadastro($nome,$email,$email2,$senha,$senha2)
	{		
		$this->ve=new ValidaEmail;
		$this->vs=new ValidaSenha;
        $this->vn=new ValidaNome;
		$this->cpt=new Cripto;
		$this->crud=new CRUD;
		$this->fd = new dataFormato;


        $nome = trim($nome);
        $email = trim($email);

		$this->email=$this->ve->setValidaEmail($email);
		$this->senha=$this->vs->setValidaSenha($senha);
        $this->nome=$this->vn->setValidaNome($nome);

		if(strtolower($this->nome) != strtolower($nome))
		{
			return $this->nome;
		}
		else
		{
			if($this->email != $email)
			{
				return $this->email;
			}
			else
			{
                if($this->email != $email2)
                {
                    return 'E-mail diferente';
                }
                else
                {
                    if($this->senha != $senha)
                    {
                        return $this->senha;
                    }
                    else
                    {
                        if($this->senha != $senha2)
                        {
                            return 'Senhas diferentes';
                        }
                        else
                        {
                            $this->lEmail = $this->crud->select('nm_email','tb_usuario','WHERE nm_email=?',array($this->email));
                            if($this->lEmail->rowCount() > 0)
                            {
                                return 'Email já cadastrado';
                            }
                            else
                            {
                                $this->criacao = date("Y-m-d");
                                $this->crud->insert('tb_usuario','nm_usuario=?, nm_email=?,cd_senha=?,dt_criacao=?,tm_online=curtime()',array($this->nome,$this->email, $this->cpt->setCripto($this->senha),$this->criacao));

                                $this->log = $this->crud->select('cd_usuario','tb_usuario','WHERE nm_email=? && cd_senha=?',array($this->email,$this->cpt->setCripto($this->senha)));

                                if($this->log->rowCount() >0)
                                {
                                    session_start();
                                    foreach ($this->log as $this->dds)
                                    {
                                        $this->id=$this->dds['cd_usuario'];
                                    }
                                }


                                //$this->crud->insert('tb_aluno','cd_usuario=?, nr_level=?,vl_cash=?,vl_exp=?, vl_points=?',array($this->id,1,0,0,0));
                               // $this->crud->insert('tb_professor','cd_usuario=?, nr_level=?,vl_exp=?',array($this->id,1,0));

                                $_SESSION['logado'] = $this->id;
                                $_SESSION['isPrimeiraVez'] = 1;

                            }
                        }
                    }
                }
		    }
	    }
    }
}

 ?>