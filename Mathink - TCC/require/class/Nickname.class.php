<?php

/**
 * Created by PhpStorm.
 * User: PC-CASA
 * Date: 23/04/2016
 * Time: 18:25
 */
class Nickname
{   
    private $nickname;
    private $isValido = array('-', '_');
    private $crud, $session;


    public function setNickname($nome){
        $this->nickname = trim($nome);


        if(strlen($this->nickname) < 3){
            return 'Você deve entrar com pelo menos 3 digitos!';
        }else{
            if(!ctype_alnum(str_replace($this->isValido, '', $this->nickname)))
            {
                return "Seu nome <b>não</b> pode conter: 'Espaços, e nem caracteres especiais!' ";
            }else{
                $this->crud = new CRUD;
                $nickname = $this->crud->select("nm_nickname","tb_aluno","WHERE nm_nickname=?",array($this->nickname));
                if($nickname->rowCount() >0){
                    return "Infelizmente esse nome já está cadastrado =(";
                }else{
                    echo "123EXISTE(";
                    $session = Session::getInstance();
                    $id = $session->logado;
                    $this->crud->update("tb_aluno","nm_nickname=? WHERE cd_usuario=?",array($this->nickname,$id));
                    $session->isPrimeiraVez = 0;
                    echo "EXISTE)123";
                }

            }
        }
    }
}