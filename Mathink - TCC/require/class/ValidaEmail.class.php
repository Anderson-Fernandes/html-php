<?php

class ValidaEmail
{
	public function setValidaEmail($email)
	{
		$ext = array('.com','.br','.us','org','net');
		$exc = array('@trbvm','@pooae','@my10minutemail','@20mail','@10mail','@adiaw','@boxtemp','@vomoto','@mohmal','@sharklasers','@grr','@guerrillamail','@guerrillamailblock','@spam4','@maildrop','@mailinater.com','@imgof','@meltmail.com

','@yopmail','@anappthat','@6paq','@grandmamail','@getairmail','@tryalert','incognitomail');
		$email = trim($email);
		if(empty($email))
		{
			return 'Preencha o E-mail';
		}
		else
		{
			if(!preg_match('/^[0-9a-z\_\.\-]+\@[0-9a-z\_\.\-]*[0-9a-z\_\-]+\.[a-z]{2,3}$/i', $email))
			{
				return 'Email Invalido';	
			}
		/*	else if(!in_array(strrchr($email, '@'),$exc))
			{
				return 'Email Temporario não é valido!';
			} */
			else
			{
				if(!in_array(strrchr($email, '.'),$ext))
				{
					return 'Email Invalido';
				}
				else
				{
				return $email;
				}
			}
		}
	}
}

?>