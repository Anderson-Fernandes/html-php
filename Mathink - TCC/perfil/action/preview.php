<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 07/06/2016
 * Time: 06:52
 */

require_once '../../require/php/logado.php';
require_once '../../require/class/conDB.class.php';
$crud = new CRUD;
$id = $_POST['id'];
$levelUser = $_POST['level'];
$pi = $_POST['pi'];


$logItensMarket = $crud->select("*","tb_loja","WHERE cd_item = ? ORDER BY nr_level_necessario ASC, nm_item ASC",array($id));


if($logItensMarket->rowCount() > 0){
    foreach ($logItensMarket as $dds){
        $codigo = $dds['cd_item'];
        $nome = $dds['nm_item'];
        $tipo = $dds['nm_tipo'];
        $preco = $dds['vl_preco'];
        $level = $dds['nr_level_necessario'];
        $owned = $crud->select("ic_equipado","loja_usuario inner join tb_loja on loja_usuario.cd_item = tb_loja.cd_item","WHERE tb_loja.cd_item =? AND cd_usuario = ?",array($codigo,$_SESSION['logado']));
        $has = ($owned->rowCount() > 0)?true:false;
        $userLevel = $crud->select("nm_item","tb_loja","WHERE cd_item=? AND nr_level_necessario>?",array($codigo,$levelUser));
        $noLevel = ($userLevel->rowCount() > 0)?true:false;
        foreach ($owned as $dds){
            $equiped = $dds['ic_equipado'];
        }
    }
}



?>

<div  class="text-center">
 <!--   <h2 > <?= $nome?> </h2>-->
    <img src="perfil/require/img/equips/<?=$tipo?>/<?=$nome?>.png" class="img-responsive"  alt="Preview"/>
</div>

<h4 <?= ($pi < $preco)?"style='color:red'":"";?> >
    Valor: <?= $preco?> PI
</h4>
<h4 <?= ($noLevel)?"style='color:red'":"";?> >
    Level Requerido: <?= $level?>
</h4>

<?php if($has):
    ?>
    <button class="btn btn-default" style="width:100%" onclick="equipar('<?=$id?>','<?=$tipo?>')"><?=($equiped)?"Equipado":"Equipar"?></button>
<?php else:?>
    <button class="btn btn-default" style="width:100%"  onclick="comprar('<?=$id?>','<?=$preco?>')" <?= ($noLevel || ($pi < $preco))?"disabled":"";?>>Comprar</button>
<?php endif;?>

