<?php
/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 06/06/2016
 * Time: 02:50
 */


require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';
require_once 'require/php/marketItens.php';


$crud = new CRUD;
$pessoa = '';


    $logPerfil = $_SESSION['logado'];
    $logPerfil = $crud->select('tb_usuario.cd_usuario, nm_nickname, nm_usuario,nr_level,vl_points,nm_color','tb_aluno inner join tb_usuario on tb_aluno.cd_usuario = tb_usuario.cd_usuario','WHERE tb_usuario.cd_usuario = ?',array($logPerfil));


$cor = null;
if($logPerfil->rowCount() > 0){
    foreach ($logPerfil as $dds){
        $id = $dds['cd_usuario'];
        $nomeCompleto = $dds['nm_usuario'];
        $nickname = $dds['nm_nickname'];
        $level = $dds['nr_level'];
        $pontos = $dds['vl_points'];
        $cor = $dds['nm_color'];
    }
}else{
    header("Location: perfil.php?ERRO");
}


$pessoa = 'dono' ;





$perfilLampi = getPerfilImg($id);



?>




<!DOCTYPE html>
<html lang="pt-BR" xmlns="http://www.w3.org/1999/html">
<head>
    <base href="..">
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/loginForm.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>
    <script src="require/js/ballon.js"></script>
    <script src="salas/require/js/avaliacao_da_sala.js"></script>
    <script src="require/contextMenu/contextMenu.js"></script>
    <link rel="stylesheet" href="require/contextMenu/contextMenu.css" />
    <script src="require/js/notification.js"></script>


    <?php
    if($cor != null){
        printf('<link rel="stylesheet" href="require/css/perfil/cores/' . $cor . '.css" />');
    }else{
        require_once "tutorial/cores.html";
    }
    ?>




    <script>

        $(document).ready(function(){
            finalyNotification('<?= $id ?>','amigo','notificacaoSala');
            getCountNotification('<?= $id ?>','amigo','notificationSala');
            setVisualizadoGeral('<?= $id ?>','amigo',0);
        });

    </script>
</head>
<body>
<?php require_once '../menu.php' ?>

</header>
<div id="perfil" class="container-fluid">
    <div id="bannerPerfil" class="row">
        <div class="col-md-12">
            <img id="lampiPerfil" class="img-responsive" src="perfil/require/img/equips/<?=$perfilLampi?>" />
            <h2 id="nomePerfil" onclick="location.href='perfil.php?nome=<?=$nickname?>'"style="cursor:pointer"  >
                <?php
                echo $nickname;
                if($pessoa!='dono'){
                    if($pessoa=='amigo'){
                        printf('<i style="margin-left:1em;" class="fa fa-user" aria-hidden="true"></i>');
                    }else{
                        printf('<i style="margin-left:1em;" class="fa fa-user-plus" aria-hidden="true"></i>');
                    }
                }else{

                }
                ?></h2>
            <h2 id="nomeCompleto" onclick="location.href='perfil.php?nome=<?=$nickname?>'"style="cursor:pointer"> <?php  echo htmlspecialchars($nomeCompleto); ?></h2>
        </div>
    </div>

       <?php if($pessoa == 'dono'):?>
        <div id="topoConfigDePerfil" class="row">
            <div class="col-md-12 text-center">

                <!-- <img src="perfil/require/img/amigo.svg" onclick="location.href='perfil/amigos.php'"/> -->
                <img src="perfil/require/img/market.svg" onclick="location.href='perfil/market.php'"/>
                <img src="salas/require/img/notificacao.svg" onclick="location.href='perfil/notificacao.php'"/> <span id="notificationSala">0</span>
                <img src="salas/require/img/config.svg" onclick="location.href='perfil/config.php'"/>


            </div>
        </div>
    <?php endif; ?>




    <div class="row">
        <div id="notificacaoSala" class="col-md-offset-1 col-md-10">
            <!--
            <div class="notificacao">
                <h2>Solicitação de entrada na sala</h2>
                <p><b>Bruno</b> quer ser seu aluno</p>
            </div>

            <div class="notificacao">
                <h2>Novo post</h2>
                <p><b>Anderson</b> postou 'Alura tecnics'</p>
            </div>

            <div class="notificacao">
                <h2>Saida da sala</h2>
                <p><b>Negocio loko</b> saiu da sala</p>
            </div>

            <div class="notificacao">
                <h2>Completou a tarefa</h2>
                <p><b>Arnold</b> completou a tarefa</p>
            </div>

            <div class="notificacao">
                <h2>Alunos que concluíram a tarefa</h2>
                <p><b>10</b> alunos concluíram a tarefa proposta</p>
            </div>
            <div class="notificacao">
                <h2>Alunos que não concluíram a tarefa</h2>
                <p><b>4</b> alunos não concluíram a tarefa proposta</p>
            </div>
        -->
        </div>
    </div>
</div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>