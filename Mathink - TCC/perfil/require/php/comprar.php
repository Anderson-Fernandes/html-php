<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 07/06/2016
 * Time: 07:54
 */



$id = $_POST['id'];
$valor = $_POST['valor'];


require_once '../../../require/class/conDB.class.php';
require_once '../../../require/php/logado.php';

$crud = new CRUD;


// insert into loja_usuario set cd_usuario = ?, cd_item = ? dt_adquirido = curdate(), ic_equipado = ?
$crud->insert("loja_usuario","cd_usuario=?, cd_item=?, dt_adquirido = curdate(), ic_equipado = ?",array($_SESSION['logado'],$id,0));
$crud->update("tb_aluno","vl_cash = vl_cash - ? WHERE cd_usuario=?",array($valor,$_SESSION['logado']));
