<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 30/05/2016
 * Time: 19:45
 */


require_once '../../../require/class/conDB.class.php';
$crud = new CRUD;

$idNotification = $_POST['msgId'];

$log = $crud->select("cd_amizade","tb_notificacao_amigo","WHERE cd_notificacao = ?",array($idNotification));

foreach ($log as $dds){
    $id = $dds['cd_amizade'];
}

 $crud->delete("tb_notificacao_amigo",'WHERE cd_notificacao = ?',array($idNotification));
 $crud->update("tb_amigo","ic_aprovado = true WHERE cd_amizade = ?",array($id));