<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 07/06/2016
 * Time: 07:30
 */


$id = $_POST['id'];
$tipo = $_POST['tipo'];


require_once '../../../require/class/conDB.class.php';
require_once '../../../require/php/logado.php';

$crud = new CRUD;


//update loja_usuario Inner join tb_loja on tb_loja.cd_item = loja_usuario.cd_item
// set ic_equipado=1 where nm_tipo = "imagem-perfil";
$crud->update("loja_usuario Inner join tb_loja on tb_loja.cd_item = loja_usuario.cd_item","ic_equipado=? WHERE nm_tipo =? AND cd_usuario=?",array(0,$tipo,$_SESSION['logado']));
$crud->update("loja_usuario Inner join tb_loja on tb_loja.cd_item = loja_usuario.cd_item","ic_equipado=? WHERE nm_tipo =? AND cd_usuario=? AND tb_loja.cd_item = ?",array(1,$tipo,$_SESSION['logado'],$id));

