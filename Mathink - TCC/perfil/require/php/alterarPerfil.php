<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 36/06/2016
 * Time: 20:29
 */


require_once '../../../require/class/conDB.class.php';
require_once '../../../require/php/logado.php';
$crud = new CRUD;
$vs = new ValidaSenha;
$ve = new ValidaEmail;
$cripto = new Cripto;


$passed = true;

$emailN = $_POST['email'];

$senhaN = $_POST['senha'];

$confirm = $_POST['confirm'];




$email = "";
$senha = "";

$changeEmail = false;
$changePass= false;


if(strlen($emailN) > 0){

	$email = $ve->setValidaEmail($emailN);
	if($email != $emailN){
		echo $email;
		$passed =false;
		}else{
			$changeEmail = true;
		}
}


if(strlen($senhaN) > 0 ){
	$senha = $vs->setValidaSenha($senhaN);
	if($senhaN != $senha){
		echo $senha;
		$passed =false;
	}else{
		$changePass = true;
	}
}


if($passed){
	$confirm = $cripto->setCripto($confirm);
	$senha = $cripto->setCripto($senha);
	$auth = $crud->select("nm_usuario","tb_usuario", "WHERE cd_usuario=? AND cd_senha =?",array($_SESSION['logado'],$confirm));

	if($auth->rowCount() > 0){
		if($changeEmail && $changePass){
			$crud->update('tb_usuario','nm_email=?, cd_senha=? WHERE cd_usuario=?',array($email,$senha,$_SESSION['logado']));
		} else if($changeEmail){
			$crud->update('tb_usuario','nm_email=? WHERE cd_usuario=?',array($email,$_SESSION['logado']));
		}else if($changePass){
			$crud->update('tb_usuario','cd_senha=? WHERE cd_usuario=?',array($senha,$_SESSION['logado']));

		}

		//update tabela 

	}else{
		echo "Senha atual incorreta";
	}

}







