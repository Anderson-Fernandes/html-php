function alterarPerfil(email, senha,confirm){

	if(email.length < 1 && senha.length < 1 ){
		alert("Altere alguma informação!");
		return;
	}

	$.ajax({
		url:'perfil/require/php/alterarPerfil.php',
		method:'POST',
		data:{email:email,senha:senha, confirm:confirm},
		success: function (data){
			if(data){
				alert(data);
			}else{
				location.reload();
			}
		}
	});
}

function excluirPerfil(email,confirm){
	$.ajax({
		url:'perfil/require/php/excluirConta.php',
		method:'POST',
		data:{email:email,confirm:confirm},
		success: function (data){
			if(data){
				alert(data);
			}else{
				location.href="index.php";
			}
		}
	});
}