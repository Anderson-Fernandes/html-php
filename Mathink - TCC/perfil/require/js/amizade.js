/**
 * Created by Anderson Luiz on 06/06/2016.
 */

function setSolicitacaoAmigo(amigo,aluno,currentSt){
    if(currentSt == "solicitado"){
        if(confirm("Você deseja cancelar a solicitação")){
            cancelFriend(amigo,aluno);
        }
    }else{
        if(confirm("Você deseja enviar uma solicitação?")){
            ajaxFriend(amigo,aluno);
        }
    }
}

function cancelFriend(amigo, aluno){
    $.ajax({
       url:"perfil/require/php/cancelAmigo.php",
       type:"POST",
       data:{amigo:amigo,aluno:aluno},
       success:function(data){
           if(data){
               console.log(data);
           }else{
               location.reload();
           }
       }
    });
}

function ajaxFriend(amigo,aluno){
    $.ajax({
        url:"perfil/require/php/amigos.php",
        type:"POST",
        data:{amigos:amigo,aluno:aluno},
        success:function(data){
            if(data){
               
                    console.log(data);
                
            }else{
               location.reload();
            }
        }
    });
}