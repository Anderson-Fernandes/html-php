/**
 * Created by Anderson Luiz on 07/06/2016.
 */

function comprar(id, valor){
    $.ajax({
        url:"perfil/require/php/comprar.php",
        method:"POST",
        data:{id:id,valor:valor},
        success: function(dados){
            if(dados){
                console.log(dados);
            }else{
                location.reload();
            }
        }
    })
}

function equipar(id, tipo){
    $.ajax({
        url:"perfil/require/php/equipar.php",
        method:"POST",
        data:{id:id,tipo:tipo},
        success: function(dados){
            if(dados){
                console.log(dados);
            }else{
                location.reload();
            }
        }
    })
}