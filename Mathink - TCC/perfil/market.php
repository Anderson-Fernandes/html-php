<?php
/**
 * Created by PhpStorm.
 * User: Anderson Fernandes
 * Date: 06/06/2016
 * Time: 02:50
 */


require_once '../require/php/logado.php';
require_once '../require/class/conDB.class.php';
require_once 'require/php/marketItens.php';



$crud = new CRUD;
$pessoa = '';
$cor = null;


$logPerfil = $_SESSION['logado'];
$logPerfil = $crud->select('tb_usuario.cd_usuario, nm_nickname, nm_usuario,nr_level,vl_points,nm_color, nm_email, vl_cash','tb_aluno inner join tb_usuario on tb_aluno.cd_usuario = tb_usuario.cd_usuario','WHERE tb_usuario.cd_usuario = ?',array($logPerfil));


$cor = null;

if($logPerfil->rowCount() > 0){
    foreach ($logPerfil as $dds){
        $id = $dds['cd_usuario'];
        $nomeCompleto = $dds['nm_usuario'];
        $nickname = $dds['nm_nickname'];
        $email = $dds['nm_email'];
        $level = $dds['nr_level'];
        $pontos = $dds['vl_points'];
        $cor = $dds['nm_color'];
        $pi = $dds['vl_cash'];
    }
}else{
    header("Location: perfil.php?ERRO");
}

$logItensMarket = $crud->select("*","tb_loja","ORDER BY nr_level_necessario ASC, nm_item ASC",array());


$itens = array();

if($logItensMarket->rowCount() > 0){
    foreach ($logItensMarket as $dds){
        $itemLoja['codigo'] = $dds['cd_item'];
        $itemLoja['nome'] = $dds['nm_item'];
        $itemLoja['tipo'] = $dds['nm_tipo'];
        $itemLoja['preco'] = $dds['vl_preco'];
        $itemLoja['level'] = $dds['nr_level_necessario'];
        $owned = $crud->select("tb_loja.cd_item","loja_usuario inner join tb_loja on loja_usuario.cd_item = tb_loja.cd_item","WHERE tb_loja.cd_item =? AND cd_usuario = ?",array($itemLoja['codigo'],$_SESSION['logado']));
        $itemLoja['has'] = ($owned->rowCount() > 0)?true:false;
        $userLevel = $crud->select("nm_item","tb_loja","WHERE cd_item=? AND nr_level_necessario>?",array($itemLoja['codigo'],$level));
        $itemLoja['noLevel'] = ($userLevel->rowCount() > 0)?true:false;
        array_push($itens,$itemLoja);
    }
}



$pessoa = 'dono' ;



$perfilLampi = getPerfilImg($_SESSION['logado']);



?>




<!DOCTYPE html>
<html lang="pt-BR" xmlns="http://www.w3.org/1999/html">
<head>
    <base href="..">
    <meta charset="UTF-8" />
    <meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <title>Mathink</title>

    <script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
    <!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
    <link rel="icon" href="require/img/logo/mathink.ico">
    <link rel="stylesheet" href="require/css/master.css">
        <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>
    <script src="require/js/ballon.js"></script>
    <script src="require/js/notification.js"></script>
    <script src="perfil/require/js/loja.js"></script>



    <?php
    if($cor != null){
        printf('<link rel="stylesheet" href="require/css/perfil/cores/' . $cor . '.css" />');
    }else{
        require_once "../tutorial/cores.html";
    }
    ?>




    <script>

        $(document).ready(function(){
            getCountNotification('<?= $_SESSION["logado"] ?>','amigo','notificationSala');
            setVisualizadoGeral('<?= $_SESSION["logado"] ?>','amigo',0);
            changePreview(1, '<?=$level?>', '<?=$pi?>');

            $('.market-place').click(function(){
               var id = $(this).attr('id');
              changePreview(id,'<?=$level?>','<?=$pi?>');
            });
        });

        function changePreview(id, level, pi){
            $.ajax({
                url:"perfil/action/preview.php",
                method:"POST",
                data:{id:id,level:level, pi:pi},
                success: function(data){
                    $('#reviewBuy').html(data);
                }
            })
        }

    </script>
</head>
<body>
<?php require_once '../menu.php' ?>

</header>
<div id="perfil" class="container-fluid">
    <div id="bannerPerfil" class="row">
        <div class="col-md-12">
            <img id="lampiPerfil" class="img-responsive" src="perfil/require/img/equips/<?=$perfilLampi?>" />
            <h2 id="nomePerfil" onclick="location.href='perfil.php?nome=<?=$nickname?>'"style="cursor:pointer"  >
                <?php
                echo $nickname;
                if($pessoa!='dono'){
                    if($pessoa=='amigo'){
                        printf('<i style="margin-left:1em;" class="fa fa-user" aria-hidden="true"></i>');
                    }else{
                        printf('<i style="margin-left:1em;" class="fa fa-user-plus" aria-hidden="true"></i>');
                    }
                }else{

                }
                ?></h2>
            <h2 id="nomeCompleto" onclick="location.href='perfil.php?nome=<?=$nickname?>'"style="cursor:pointer"> <?php  echo htmlspecialchars($nomeCompleto); ?></h2>
        </div>
    </div>

       <?php if($pessoa == 'dono'):?>
        <div id="topoConfigDePerfil" class="row">
            <div class="col-md-12 text-center">

                <!-- <img src="perfil/require/img/amigo.svg" onclick="location.href='perfil/amigos.php'"/> -->
                <img src="perfil/require/img/market.svg" onclick="location.href='perfil/market.php'"/>
                <img src="salas/require/img/notificacao.svg" onclick="location.href='perfil/notificacao.php'"/> <span id="notificationSala">0</span>
                <img src="salas/require/img/config.svg" onclick="location.href='perfil/config.php'"/>


            </div>
        </div>
    <?php endif; ?>




    <div id="market-submenu" class="row" >


        <div class=" col-md-offset-10 col-md-2">
            <h4>Pi Coins(<?=$pi?>)</h4>
        </div>




    </div>

    <div class="row" style="margin-bottom: 8em">


        <div id="reviewBuy" class="col-md-3">


        </div>

        <div class="col-md-offset-1 col-md-8">
           <div id="compra">

                <?php
                $i = 0;
                $index = -1;
                $counter = count($itens);
                for(;$i < $counter; $i++):
                    $index++;
                if($i == 6){
                    $i = 0;

                    $counter-=6;
                }

                ?>

                    <div id="<?=$itens[$index]["codigo"]?>" class="col-md-2 market-place <?= ($itens[$index]["has"])?"makert-has":"";?> <?=($itens[$index]['noLevel'])?"market-noLevel":""?>"> <img src="perfil/require/img/equips/<?=$itens[$index]['tipo']?>/<?=$itens[$index]['nome']?>.png"> </div>
            <?php endfor;?>

           </div>
        </div>

    </div>
</div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>