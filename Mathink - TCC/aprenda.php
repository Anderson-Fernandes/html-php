<?php
session_start();
require_once 'require/class/conDB.class.php';
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
	<title>Mathink</title>
    <script src="require/js/jquery.js"></script>
        <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
	<!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
	<link rel="icon" href="require/img/logo/mathink.ico">
	<link rel="stylesheet" href="require/css/master.css">
	
	<script src="require/js/loginForm.js"></script>
	<script src="require/js/popup.js"></script>
    <link rel="stylesheet" href="require/css/newMaster.css" />
    <script src="require/js/upTime.js"></script>

    <?php
        if(!isset($_SESSION['logado'])){
            ?>
            <script>
               $(document).ready(function(){
                   $('.requireLogin').click(function () {
                       $(this).removeAttr("href");
                       var res = confirm("Você precisa está logado\nDeseja se logar?");
                       if(res){
                           if($(this).hasClass('salaOption')){
                               window.location.href = 'cadastro.php?url=L3d3dy8xOS9zYWxhcy9saXN0YS5waHA=';
                           }else if($(this).hasClass('desafioOption')){
                               window.location.href = 'cadastro.php?url=L3d3dy8xOS9kZXNhZmlvL29ubGluZS5waHA=';
                           }
                       }
                   })
               })
            </script>

            <?php
        }
    ?>

</head>
<body>

<?php require_once 'menu.php' ?>
		<div id="notice">
			<i class="fa fa-gamepad"></i><h1>Jogue e Aprenda!</h1><p>Escolha o modo de aprendizagem</p>
		</div>
	</header>

	

	<div class="container-fluid">
        <div id="conteudo-escolhas" class="row">
            <a href="selecionarSolo.php" class="aNoStyle "> 
                <div class="col-md-4 lamina">
                    <i class="fa fa-bullseye"></i><h1>Sozinho</h1><p>Treine o conteúdo do seu ano, jogue o modo diário e evolua em 'Desvendando o Mathink'</p>
                </div>
            </a>
            <a href="salas/lista.php" class="aNoStyle requireLogin salaOption">
                <div class="col-md-4 lamina">
                    <i class="fa fa-university"></i><h1>Salas</h1><p>Complete as tarefas sugeridas pelo seu professor!</p>
                </div>
            </a>
            <a href="desafio/online.php" class="aNoStyle requireLogin desafioOption">
                <div class="col-md-4 lamina">
                    <i class="fa fa-trophy"></i><h1>Desafio</h1><p>Prove ser o melhor contra outros jogadores!</p>
                </div>
            </a>
        </div>
    </div>

<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>
</html>