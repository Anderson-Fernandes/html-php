<?php 

session_start();
if(isset($_SESSION['logado']))
	header('Location: index.php');

?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <script>var url;</script>
    <?php
    if(isset($_GET["ERRO"])){
        $erro = $_GET['ERRO'];
        if($erro == "login"){
            echo '<script>alert("Você precisa se logar para acessar a pagina")</script>';
        }
    }

    if(isset($_GET['url'])){
        $url = $_GET['url'];
        $url = base64_decode($url);
    }else{
        $url = null;
    }

    ?>


    <meta charset="UTF-8">
	<meta name="description"  content="Home" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
	<title>Mathink</title>
	<script src="require/js/jquery.js"></script>
    <link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
    <script src="require/boot/js/bootstrap.js"></script>
	<!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
	<link rel="icon" href="require/img/logo/mathink.ico">
	<link rel="stylesheet" href="require/css/master.css">
    <script src="require/js/cookie.js"></script>
    <script src="require/js/loginForm.js"></script>
    <script src="require/js/cadastro.js"></script>
    <script src="require/js/login.js"></script>

    <link rel="stylesheet" href="require/css/newMaster.css" />
</head>

<style>
	#menu #login #flutuante{
		display: none;
	}


</style>
<body>

<script>
    $(document).ready(function(){
        setLoginCookie(0);
        $('#menu #login #flutuante').fadeOut();
    });
</script>
        <?php require_once 'menu.php'; ?>
    	<div id="notice">
			 <i class="fa fa-graduation-cap"></i><h1>Login</h1><p>Faça parte da maior escola de matematica</p>
		</div>
	</header>
	
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <div class="cadastro">
                    <h2>Criar conta</h2>
                     <form method="POST" action="javascript:void(0)">
                        Nome <input type="text" placeholder="Digite seu nome" id="nome" >
                        E-mail <input type="email" placeholder="Digite seu email" id="email">
                        Confirmação de Email <input type="email" placeholder="Digite seu email" id="email2">
                         <div class="row">
                             <div class="col-md-6 text-center">
                                 Senha
                             </div>
                             <div class="col-md-6 text-center">
                                 Confirmar senha
                             </div>
                         </div>
                         <div class="row center-block" style="width: 100%">
                             <div class="col-md-6 text-center">
                                 <input type="password" placeholder="Digite sua senha" id="senha">
                             </div>
                             <div class="col-md-6 text-center">
                                 <input type="password" placeholder="Confime a sua senha" id="senha2">
                             </div>
                         </div>
                        <button onclick="fctCadastro()">Confirmar</button>
                    </form>
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="cadastro">
                    <h2>Já tenho conta</h2>
                    <form method="POST" action="javascript:void(0)">
                        E-mail <input type="text" placeholder="Digite seu email" id="Lemail">
                        Senha <input type="password" placeholder="Digite sua senha" id="Lsenha" >
                        <button onclick="fctLogin('<?= $url; ?>')">Logar</button>
                        <a href="esqueceu.php">Esqueceu a senha?</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
<footer>
    <div class="footer-copy">
        <div class="container">
            <div class="row">

                <div id="copyright" class="col-md-3">
                    <div class="row center-block">
                        <div class="col-md-10" id="imagemFooter">
                            <a href="index.php"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-left">© Todos os direitos reservados!</h4>
                        </div>
                    </div>
                </div>

                <div id="mapaSite" class="col-md-8">
                    <nav>
                        <a href="index.php">Inicio</a>
                        <a href="index.php">Desafio</a>
                        <a href="index.php">Contato</a>
                        <a href="index.php">Sobre</a>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</footer>
        
		<!--<div id="rodape"> Mathink © Copyright 2016</div>-->
</body>
</html>