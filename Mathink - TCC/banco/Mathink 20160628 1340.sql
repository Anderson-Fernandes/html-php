-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.9-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema db_mathink_novo
--

CREATE DATABASE IF NOT EXISTS db_mathink_novo;
USE db_mathink_novo;

--
-- Definition of table `aluno_conquista`
--

DROP TABLE IF EXISTS `aluno_conquista`;
CREATE TABLE `aluno_conquista` (
  `cd_aluno_conquista` int(11) NOT NULL AUTO_INCREMENT,
  `cd_conquista` int(11) NOT NULL,
  `cd_usuario` int(11) NOT NULL,
  `dt_adquirida` date DEFAULT NULL,
  PRIMARY KEY (`cd_aluno_conquista`),
  KEY `fk_aluno_conquista` (`cd_conquista`),
  KEY `fk_conquista_usuario` (`cd_usuario`),
  CONSTRAINT `fk_aluno_conquista` FOREIGN KEY (`cd_conquista`) REFERENCES `tb_conquista` (`cd_conquista`),
  CONSTRAINT `fk_conquista_usuario` FOREIGN KEY (`cd_usuario`) REFERENCES `tb_usuario` (`cd_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aluno_conquista`
--

/*!40000 ALTER TABLE `aluno_conquista` DISABLE KEYS */;
/*!40000 ALTER TABLE `aluno_conquista` ENABLE KEYS */;


--
-- Definition of table `desafio_aluno`
--

DROP TABLE IF EXISTS `desafio_aluno`;
CREATE TABLE `desafio_aluno` (
  `cd_desafio_aluno` int(11) NOT NULL AUTO_INCREMENT,
  `cd_desafio` int(11) NOT NULL,
  `cd_sala_aluno` int(11) NOT NULL,
  `qtd_acertos` int(11) DEFAULT NULL,
  `qtd_erros` int(11) DEFAULT NULL,
  `dt_concluido` date DEFAULT NULL,
  `tm_concluido` time DEFAULT NULL,
  PRIMARY KEY (`cd_desafio_aluno`),
  KEY `fk_aluno_desafio` (`cd_desafio`),
  CONSTRAINT `fk_aluno_desafio` FOREIGN KEY (`cd_desafio`) REFERENCES `tb_desafio` (`cd_desafio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desafio_aluno`
--

/*!40000 ALTER TABLE `desafio_aluno` DISABLE KEYS */;
/*!40000 ALTER TABLE `desafio_aluno` ENABLE KEYS */;


--
-- Definition of table `loja_usuario`
--

DROP TABLE IF EXISTS `loja_usuario`;
CREATE TABLE `loja_usuario` (
  `cd_usuario` int(11) NOT NULL,
  `cd_item` int(11) NOT NULL,
  `dt_adquirido` date DEFAULT NULL,
  `ic_equipado` tinyint(1) DEFAULT NULL,
  KEY `fk_usuario_loja` (`cd_item`),
  KEY `fk_loja_usuario` (`cd_usuario`),
  CONSTRAINT `fk_loja_usuario` FOREIGN KEY (`cd_usuario`) REFERENCES `tb_usuario` (`cd_usuario`),
  CONSTRAINT `fk_usuario_loja` FOREIGN KEY (`cd_item`) REFERENCES `tb_loja` (`cd_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loja_usuario`
--

/*!40000 ALTER TABLE `loja_usuario` DISABLE KEYS */;
INSERT INTO `loja_usuario` (`cd_usuario`,`cd_item`,`dt_adquirido`,`ic_equipado`) VALUES 
 (10,1,'2016-06-07',0),
 (10,13,'2016-06-07',0),
 (10,8,'2016-06-07',0),
 (11,1,'2016-06-07',1),
 (12,1,'2016-06-07',0),
 (13,1,'2016-06-07',0),
 (14,1,'2016-06-07',0),
 (15,1,'2016-06-07',0),
 (13,2,'2016-06-07',1),
 (14,6,'2016-06-07',1),
 (12,9,'2016-06-07',1),
 (15,7,'2016-06-07',1),
 (10,3,'2016-06-07',1),
 (16,1,'2016-06-07',0),
 (16,13,'2016-06-07',1),
 (17,1,'2016-06-10',0),
 (17,8,'2016-06-10',0),
 (17,13,'2016-06-10',1);
/*!40000 ALTER TABLE `loja_usuario` ENABLE KEYS */;


--
-- Definition of table `mensagem_perfil`
--

DROP TABLE IF EXISTS `mensagem_perfil`;
CREATE TABLE `mensagem_perfil` (
  `cd_resposta` int(11) NOT NULL AUTO_INCREMENT,
  `ds_resposta` text,
  `dt_resposta` date DEFAULT NULL,
  `tm_resposta` time DEFAULT NULL,
  `cd_usuario` int(11) NOT NULL,
  `cd_remetente` int(11) NOT NULL,
  PRIMARY KEY (`cd_resposta`),
  KEY `fk_usuario_recebe` (`cd_usuario`),
  KEY `fk_usuario_remetente` (`cd_remetente`),
  CONSTRAINT `fk_usuario_recebe` FOREIGN KEY (`cd_usuario`) REFERENCES `tb_usuario` (`cd_usuario`),
  CONSTRAINT `fk_usuario_remetente` FOREIGN KEY (`cd_remetente`) REFERENCES `tb_usuario` (`cd_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mensagem_perfil`
--

/*!40000 ALTER TABLE `mensagem_perfil` DISABLE KEYS */;
INSERT INTO `mensagem_perfil` (`cd_resposta`,`ds_resposta`,`dt_resposta`,`tm_resposta`,`cd_usuario`,`cd_remetente`) VALUES 
 (1,'Oiiiiieee','2016-06-07','17:26:36',16,12),
 (2,'Olá pessoal!','2016-06-10','09:12:30',17,17);
/*!40000 ALTER TABLE `mensagem_perfil` ENABLE KEYS */;


--
-- Definition of table `mensagem_resposta`
--

DROP TABLE IF EXISTS `mensagem_resposta`;
CREATE TABLE `mensagem_resposta` (
  `cd_resposta` int(11) NOT NULL AUTO_INCREMENT,
  `ds_resposta` text,
  `dt_resposta` date DEFAULT NULL,
  `tm_resposta` time DEFAULT NULL,
  `cd_sala_aluno` int(11) NOT NULL,
  `cd_mensagem` int(11) NOT NULL,
  PRIMARY KEY (`cd_resposta`),
  KEY `fk_resposta_mensagem` (`cd_mensagem`),
  KEY `fk_resposta_usuario` (`cd_sala_aluno`),
  CONSTRAINT `fk_resposta_mensagem` FOREIGN KEY (`cd_mensagem`) REFERENCES `sala_mensagem` (`cd_mensagem`),
  CONSTRAINT `fk_resposta_usuario` FOREIGN KEY (`cd_sala_aluno`) REFERENCES `sala_mensagem` (`cd_sala_aluno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mensagem_resposta`
--

/*!40000 ALTER TABLE `mensagem_resposta` DISABLE KEYS */;
/*!40000 ALTER TABLE `mensagem_resposta` ENABLE KEYS */;


--
-- Definition of table `sala_aluno`
--

DROP TABLE IF EXISTS `sala_aluno`;
CREATE TABLE `sala_aluno` (
  `cd_sala_aluno` int(11) NOT NULL AUTO_INCREMENT,
  `cd_usuario` int(11) NOT NULL,
  `cd_sala` int(11) NOT NULL,
  `cd_autoridade` tinyint(5) DEFAULT NULL,
  `dt_entrada` date DEFAULT NULL,
  `vl_avaliacao` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`cd_sala_aluno`),
  KEY `fk_aluno_sala` (`cd_sala`),
  KEY `fk_sala_usuario` (`cd_usuario`),
  CONSTRAINT `fk_aluno_sala` FOREIGN KEY (`cd_sala`) REFERENCES `tb_sala` (`cd_sala`),
  CONSTRAINT `fk_sala_usuario` FOREIGN KEY (`cd_usuario`) REFERENCES `tb_usuario` (`cd_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sala_aluno`
--

/*!40000 ALTER TABLE `sala_aluno` DISABLE KEYS */;
INSERT INTO `sala_aluno` (`cd_sala_aluno`,`cd_usuario`,`cd_sala`,`cd_autoridade`,`dt_entrada`,`vl_avaliacao`) VALUES 
 (17,10,24,5,NULL,NULL),
 (18,12,25,5,NULL,NULL),
 (19,11,25,1,'2016-06-07',NULL),
 (20,13,25,NULL,'2016-06-07',NULL),
 (21,14,25,NULL,'2016-06-07',NULL),
 (22,15,26,5,NULL,NULL),
 (23,15,25,NULL,'2016-06-07',NULL),
 (24,15,27,5,NULL,NULL),
 (25,12,27,1,'2016-06-07',NULL),
 (26,16,25,NULL,'2016-06-07',NULL),
 (27,17,25,NULL,'2016-06-10',NULL),
 (28,17,26,1,'2016-06-10',NULL),
 (29,17,28,5,NULL,NULL),
 (30,13,29,5,NULL,NULL),
 (31,14,29,1,'2016-06-28',NULL),
 (32,14,30,5,NULL,NULL),
 (33,13,30,1,'2016-06-28',NULL);
/*!40000 ALTER TABLE `sala_aluno` ENABLE KEYS */;


--
-- Definition of trigger `tr_usuario_professor`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_usuario_professor`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_usuario_professor` AFTER INSERT ON `sala_aluno` FOR EACH ROW begin
      if exists(select * from sala_aluno where cd_sala_aluno = new.cd_sala_aluno AND cd_autoridade = 5) then
        if not exists(select * from tb_professor where cd_usuario = new.cd_usuario) then
          insert into tb_professor set cd_usuario = new.cd_usuario;
               end if;
       end if;


       if exists(select * from sala_aluno where cd_sala_aluno = new.cd_sala_aluno AND cd_autoridade is null) then
      insert into tb_notificacao_sala set dt_notificacao = curdate(), tm_notificacao = curtime(), cd_sala_aluno = new.cd_sala_aluno;
    end if;

  end $$

DELIMITER ;

--
-- Definition of trigger `tr_usuario_professor_del`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_usuario_professor_del`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_usuario_professor_del` BEFORE DELETE ON `sala_aluno` FOR EACH ROW begin
    delete from tb_notificacao_sala where cd_sala_aluno = old.cd_sala_aluno;
  end $$

DELIMITER ;

--
-- Definition of table `sala_mensagem`
--

DROP TABLE IF EXISTS `sala_mensagem`;
CREATE TABLE `sala_mensagem` (
  `cd_mensagem` int(11) NOT NULL AUTO_INCREMENT,
  `nm_titulo` varchar(225) NOT NULL,
  `ds_mensagem` text NOT NULL,
  `ic_aprovado` tinyint(1) DEFAULT NULL,
  `dt_criacao` date DEFAULT NULL,
  `tm_criacao` time DEFAULT NULL,
  `cd_sala_aluno` int(11) NOT NULL,
  `ic_resolvido` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`cd_mensagem`),
  KEY `fk_mensagem_sala_aluno` (`cd_sala_aluno`),
  CONSTRAINT `fk_mensagem_sala_aluno` FOREIGN KEY (`cd_sala_aluno`) REFERENCES `sala_aluno` (`cd_sala_aluno`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sala_mensagem`
--

/*!40000 ALTER TABLE `sala_mensagem` DISABLE KEYS */;
INSERT INTO `sala_mensagem` (`cd_mensagem`,`nm_titulo`,`ds_mensagem`,`ic_aprovado`,`dt_criacao`,`tm_criacao`,`cd_sala_aluno`,`ic_resolvido`) VALUES 
 (14,'Olá alunos','Sejam bem-vindos ao Clube do Mathink',NULL,'2016-06-07','17:29:20',18,1),
 (15,'Dúvidas da prova','1ªTava muito difícil',NULL,'2016-06-10','09:15:53',29,1);
/*!40000 ALTER TABLE `sala_mensagem` ENABLE KEYS */;


--
-- Definition of table `tb_aluno`
--

DROP TABLE IF EXISTS `tb_aluno`;
CREATE TABLE `tb_aluno` (
  `cd_usuario` int(11) NOT NULL,
  `nr_level` int(11) DEFAULT '1',
  `vl_cash` int(11) DEFAULT '0',
  `vl_exp` double DEFAULT '0',
  `vl_points` int(11) DEFAULT '0',
  `nm_nickname` varchar(50) DEFAULT NULL,
  `nm_color` varchar(20) DEFAULT NULL,
  UNIQUE KEY `nm_nickname` (`nm_nickname`),
  KEY `fk_aluno_usuario` (`cd_usuario`),
  CONSTRAINT `fk_aluno_usuario` FOREIGN KEY (`cd_usuario`) REFERENCES `tb_usuario` (`cd_usuario`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_aluno`
--

/*!40000 ALTER TABLE `tb_aluno` DISABLE KEYS */;
INSERT INTO `tb_aluno` (`cd_usuario`,`nr_level`,`vl_cash`,`vl_exp`,`vl_points`,`nm_nickname`,`nm_color`) VALUES 
 (10,50,0,0,300,'[TH] AnderSun','roxo'),
 (11,50,0,0,200,'[TH] Jesus','verde'),
 (12,50,0,0,500,'[TH] Rick','preto'),
 (13,1,0,0,0,'JessFontes','rosa'),
 (14,1,0,0,0,'DouglasMn','branco'),
 (15,1,0,0,0,'Fab27','roxo'),
 (16,1,28,0,16,'Llucas','rosa'),
 (17,1,18,0,20,'Quantico','branco');
/*!40000 ALTER TABLE `tb_aluno` ENABLE KEYS */;


--
-- Definition of trigger `tr_foto_perfil`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_foto_perfil`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_foto_perfil` AFTER INSERT ON `tb_aluno` FOR EACH ROW begin
      insert into loja_usuario values (new.cd_usuario,1,curdate(),1);
    end $$

DELIMITER ;

--
-- Definition of table `tb_amigo`
--

DROP TABLE IF EXISTS `tb_amigo`;
CREATE TABLE `tb_amigo` (
  `cd_amizade` int(11) NOT NULL AUTO_INCREMENT,
  `cd_usuario` int(11) NOT NULL,
  `cd_amigo` int(11) NOT NULL,
  `ic_aprovado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`cd_amizade`),
  KEY `fk_amigo_usuario` (`cd_usuario`),
  KEY `fk_usuario_amigo` (`cd_amigo`),
  CONSTRAINT `fk_amigo_usuario` FOREIGN KEY (`cd_usuario`) REFERENCES `tb_usuario` (`cd_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_amigo`
--

/*!40000 ALTER TABLE `tb_amigo` DISABLE KEYS */;
INSERT INTO `tb_amigo` (`cd_amizade`,`cd_usuario`,`cd_amigo`,`ic_aprovado`) VALUES 
 (8,10,13,NULL),
 (9,12,16,1),
 (10,12,17,NULL);
/*!40000 ALTER TABLE `tb_amigo` ENABLE KEYS */;


--
-- Definition of trigger `tr_usuario_amigo`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_usuario_amigo`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_usuario_amigo` AFTER INSERT ON `tb_amigo` FOR EACH ROW begin
      insert tb_notificacao_amigo set dt_notificacao = curdate(), tm_notificacao = curtime(), cd_amizade = new.cd_amizade;
  end $$

DELIMITER ;

--
-- Definition of table `tb_conquista`
--

DROP TABLE IF EXISTS `tb_conquista`;
CREATE TABLE `tb_conquista` (
  `cd_conquista` int(11) NOT NULL AUTO_INCREMENT,
  `nm_conquista` varchar(225) DEFAULT NULL,
  `ds_conquista` text,
  `ic_secreto` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`cd_conquista`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_conquista`
--

/*!40000 ALTER TABLE `tb_conquista` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_conquista` ENABLE KEYS */;


--
-- Definition of table `tb_desafio`
--

DROP TABLE IF EXISTS `tb_desafio`;
CREATE TABLE `tb_desafio` (
  `cd_desafio` int(11) NOT NULL AUTO_INCREMENT,
  `cd_sala_aluno` int(11) NOT NULL,
  `dt_desafio` date DEFAULT NULL,
  `nm_desafio` varchar(225) DEFAULT NULL,
  `nm_exercicio` text,
  `dt_expira` date NOT NULL,
  PRIMARY KEY (`cd_desafio`),
  KEY `fk_desafio_aluno` (`cd_sala_aluno`),
  CONSTRAINT `fk_desafio_aluno` FOREIGN KEY (`cd_sala_aluno`) REFERENCES `sala_aluno` (`cd_sala_aluno`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_desafio`
--

/*!40000 ALTER TABLE `tb_desafio` DISABLE KEYS */;
INSERT INTO `tb_desafio` (`cd_desafio`,`cd_sala_aluno`,`dt_desafio`,`nm_desafio`,`nm_exercicio`,`dt_expira`) VALUES 
 (4,18,'2016-06-07','Desafio Inicial','[\"2\",\"1\",\"6\"]','2016-06-08'),
 (5,29,'2016-06-10','Teste Complicado','[\"8\",\"2\",\"3\",\"1\",\"5\"]','2016-06-11');
/*!40000 ALTER TABLE `tb_desafio` ENABLE KEYS */;


--
-- Definition of table `tb_exercicio`
--

DROP TABLE IF EXISTS `tb_exercicio`;
CREATE TABLE `tb_exercicio` (
  `cd_exercicio` int(11) NOT NULL AUTO_INCREMENT,
  `nm_exercicio` varchar(255) NOT NULL,
  `vl_pontos` int(11) DEFAULT NULL,
  `qtd_acertos` int(11) DEFAULT NULL,
  `qtd_Erros` int(11) DEFAULT NULL,
  `vl_level` int(11) DEFAULT NULL,
  `dt_ultimo_jogo` date DEFAULT NULL,
  `cd_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_exercicio`),
  KEY `fk_exercicio_usuario` (`cd_usuario`),
  CONSTRAINT `fk_exercicio_usuario` FOREIGN KEY (`cd_usuario`) REFERENCES `tb_aluno` (`cd_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_exercicio`
--

/*!40000 ALTER TABLE `tb_exercicio` DISABLE KEYS */;
INSERT INTO `tb_exercicio` (`cd_exercicio`,`nm_exercicio`,`vl_pontos`,`qtd_acertos`,`qtd_Erros`,`vl_level`,`dt_ultimo_jogo`,`cd_usuario`) VALUES 
 (1,'Subtracao',16,10,2,2,'2016-06-07',16),
 (2,'Adicao',20,10,0,2,'2016-06-10',17);
/*!40000 ALTER TABLE `tb_exercicio` ENABLE KEYS */;


--
-- Definition of table `tb_exercicio_professor`
--

DROP TABLE IF EXISTS `tb_exercicio_professor`;
CREATE TABLE `tb_exercicio_professor` (
  `cd_exercicio` int(11) NOT NULL AUTO_INCREMENT,
  `nm_titulo` varchar(255) NOT NULL,
  `ds_exercicio` text NOT NULL,
  `ds_formula` varchar(225) DEFAULT NULL,
  `nm_imagem` varchar(255) DEFAULT NULL,
  `ic_privacidade` tinyint(1) DEFAULT NULL,
  `nm_alternativas` text,
  `vl_dificuldade` int(11) DEFAULT NULL,
  `cd_resposta_correta` int(11) DEFAULT '0',
  `cd_usuario` int(11) NOT NULL,
  PRIMARY KEY (`cd_exercicio`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_exercicio_professor`
--

/*!40000 ALTER TABLE `tb_exercicio_professor` DISABLE KEYS */;
INSERT INTO `tb_exercicio_professor` (`cd_exercicio`,`nm_titulo`,`ds_exercicio`,`ds_formula`,`nm_imagem`,`ic_privacidade`,`nm_alternativas`,`vl_dificuldade`,`cd_resposta_correta`,`cd_usuario`) VALUES 
 (1,'Soma Simples','João tinha 5 maçãs, sua mãe lhe deu mais 2, com quantas maçãs ele ficou.',NULL,NULL,1,'[\"7\",\"3\",\"5\",\"6\"]',NULL,1,12),
 (2,'Subtração Mediana','Marcelo recebeu 20 reais de sua mãe para comprar pão. Na compra, gastou 5 reais. Quanto de dinheiro sobrou?',NULL,NULL,1,'[\"15\",\"14\",\"18\",\"20\",\"13\",\"12\"]',NULL,1,12),
 (3,'Soma Simples 2','Jorge tinha 3 cachorros em casa, porém um deles ficou na casa de sua vó. Com quantos ele ficou?',NULL,NULL,1,'[\"4\",\"5\",\"1\",\"2\",\"3\"]',NULL,4,12),
 (4,'Divisão Mediana','Dimitre tinha 10 biscoitos, e dividiu com seus 4 amigos. Quantos biscoitos cada um recebeu?',NULL,NULL,1,'[\"4\",\"5\",\"8\",\"7\",\"3\",\"2\"]',NULL,6,12),
 (5,'Multiplicação Simples','Fernanda tinha 5 maçãs, e decidiu comprar o dobro da quantidade que possuía. Quantas maçãs ela decidiu comprar?',NULL,NULL,1,'[\"8\",\"10\",\"7\",\"9\"]',NULL,2,12),
 (6,'Somando Laranjas','Márcia precisava de 50 laranjas, porém só tinha 25. De quantas laranjas ela precisa?',NULL,NULL,1,'[\"23\",\"27\",\"28\",\"25\"]',NULL,4,15),
 (7,'Exercício de Soma','Maria tinha 5 pirulitos e comeu 2, com quantos ela ficou?',NULL,NULL,1,'[\"4\",\"2\",\"3\",\"1\",\"6\"]',NULL,3,12),
 (8,'Exerício de Soma','João tinha 3 maçãs e ganhou mais 2, quantas maçãs João tem?',NULL,NULL,1,'[\"5\",\"2\",\"8\",\"9\",\"6\",\"4\"]',NULL,1,17);
/*!40000 ALTER TABLE `tb_exercicio_professor` ENABLE KEYS */;


--
-- Definition of table `tb_loja`
--

DROP TABLE IF EXISTS `tb_loja`;
CREATE TABLE `tb_loja` (
  `cd_item` int(11) NOT NULL AUTO_INCREMENT,
  `nm_item` varchar(225) NOT NULL,
  `nm_tipo` varchar(225) NOT NULL,
  `vl_preco` decimal(9,2) NOT NULL,
  `nr_level_necessario` int(11) DEFAULT '0',
  PRIMARY KEY (`cd_item`),
  UNIQUE KEY `nm_item` (`nm_item`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_loja`
--

/*!40000 ALTER TABLE `tb_loja` DISABLE KEYS */;
INSERT INTO `tb_loja` (`cd_item`,`nm_item`,`nm_tipo`,`vl_preco`,`nr_level_necessario`) VALUES 
 (1,'Lampi','imagem-perfil','1.00',1),
 (2,'Lamparina','imagem-perfil','15.00',3),
 (3,'LampiAranha','imagem-perfil','35.00',5),
 (5,'LampiBirrrrr','imagem-perfil','13.00',13),
 (6,'LampiChaves','imagem-perfil','1.00',2),
 (7,'LampiCoelho','imagem-perfil','22.00',15),
 (8,'LampiEsquisito','imagem-perfil','10.00',1),
 (9,'LampiHP','imagem-perfil','50.00',20),
 (10,'LampiNegro','imagem-perfil','8.00',12),
 (11,'Lampinion','imagem-perfil','42.00',20),
 (12,'LampiNoel','imagem-perfil','13.00',3),
 (13,'LampiPensador','imagem-perfil','5.00',1),
 (14,'Lampirata','imagem-perfil','22.00',14),
 (15,'Lampiruto','imagem-perfil','130.00',26),
 (16,'Lampones','imagem-perfil','21.00',5);
/*!40000 ALTER TABLE `tb_loja` ENABLE KEYS */;


--
-- Definition of table `tb_notificacao_amigo`
--

DROP TABLE IF EXISTS `tb_notificacao_amigo`;
CREATE TABLE `tb_notificacao_amigo` (
  `cd_notificacao` int(11) NOT NULL AUTO_INCREMENT,
  `dt_notificacao` date DEFAULT NULL,
  `tm_notificacao` time DEFAULT NULL,
  `ic_visualizou` tinyint(1) DEFAULT NULL,
  `cd_amizade` int(11) NOT NULL,
  PRIMARY KEY (`cd_notificacao`),
  KEY `fk_amizade` (`cd_amizade`),
  CONSTRAINT `fk_amizade` FOREIGN KEY (`cd_amizade`) REFERENCES `tb_amigo` (`cd_amizade`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notificacao_amigo`
--

/*!40000 ALTER TABLE `tb_notificacao_amigo` DISABLE KEYS */;
INSERT INTO `tb_notificacao_amigo` (`cd_notificacao`,`dt_notificacao`,`tm_notificacao`,`ic_visualizou`,`cd_amizade`) VALUES 
 (2,'2016-06-07','16:01:23',NULL,8),
 (3,'2016-06-10','09:13:12',NULL,10);
/*!40000 ALTER TABLE `tb_notificacao_amigo` ENABLE KEYS */;


--
-- Definition of table `tb_notificacao_conquista`
--

DROP TABLE IF EXISTS `tb_notificacao_conquista`;
CREATE TABLE `tb_notificacao_conquista` (
  `cd_notificacao` int(11) NOT NULL AUTO_INCREMENT,
  `dt_notificacao` date DEFAULT NULL,
  `tm_notificacao` time DEFAULT NULL,
  `ic_visualizou` tinyint(1) DEFAULT NULL,
  `cd_conquista` int(11) NOT NULL,
  PRIMARY KEY (`cd_notificacao`),
  KEY `fk_notificacao_conquista` (`cd_conquista`),
  CONSTRAINT `fk_notificacao_conquista` FOREIGN KEY (`cd_conquista`) REFERENCES `tb_conquista` (`cd_conquista`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notificacao_conquista`
--

/*!40000 ALTER TABLE `tb_notificacao_conquista` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_notificacao_conquista` ENABLE KEYS */;


--
-- Definition of table `tb_notificacao_desafio`
--

DROP TABLE IF EXISTS `tb_notificacao_desafio`;
CREATE TABLE `tb_notificacao_desafio` (
  `cd_notificacao` int(11) NOT NULL AUTO_INCREMENT,
  `dt_notificacao` date DEFAULT NULL,
  `tm_notificacao` time DEFAULT NULL,
  `ic_visualizou` tinyint(1) DEFAULT NULL,
  `cd_desafio_aluno` int(11) NOT NULL,
  PRIMARY KEY (`cd_notificacao`),
  KEY `fk_notificacao_desafio` (`cd_desafio_aluno`),
  CONSTRAINT `fk_notificacao_desafio` FOREIGN KEY (`cd_desafio_aluno`) REFERENCES `desafio_aluno` (`cd_desafio_aluno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notificacao_desafio`
--

/*!40000 ALTER TABLE `tb_notificacao_desafio` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_notificacao_desafio` ENABLE KEYS */;


--
-- Definition of table `tb_notificacao_sala`
--

DROP TABLE IF EXISTS `tb_notificacao_sala`;
CREATE TABLE `tb_notificacao_sala` (
  `cd_notificacao` int(11) NOT NULL AUTO_INCREMENT,
  `dt_notificacao` date DEFAULT NULL,
  `tm_notificacao` time DEFAULT NULL,
  `ic_visualizou` tinyint(1) DEFAULT NULL,
  `cd_sala_aluno` int(11) NOT NULL,
  PRIMARY KEY (`cd_notificacao`),
  KEY `fk_sala_aluno` (`cd_sala_aluno`),
  CONSTRAINT `fk_sala_aluno` FOREIGN KEY (`cd_sala_aluno`) REFERENCES `sala_aluno` (`cd_sala_aluno`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notificacao_sala`
--

/*!40000 ALTER TABLE `tb_notificacao_sala` DISABLE KEYS */;
INSERT INTO `tb_notificacao_sala` (`cd_notificacao`,`dt_notificacao`,`tm_notificacao`,`ic_visualizou`,`cd_sala_aluno`) VALUES 
 (2,'2016-06-07','14:27:09',0,20),
 (3,'2016-06-07','14:30:29',0,21),
 (4,'2016-06-07','14:52:30',0,23),
 (6,'2016-06-07','17:28:27',0,26),
 (7,'2016-06-10','09:14:13',NULL,27);
/*!40000 ALTER TABLE `tb_notificacao_sala` ENABLE KEYS */;


--
-- Definition of table `tb_professor`
--

DROP TABLE IF EXISTS `tb_professor`;
CREATE TABLE `tb_professor` (
  `cd_usuario` int(11) NOT NULL,
  `nr_level` int(11) DEFAULT '1',
  `vl_exp` double DEFAULT '0',
  UNIQUE KEY `cd_usuario` (`cd_usuario`),
  CONSTRAINT `fk_usuario_professor` FOREIGN KEY (`cd_usuario`) REFERENCES `tb_usuario` (`cd_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_professor`
--

/*!40000 ALTER TABLE `tb_professor` DISABLE KEYS */;
INSERT INTO `tb_professor` (`cd_usuario`,`nr_level`,`vl_exp`) VALUES 
 (10,1,0),
 (12,1,0),
 (13,1,0),
 (14,1,0),
 (15,1,0),
 (17,1,0);
/*!40000 ALTER TABLE `tb_professor` ENABLE KEYS */;


--
-- Definition of table `tb_ranking`
--

DROP TABLE IF EXISTS `tb_ranking`;
CREATE TABLE `tb_ranking` (
  `cd_usuario` int(11) NOT NULL,
  `vl_ultimo_rank` int(11) NOT NULL,
  `vl_antigo_rank` int(11) DEFAULT NULL,
  `dt_alteracao` date DEFAULT NULL,
  `tm_alteracao` time DEFAULT NULL,
  KEY `fk_ranking_usuario` (`cd_usuario`),
  CONSTRAINT `fk_ranking_usuario` FOREIGN KEY (`cd_usuario`) REFERENCES `tb_usuario` (`cd_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ranking`
--

/*!40000 ALTER TABLE `tb_ranking` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_ranking` ENABLE KEYS */;


--
-- Definition of table `tb_sala`
--

DROP TABLE IF EXISTS `tb_sala`;
CREATE TABLE `tb_sala` (
  `cd_sala` int(11) NOT NULL AUTO_INCREMENT,
  `nm_sala` varchar(225) DEFAULT NULL,
  `dt_criacao` date DEFAULT NULL,
  `ic_privacidade` tinyint(1) DEFAULT NULL,
  `cd_senha` char(128) DEFAULT NULL,
  PRIMARY KEY (`cd_sala`),
  UNIQUE KEY `nm_sala` (`nm_sala`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sala`
--

/*!40000 ALTER TABLE `tb_sala` DISABLE KEYS */;
INSERT INTO `tb_sala` (`cd_sala`,`nm_sala`,`dt_criacao`,`ic_privacidade`,`cd_senha`) VALUES 
 (24,'Mathinkeiros','2016-06-07',1,NULL),
 (25,'Clube do Mathink','2016-06-07',1,NULL),
 (26,'5º ano B','2016-06-07',1,'1c62d496bbbbf88d8dc0bd39e6a0939294c3cfcb4467be4dd349e19d2561f48c54c3ce260bc0992ea1aee561caccec441e6a993795351913aead35e84677238b'),
 (27,'Área Matemática','2016-06-07',1,NULL),
 (28,'Senai','2016-06-10',1,NULL),
 (29,'Thinkers','2016-06-28',1,'1c62d496bbbbf88d8dc0bd39e6a0939294c3cfcb4467be4dd349e19d2561f48c54c3ce260bc0992ea1aee561caccec441e6a993795351913aead35e84677238b'),
 (30,'Etec Rutinha','2016-06-28',1,NULL);
/*!40000 ALTER TABLE `tb_sala` ENABLE KEYS */;


--
-- Definition of table `tb_usuario`
--

DROP TABLE IF EXISTS `tb_usuario`;
CREATE TABLE `tb_usuario` (
  `cd_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nm_usuario` varchar(255) NOT NULL,
  `nm_email` varchar(255) NOT NULL,
  `cd_senha` char(128) NOT NULL,
  `dt_criacao` date DEFAULT NULL,
  `tm_online` time DEFAULT NULL,
  PRIMARY KEY (`cd_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_usuario`
--

/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
INSERT INTO `tb_usuario` (`cd_usuario`,`nm_usuario`,`nm_email`,`cd_senha`,`dt_criacao`,`tm_online`) VALUES 
 (10,'Anderson Fernandes','anderson_vado@hotmail.com','e278cf06340c682a11d2837e5a334868b916cc09e45b86d7dc104edc2b297201525426d394a9ef881d04514370190229c349a47d7358d287e68cb9037a07c178','2016-06-07','16:44:59'),
 (11,'Lucas Alves','f@f.com','1c62d496bbbbf88d8dc0bd39e6a0939294c3cfcb4467be4dd349e19d2561f48c54c3ce260bc0992ea1aee561caccec441e6a993795351913aead35e84677238b','2016-06-07','14:25:50'),
 (12,'Ricardo Fernandes','ricardo@mathink.com','1c62d496bbbbf88d8dc0bd39e6a0939294c3cfcb4467be4dd349e19d2561f48c54c3ce260bc0992ea1aee561caccec441e6a993795351913aead35e84677238b','2016-06-07','18:44:45'),
 (13,'Jéssica','jess@jess.com','1c62d496bbbbf88d8dc0bd39e6a0939294c3cfcb4467be4dd349e19d2561f48c54c3ce260bc0992ea1aee561caccec441e6a993795351913aead35e84677238b','2016-06-07','13:40:16'),
 (14,'Douglas Mineiro','d@d.com','1c62d496bbbbf88d8dc0bd39e6a0939294c3cfcb4467be4dd349e19d2561f48c54c3ce260bc0992ea1aee561caccec441e6a993795351913aead35e84677238b','2016-06-07','13:36:36'),
 (15,'Fabiana','fab@fab.com','1c62d496bbbbf88d8dc0bd39e6a0939294c3cfcb4467be4dd349e19d2561f48c54c3ce260bc0992ea1aee561caccec441e6a993795351913aead35e84677238b','2016-06-07','15:48:41'),
 (16,'Lucas','lucas@hotmail.com','1c62d496bbbbf88d8dc0bd39e6a0939294c3cfcb4467be4dd349e19d2561f48c54c3ce260bc0992ea1aee561caccec441e6a993795351913aead35e84677238b','2016-06-07','17:32:29'),
 (17,'Robson','robson@gmail.com','1c62d496bbbbf88d8dc0bd39e6a0939294c3cfcb4467be4dd349e19d2561f48c54c3ce260bc0992ea1aee561caccec441e6a993795351913aead35e84677238b','2016-06-10','09:22:06');
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;


--
-- Definition of trigger `tr_usuario_aluno`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_usuario_aluno`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_usuario_aluno` AFTER INSERT ON `tb_usuario` FOR EACH ROW begin
      insert into tb_aluno set cd_usuario = new.cd_usuario;
    end $$

DELIMITER ;

--
-- Definition of procedure `pr_sala_professor`
--

DROP PROCEDURE IF EXISTS `pr_sala_professor`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `pr_sala_professor`(in sala varchar(225), in usuario int, privacidade boolean, senha char(128) )
begin
      insert into tb_sala set nm_sala = sala, dt_criacao = curdate(), ic_privacidade = privacidade , cd_senha = senha;
            set @sala = ( SELECT LAST_INSERT_ID());
      insert into sala_aluno set cd_usuario = usuario, cd_sala = @sala, cd_autoridade = 5;
    end $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
