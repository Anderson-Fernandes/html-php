<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 07/06/2016
 * Time: 09:33
 */

?>


<script>

$(document).ready(function(){
   $("#menu .row").fadeIn();

    $("#voltar").click(function(){
        $('#menu .row').fadeOut(function(){
            $('#menu').load("menu/default.php");
        });
    });

    $("#multip").click(function(){
        location.href="jogos/multiplicacao.php";
    });
    $("#soma").click(function(){
        location.href="jogos/soma.php";
    });
    $("#subtra").click(function(){
        location.href="jogos/subtracao.php";
    });
    $("#divisao").click(function(){
        location.href="jogos/divisao.php";
    });


});







</script>


<style>
    #menu .row i{
        font-size:8em;
    }
</style>


<div class="row" style="display: none; ">
    <div class="col-md-12">
        <div class="row">
             <div class="col-md-8 text-center" style="color:#333333">
                <div class="row">

                    <div id="soma" class="col-md-6 menuButton">
                        <i class="fa fa-plus"  aria-hidden="true"></i>
                        <h2>Adição</h2>
                    </div>
                    <div id="subtra" class="col-md-6 menuButton">
                        <i class="fa fa-minus"  aria-hidden="true"></i>
                        <h2>Subtração</h2>

                    </div>

                </div>

                <div class="row">

                    <div  id="divisao" class="col-md-6 menuButton">
                        <i class="fa fa-th"  aria-hidden="true"></i>
                        <h2>Divisão</h2>

                    </div>
                    <div id="multip" class="col-md-6 menuButton">
                        <i class="fa fa-times"  aria-hidden="true"></i>
                        <h2>Mutiplicação</h2>
                    </div>

                </div>


            </div>

            <div id="voltar" class="col-md-1 menuButton" style="margin:1em 0;padding-top: 7em;height: 30em">
                <i style="margin-left: 0.3em" class="text-center fa fa-arrow-left" aria-hidden="true"></i>
                <h2 class="text-center">Voltar</h2>
            </div>

        </div>

    </div>
</div>

