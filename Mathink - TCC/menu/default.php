<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 07/06/2016
 * Time: 05:48
 */

require_once '../perfil/require/php/marketItens.php';
require_once '../require/class/conDB.class.php';
require_once '../require/php/logado.php';

$perfilImage = getPerfilImg($_SESSION['logado']);
// Capitura de Dados


$id = $_SESSION['logado'] ;
$crud = new CRUD;
$log = $crud->select('nm_nickname','tb_aluno','WHERE cd_usuario =?',array($id));
if($log->rowCount() >0)
{
    foreach ($log as $dds) {
        $nome = $dds['nm_nickname'];
    }
}

$log = $crud->select('nr_level, vl_cash, vl_exp','tb_usuario, tb_aluno', 'WHERE tb_usuario.cd_usuario =? and tb_aluno.cd_usuario =?',array($id,$id));
if($log->rowCount()>0)
{
    foreach ($log as $dds) {
        $level = $dds['nr_level'];
        $cash = $dds['vl_cash'];
        $exp = $dds['vl_exp'];
    }
}

// Numero de Amigos
$logAmigos = $crud->select('*','tb_amigo', 'WHERE cd_usuario_amigo =? OR cd_amigo_usuario=?',array($id,$id));
$amigos = $logAmigos -> rowCount();

//Fim Informações


?>

<script>

    $(document).ready(function(){
        var jogoMenu = function (){
            $('#menu').load("menu/jogar.php");
        }

        //perfil
        var clickEventLoadMenu = function (id,defaultColor,newColor,link){

            clickEvent(id,function(){
                if(id == "menuJogar") {
                    $("#menu .row").fadeOut(
                        function () {
                            if (id == "menuJogar") {
                                jogoMenu();
                            }
                        }
                    );
                }

                if(id == "perfilMenu"){
                    trocaPagina("perfil.php");
                }

                if (id == "menuConfig"){
                    trocaPagina("perfil/market.php");
                }

                if(id == "menuRank"){
                    trocaPagina("desafio/melhores.php");
                }

                    if(id == "menuSala"){
                        trocaPagina("salas/lista.php");
                    }

                },200,
                function(){},
                function(){
                    trocaPagina(link);
                },800
                ,function(){
                    setTimeout(function(){
                        document.getElementById(id).style.transition = "1000ms";
                        document.getElementById(id).style.backgroundColor = newColor;
                    },5);

                },
                function(){
                    document.getElementById(id).style.transition = "";
                    document.getElementById(id).style.backgroundColor = '';
                });
        };

        var defaultColor = "#C7C6FF";
        var newColor = "#73739b";

        clickEventLoadMenu("perfilMenu",defaultColor,newColor,"perfil.php");
        clickEventLoadMenu("menuSala",defaultColor,newColor,"salas/lista.php");
        clickEventLoadMenu("menuJogar",defaultColor,newColor,"selecionarSolo.php");
        clickEventLoadMenu("menuConfig",defaultColor,newColor,"perfil/market.php");
        clickEventLoadMenu("menuRank",defaultColor,newColor,"desafio/melhores.php");


    })
</script>

<div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8 ">
                                    <div class="row">

                                        <div id="menuJogar" class="col-md-4 menuButton">
                                           <img src="require/img/menu/jogar.svg"/>
                                        </div>
                                        <div id="menuSala" class="col-md-4 menuButton">
                                            <img src="require/img/menu/sala.svg"/>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div  id="menuRank" class="col-md-4 menuButton">
                                            <img src="require/img/menu/rank.svg"/>
                                        </div>
                                        <div id="menuConfig" class="col-md-4 menuButton">
                                            <img src="require/img/menu/market.svg"/>
                                        </div>

                                    </div>


                                </div>

                                <div id="perfilMenu" class="col-md-4">
                                    <img class="img-responsive center-block" src="perfil/require/img/equips/<?=$perfilImage?>" />
                                    <h1 class="text-center"><?= $nome?></h1>
<h3>Level: <?= $level ?></h3>
<h3>PI Coins: <?= $cash ?></h3>
<h3>Amigos: <?= $amigos ?></h3>
<h3>Conquistas: 0 de 0</h3>
<?php if (1 == 1): ?>
    <a href="logoff.php"  style="z-index: 9800; font-size:1.5em;">Sair</a>
<?php endif; ?>
</div>

</div>

</div>