<?php
session_start();
require_once '../require/class/conDB.class.php';
$crud = new CRUD;
$nomeGame = 'Multiplicação';
$estaLogado = (isset($_SESSION['logado']))?true:false;


if($estaLogado){
	$jogador = $_SESSION['logado'];


	$log = $crud->select('vl_level','tb_exercicio','WHERE nm_exercicio=? AND cd_usuario=?',array($nomeGame,$jogador));
	if($log->rowCount() >0)
	{
		foreach ($log as $dds) {
			$porcento = (int)0;
			$Preparelevel = (int)$dds['vl_level'];
		}
	}else{
		$porcento =(int)0;
		$Preparelevel = (int)1;
	}

}else{
	$porcento =(int)0;
	$Preparelevel = (int)1;
}



?>


<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<base href=".."/>
	<meta charset="UTF-8" />
	<meta name="description"  content="Home" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
	<title>Mathink</title>

	<script src="require/js/jquery.js"></script>
	<link rel="stylesheet" href="js/jqueryui/jquery-ui.css"/>
	<script src="require/js/jqueryui/jquery-ui.js"></script>
	<script src="js/hours.js"></script>
	<script src="jogos/functions/multiplicacao/multiplicacao.js"></script>
	<link href="require/boot/css/bootstrap.css" rel="stylesheet"/>
	<script src="require/boot/js/bootstrap.js"></script>
	<!-- Thanks Font Awesome --><link rel="stylesheet" href="require/fonts/css/font-awesome.css">
	<link rel="icon" href="require/img/logo/mathink.ico">
	<link rel="stylesheet" href="require/css/master.css">
	<script src="require/js/loginForm.js"></script>
	<link rel="stylesheet" href="require/css/newMaster.css" />
	<link rel="stylesheet" href="jogos/require/css/questionario.css">
	<script src="require/js/sketch.js"></script>
	<script src="jogos/sairPagina.js"></script>
	<script src="require/js/upTime.js"></script>
<script src="require/js/moment.js"></script>
</head >
<body>

	<?php

	require_once "../menu.php";
	?>
	</header>


	<script>
		var porcentoF = "<?php echo $porcento ?>";
		console.log(porcentoF);
		var levelF = "<?php echo $Preparelevel?>";
		console.log("a" + "<?php echo $Preparelevel?>");
		var pregame = preGame(levelF,porcentoF);
		var resPerg;
		var game =  thegame(levelF);
	</script>

	<div class="container base">

		<div class="row">
			<div class="container-fluid">
				<div class="col-md-3">
					<h4> <?php echo $nomeGame ?> </h4>
				</div>
				<div id="level" class="col-md-push-2 col-md-3">
					<!--O level é escrito na função writeLevel do soma.js-->
				</div>
				<div id="scoreP" class="col-md-push-2 col-md-3">
					<!--O Score é escrito na função writeScore do soma.js-->
				</div>
				<div id="vidaP" class="col-md-push-1 col-md-3">
					<!--As vidas é escrito na função writeVida do soma.js-->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="container">
				<div class="col-md-12">
					<div id="progressbar"></div>
				</div>
			</div>
		</div>
		<div class="row"><br></div>

			<!--****************** DESIGN JOGANDO *******************-->
			<div id="jogando" >
				<div class="row">
					<div class="col-md-push-1 col-md-2 col-sm-2 col-xs-2 col-lg-3">
						<br /><br /><br /><img style="width: 72%" class="img-responsive" src="require/img/lampi/Lampi.png" alt="Lampi" />
					</div>
					<div class="col-xs-9 col-sm-9 col-md-10 col-lg-8">
						<div id="pergunta">
							<h4>A questão a ser exibida neste balão será inserida dentro de um p. Este texto é apenas um teste para ver o quão grande o balão pode ser.</h4>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="resposta">
						<!--PERGUNTAS ESCRITA NO writeResposta-->
						</div>
						<div class="row botoesAcoes">
							<div class="col-md-offset-3 col-md-5 col-sm-6 col-xs-6">
								<button onclick="desistir();">Desistir&nbsp <i class="fa fa-times"></i></button>
							</div>

							<div class="col-md-4 col-sm-6 col-xs-6">
								<button onclick="errou(true);">Pular&nbsp <i class="fa fa-arrow-right"></i></button>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-sm-6 hidden-xs">
						<div class="row">
							<div class="col-md-12">
								<div class="teste">
									<canvas class="lousa" id="tools_sketch"></canvas>									
									<div style="position:absolute;font-size: 3em">
									<a style="color:white" href="#tools_sketch" data-tool="marker">Desenhar</a>
									<a style="color:white" href="#tools_sketch" data-tool="eraser">Apagar</a>
									</div>
									<script>
										$(function() {
											$('#tools_sketch').sketch({defaultColor: "#FFF"});
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--****************** FIM DESIGN JOGANDO *******************-->





	</div>



	</div>

	</body>

</body>
</html>



