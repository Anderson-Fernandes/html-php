/**
 * Created by Anderson on 11/04/2016.
 */

window.onbeforeunload = function (e) {
    e = e || window.event;

    // For IE and Firefox prior to version 4
    if (e) {
        e.returnValue = 'Você perderar todo o seu progresso!';
    }

    // For Safari
    return 'Você perderar todo o seu progresso!';
};