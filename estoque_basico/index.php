<?php


require_once 'require/class/conDB.class.php';

$crud = new CRUD;


$produtoInfo = $crud->select('*','produto','',array());


//cd_produto , sg_status, ds_produto, qt_est_Min,max
$produto = [];
if($produtoInfo->rowCount() > 0){
	foreach ($produtoInfo as $dds) {
		$stats['codigo'] = $dds['cd_produto']; 
		$stats['nome'] = $dds['nm_produto']; 
		$stats['descricao'] = $dds['ds_produto'];
		$stats['status'] = $dds['sg_status'];
		$stats['preco'] =$dds['vl_produto'];
		$stats['qtdMax'] = $dds['qt_estoque_maximo'];
		$stats['qtdMin'] = $dds['qt_estoque_minimo'];



		$logEstoque = $crud->select('vl_unitario,qt_estoque','estoque','WHERE cd_produto=?',array($dds['cd_produto']));

		foreach ($logEstoque as $dds) {
			
			$stats['estoque'] =$dds['qt_estoque'];
		}

		
		array_push($produto, $stats);
	}
}



?>


<html>
<head>
	<meta charset="utf-8" />
	<title>Banco Simples</title>
	<link rel="stylesheet" href="require/boostrap/css/bootstrap.css" />
	<script type="text/javascript" src="require/jquery/jquery.js"></script>
	<style type="text/css" media="screen">
		#popup{
			position: fixed;
			width: 50%;
			height: 45%;
			background-color: #50587D;
			border: 0.2em solid #c7c7c7 ;
			top:25%;
			left: 25%;
			padding-top: 1%;
			display: none;
			z-index: 2;
		}

		#bg{
			width: 100%;
			height: 100%;
			background-color: #c7c7c7;
			opacity: 0.7;
			position: fixed;
			display: none;
		}

		#popup button{
			width: 100%;
		}


	tr{
		cursor:pointer;
	}

	</style>


	<script>
	$(document).ready(function(){
		$('.produto').click(function(){
			var id = $(this).attr('id');
			$('#popup').fadeIn();	
			$('#bg').fadeIn();
			$('#adicionarEstoque').attr('onclick','adcionarEstoque('+id+',true)');
			$('#removerEstoque').attr('onclick','adcionarEstoque('+id+',false)');
			$('#alterarStatus').attr('onclick','alterarStatus('+id+')');
			$('#editarProduto').attr('onclick','editarProduto('+id+')');
			$('#excluirProduto').attr('onclick','excluirProduto('+id+')');
			$('#historico').attr('onclick','historico('+id+')');
		});

		$('#bg').click(function(){
			$('#bg').fadeOut();
			$('#popup').fadeOut();
		})
	});

	function historico(produto){
		location.href="historico.php?id="+produto;
	}


	function excluirProduto(produto){
		if(confirm("Tem certeza que deseja excluir esse produto?")){
			$.ajax({
				url: "excluir.php",
				type: "POST",
				data: {id:produto},
				success: function (post) {
					if (post) {
						console.log(post);
						alert("Ocorreu algum erro, desculpe");
					}else{
					    location.href = 'index.php';
					}
				}
   			});
		}
	}

	function editarProduto(produto){
		location.href = 'produto.php?id=' + produto;
	}

	function alterarStatus(produto){
		if(confirm("Tem certeza que deseja alterar o status desse produto?")){
			$.ajax({
				url: "status.php",
				type: "POST",
				data: {id:produto},
				success: function (post) {
					if (post) {
						console.log(post);
						alert("Ocorreu algum erro, desculpe");
					}else{
					    location.href = 'index.php';
					}
				}
   			});
		}
	}

	function adcionarEstoque(produto, entrada){


		var qtd = prompt("Quantas unidades será adicionada?");
		if(isNaN(parseInt(qtd))){
				alert("Deve digitar apenas com numeros");
		}else{
			 $.ajax({
				url: "estoque.php",
				type: "POST",
				data: {adcionando:entrada,valor:qtd,id:produto},
				success: function (post) {
					if (post) {
						console.log(post);
						alert("Ocorreu algum erro, desculpe");
					}else{
					    location.href = 'index.php';
					}
				}
   			});
		}
	}

		
	</script>
</head>
<body>

<div id="popup" class="container-fluid center-block">
	<div class="col-md-12">
		<button id="adicionarEstoque" class="btn btn-default">Adicionar Estoque</button>
		<br />
		<button id="removerEstoque" class="btn btn-default">Remover Estoque</button>
		<br />
		<button id="alterarStatus" class="btn btn-default">Altera Status</button>
		<br />
		<button id="editarProduto" class="btn btn-default">Editar Produto</button>
		<br />
		<button id="excluirProduto" class="btn btn-default">Excluir Produto</button>
		<br />
		<button id="historico" class="btn btn-default">Historico</button>
	</div>
</div>

<div id='bg'></div>


	<table class="table table-hover">
		<thead>
			<th>Codigo</th>
			<th>Nome</th>
			<th>Descricao</th>
			<th>Valor</th>
		<!--	<th>Status</th> -->
			<th>Quantidade Minima</th>
			<th>Quantidade Maxima</th>
			<th>Em Estoque</th>
		</thead>
		<tbody>
				<?php if(count($produto) >0): 
					for($i = 0; $i < count($produto);$i++): ?>
				<tr id=<?= $produto[$i]['codigo'] ?> class='produto  <?php echo ($produto[$i]['status'] == 'i')?"danger":""?>'	>
					<td> <?php echo $produto[$i]["codigo"];?> </td>
					<td> <?php echo $produto[$i]['nome'];?></td>
					<td> <?php echo $produto[$i]["descricao"];?> </td>
					<td> <?php echo $produto[$i]["preco"];?> </td>
					<!--<td> <?php echo $produto[$i]["status"];?> </td>-->
					<td> <?php echo $produto[$i]["qtdMin"];?> </td>
					<td> <?php echo $produto[$i]["qtdMax"];?> </td>
					<td> <?php echo $produto[$i]['estoque'];?></td>
				</tr>
				<?php endfor;
				else:
					echo '<tr>';
					 echo "<td colspan='8' style='text-align:center'> Sem cadastro no momento, cadastre! </td>";
					 echo '</tr>'; 
				endif;?>
		</tbody>
	</table>
	<a href="produto.php">Novo Produto</a>
</body>
</html>
