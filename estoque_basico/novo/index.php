<?php

$host = "localhost";
$pass = "";
$user = "root";
$db = "estoque_basico";

mysql_connect($host,$user,$pass);
mysql_select_db($db);

$sql = "SELECT * FROM produto";
$info = mysql_query($sql);

$produtos = array();


while ($row = mysql_fetch_assoc($info)) {
    $dados['cd'] = $row["cd_produto"];
    $dados['sigla'] = $row['sg_status'];
    $dados['desc'] = $row['ds_produto'];
    $dados['min'] = $row['qt_estoque_minimo'];
    $dados['max'] = $row['qt_estoque_maximo'];
    $dados['nome'] = $row['nm_produto'];
    $dados['valor'] = $row['vl_produto'];

    $sql2 = "SELECT vl_unitario,qt_estoque FROM estoque WHERE cd_produto =" . $dados['cd'];
    $info2 = mysql_query($sql2);

    while($row2 = mysql_fetch_assoc($info2)){
        $dados['estoque'] = $row2['qt_estoque'];
    }

    array_push($produtos,$dados);

}



?>

<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>Banco Simples</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Nome</th>
                <th>Descricao</th>
                <th>Valor</th>
                <th>Quantidade Minima</th>
                <th>Quantidade Maxima</th>
                <th>Em Estoque</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
        <?php if(count($produtos)>0):
                for($i = 0 ; $i <  count($produtos); $i++):
                    ?>
            <tr>
                <td> <?= $produtos[$i]['nome'] ?></td>
                <td> <?= $produtos[$i]['desc'] ?></td>
                <td> <?= $produtos[$i]['valor'] ?></td>
                <td> <?= $produtos[$i]['min'] ?></td>
                <td> <?= $produtos[$i]['max'] ?></td>
                <td> <?= $produtos[$i]['estoque'] ?></td>
                <td>
                    <a href="editar.php?id=<?=$produtos[$i]['cd']?>">Editar</a>
                    <a href="alterar_estoque.php?id=<?=$produtos[$i]['cd']?>">Alterar estoque</a>
                    <a href="excluir.php?id=<?=$produtos[$i]['cd']?>">Excluir</a>
                </td>
            </tr>

        <?php endfor;
                endif;
        ?>

        </tbody>
    </table>

    <a href="novo.php">Adicionar Produto</a>

</body>
</html>
