<?php

if(isset($_GET['id'])){
	require_once 'require/class/conDB.class.php';
	$id = $_GET['id'];
	$crud = new CRUD;

	$logEntrada = $crud->select('qt_entrada,dt_entrada','entrada_produto','WHERE cd_produto=? ORDER BY dt_entrada DESC',array($id));

	$logSaida = $crud->select('qt_saida,dt_saida','saida_produto','WHERE cd_produto=? ORDER BY dt_saida DESC',array($id));

	if($logSaida->rowCount() > 0 || $logEntrada->rowCount() > 0){

		$historico = array();

		if($logSaida->rowCount() >0){

			foreach ($logSaida as $dds) {

				$dados['tipo'] = 'Saida';
				$dados['qtd'] = $dds['qt_saida'] *-1;
				$dados['data'] = $dds['dt_saida'];
				array_push($historico, $dados);
			}
		}

		if($logEntrada->rowCount() >0){
			
			foreach ($logEntrada as $dds) {
				$dados['tipo'] = 'Entrada';
				$dados['qtd'] = $dds['qt_entrada'];
				$dados['data'] = $dds['dt_entrada'];
				array_push($historico, $dados);
			}
		}



	}else{
		echo '<script>alert("Produto sem Historico");
					location.href="index.php" </script>';
}

}else{
	echo '<script>alert("Produto não encontrado");
					location.href="index.php" </script>';
}



?>


<html>
<head>
	<meta charset="utf-8" />
	<title>Banco Simples</title>
	<link rel="stylesheet" href="require/boostrap/css/bootstrap.css" />
	<script type="text/javascript" src="require/jquery/jquery.js"></script>
</head>

<body>
	<table class="table table-hover">
		<thead>
			<th>Tipo de Transição</th>
			<th>Quantidade</th>
			<th>Data</th>
		</thead>
		<tbody>
			<?php for($i = 0; $i < count($historico);$i++): ?>
				<tr>
					<td><?= $historico[$i]['tipo'];?></td>
					<td><?=$historico[$i]['qtd'];?></td>
					<td><?=$historico[$i]['data'];?></td>
				</tr>
			<?php endfor;?>
		</tbody>
	</table>
	<a href="index.php">Voltar</a>
</body>
</html>
