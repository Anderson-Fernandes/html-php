<?php
$id = 'isNovo';
$nome = '';
$desc = '';
$min = '';
$max = '';
$val = '';
if(isset($_GET['id'])){

	require_once 'require/class/conDB.class.php';

	$crud = new CRUD;

	$id = $_GET['id'];

	$log = $crud->select('*','produto','WHERE cd_produto=?',array($id));

	if($log->rowCount() > 0){
		foreach ($log as $dds) {
			$nome = $dds['nm_produto'];
			$desc = $dds['ds_produto'];
			$min = $dds['qt_estoque_minimo'];
			$max = $dds['qt_estoque_maximo'];
			$val = $dds['vl_produto'];
		}
	}else{
		echo "<script>alert('produto não encontrado');</script>";
	}
}


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="require/boostrap/css/bootstrap.css" />
	<script type="text/javascript" src="require/jquery/jquery.js"></script>
	<script>
		function salvar(isNovo){
			var nome = $('#nome');
			var preco = $('#preco');
			if(isNaN(parseInt(preco.val()))){
				alert("Deve digitar apenas numero no preço");
				preco.focus();
				return;
			}

			var desc = $('#descricao');

			var qtdMin = $('#minimo');
			var qtdMax = $('#maximo');

			if(isNaN(parseInt(qtdMin.val())) || isNaN(parseInt(qtdMax.val()))){
				alert("Deve digitar apenas numero nos estoques");
				return;
			}
		
			
			if(notNull(nome)){
				if(notNull(preco)){
					if(notNull(desc)){
						if(estoque(qtdMin, qtdMax)){
							    $.ajax({
							        url: "salvar.php",
							        type: "POST",
							        data: {novo:isNovo, nome:nome.val(), preco:preco.val(),desc:desc.val(),min:qtdMin.val(),max:qtdMax.val()},
							        success: function (post) {
							            if (post) {
							                console.log(post);
							                alert("Ocorreu algum erro, desculpe");
							            }else{
							                location.href = 'index.php';
							            }
							        }
   								 });
						}
					}
				}
			}

		}

		function notNull(htmlIdElement){
			var value = htmlIdElement.val();

			if(value.length < 1){
				alert("Escreva no campo " + htmlIdElement.attr('id'));
				htmlIdElement.focus();
				return false;
			}

			return true;
		}

		function estoque(htmlIdElement, htmlIdElement2){
			if(notNull(htmlIdElement)){
				if(notNull(htmlIdElement2)){
					if(htmlIdElement.val() >0){
						if(htmlIdElement.val() >= htmlIdElement2.val()){
						alert("O estoque minimo tem que ser menor ou igual ao estoque maximo");
						return false;
						}else{
							return true;
						}
					}else{
						alert("O estoque minimo tem que ser maior que 0");
						return false;
					}
					
				}
			}
		}
	</script>

</head>
<body>
	<div class="containter-fluid">
		<form action="javascript:void(0)">
		  	<div class="form-group">
			    <label for="nome">Nome</label>
			    <input type="text" class="form-control" id="nome" value='<?= $nome ?>'>
		  	</div>

		  	<div class="form-group">
			    <label for="preco">Preço</label>
			    <input type="text" class="form-control" id="preco" value="<?= $val ?>">
		  	</div>

		  	<div class="form-group">
			    <label for="descricao">Descrição</label>
			    <textarea class="form-control" name="descricao" id="descricao"><?= $desc ?></textarea>
		  	</div>
			<div class="form-inline">
			    <label>Estoque: </label>
			    <input type="text" class="form-control" id="minimo" placeholder="Minimo" value="<?=$min?>">
			    <input type="text" class="form-control" id="maximo" placeholder="Maximo" value="<?=$max?>">
		 	</div>
		 	<br />
		 	<button onclick="location.href='index.php';" type="submit" class="btn btn-default">Cancelar</button>
		 	<button onclick="salvar('<?= $id ?>');" type="submit" class="btn btn-default">Salvar</button>
		</form>
	</div>
</body>
</html>