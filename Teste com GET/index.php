 
 <!DOCTYPE html>
 <html lang="pt-br">
	<head>
	<meta charset="UTF-8">
<title>AULA 2 - PHP - Variáveis</title>
</head>
<body>
	
	<!-- Para concatenar no PHP utilize pontos
	As variáveis são declaradas utilizando cifrão $
	Utilize ponto e vírgula ao finla da instrução
	Para imprimir na tela pode-se utilizar echo ou prin:
	
		o echo é um pouco mais rápido que o print, mas não retorna um valor,
		e também não precisa concatenar variáveis que podem ser separadas por vírgulas.
		O echo não é uma função e sim um construtor do PHP.
		
		O print é utilizado também para impressão no PHP,
		é considerado uma função de impressão de valores.
		A função print retorna um valor.
	-->

<?php
$num1=200;
$num2=5;

echo "<hr>";
echo "Usando 2 variáveis. <br />";
echo "Os número são: ",$num1," e ",$num2,"<br /:";

echo "br />Soma: ".($num1+$num2)."<br />";
echo "Subtração: ".($num1-$num2)."<br />";
echo "Multiplicação: ".($num1*$num2)."<br />";
echo "Divisão: ".$num1/$num2."<br />";

echo "<hr>";

print "Usando 3 variáveis"."<br />";
$resultado=$num1+$num2;
print "Os número são: ".$num1." e ".$num2."<br />";

print "<br />Soma: ".$resultado."<br />";
$resultado=$num1-$num2;
print "Subtração: ".$resultado."<br />";
$resultado=$num1*$num2;
print "Multiplicação: ".$resultado."<br />";
$resultado=$num1/$num2;

print "<hr>";

print "<br /> Observe o que tem na variável resultado: ".$resultado;
$resultado = print "<br /> Observe o conteúdo da variável resultado após o print atribuido a ela ";
print $resultado;
?>

</body>
</html>