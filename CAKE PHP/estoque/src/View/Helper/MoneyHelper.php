<?php
/**
 * Created by PhpStorm.
 * User: PC-CASA
 * Date: 14/05/2016
 * Time: 21:24
 */

namespace App\View\Helper;

use Cake\View\Helper;

class MoneyHelper extends Helper{
    public function format($number){
        return "R$ ". number_format($number,2,",",".");
    }
}
?>