<?php
/**
 * Created by PhpStorm.
 * User: Anderson Luiz
 * Date: 14/05/2016
 * Time: 15:56
 */

namespace App\Controller;

use Cake\ORM\TableRegistry;

class ProdutosController extends AppController{
    public function index(){

        $produtosTable = TableRegistry::get('Produtos');

        $produtos = $produtosTable->find('all');
        $this->set('produtos',$produtos);
    }

    public function novo(){
        $produtosTable = TableRegistry::get('Produtos');
        $produto = $produtosTable->newEntity();
        $this->set('produto',$produto);
    }

    public function salva(){
        $produtosTable = TableRegistry::get('Produtos');
        $produto = $produtosTable->newEntity($this->request->data());
        if($produtosTable->save($produto)){
            $msg = "Produto salvo com sucesso";
            $this->Flash->set($msg,['element'=>'success']);
        }else{
            $msg = "Erro ao salvar o produto";
            $this->Flash->set($msg,['element'=>'error']);
        }

        $this->redirect('/produtos/index');
    }

    public function editar($id){
        $produtosTable = TableRegistry::get('Produtos');
        $produto = $produtosTable->get($id);

        $this->set('produto',$produto);
        $this->render('novo');
    }

    public function apagar($id){
        $produtosTable = TableRegistry::get('Produtos');
        $produto = $produtosTable->get($id);

        if($produtosTable->delete($produto)){
            $msg = "Produto removido com sucesso";
        }else{
            $msg = "Erro ao deletar o produto";
        }

        $this->Flash->set($msg,['element' => 'error']);
        $this->redirect('/Produtos/index');

    }
}

?>