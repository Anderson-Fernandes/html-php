<?php
/**
 * Created by PhpStorm.
 * User: PC-CASA
 * Date: 14/05/2016
 * Time: 22:58
 */

namespace App\Controller;
use Cake\ORM\TableRegistry;

class UsersController extends AppController{

    public function adcionar(){

        $userTable = TableRegistry::get('users');

        $user = $userTable->newEntity();

        $this->set('user',$user);
    }

    public function salvar(){
        $userTable = TableRegistry::get('users');
        $user = $userTable->newEntity($this->request->data());

        if($userTable->save($user)){
            $msg = "Usuario Cadastrado com Sucesso";
        }else{
            $msg = "Erro ao cadastrar o usuario";
        }

        $this->Flash->set($msg);

        $this->redirect("/users/adcionar");
    }

    public function login(){
        $userTable = TableRegistry::get('users');
        $user = $userTable->newEntity();

        $this->set('user',$user);
    }
}

?>