<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<style type="text/css">
    
html{
    background-color: #f1f1f1;
}

#conteudo{
    margin-top: 20%;
    border: 3px solid #c6c6FF;
    background-color: #e1e1FF;
    width: 30%;
    text-align: left;
    padding: 64px;
   /* padding-bottom: 64px;
    padding-left: 54px;*/
}

.rei{
    border-color: #c9c9FF;
    background-color: #a9a9FF;
    border-radius: 8px;
}

.rei:hover{
    background-color: #9999FF;
}

.form{
    padding-left: 4px;
}

.form:focus{
    border: 2px solid #000000;
}


</style>
<title>Jogo da Forca</title>
</head>

<body>
<center>
<div id="conteudo">
<?php
    include 'config.php';
    include 'functions.php';
    if (isset($_POST['reiniciar'])) unset($_SESSION['answer']);
    if (!isset($_SESSION['answer']))
    {
        $_SESSION['tentativas'] = 0;
        $answer = setContEspaco($LISTA_PALAVRAS);
        $_SESSION['answer'] = $answer;
        $_SESSION['espaco'] = ocultarPalavras($answer);
        echo 'Tentativas restantes: '.($MAX_TENTATIVAS - $_SESSION['tentativas']).'<br>';
    }else
    {
        if (isset ($_POST['userResposta']))
        {
            $userResposta = $_POST['userResposta'];
            $_SESSION['espaco'] = checkPalavra(strtolower($userResposta), $_SESSION['espaco'], $_SESSION['answer']);
            checkGameOver($MAX_TENTATIVAS,$_SESSION['tentativas'], $_SESSION['answer'],$_SESSION['espaco']);
        }
        $_SESSION['tentativas'] = $_SESSION['tentativas'] + 1;
        echo 'Tentativas restantes: '.($MAX_TENTATIVAS - $_SESSION['tentativas'])."<br>";
    }
    $espaco = $_SESSION['espaco'];
    foreach ($espaco as $char) echo $char."  ";
?>
<script type="application/javascript">
    function validaResposta()
    {
    var x=document.forms["inputForm"]["userResposta"].value;
    if (x=="" || x==" ")
      {
          alert("Por favor, digite uma letra.");
          return false;
      }
    if (!isNaN(x))
    {
        alert("Por favor, digite uma letra.");
        return false;
    }
}
</script>
<form name = "inputForm" action = "" method = "post">
Entre com uma letra: <input class="form" name = "userResposta" type = "text" size="1" maxlength="1"  />
<input class="rei" type = "submit"  value = "Tentar" onclick="return validaResposta()"/>
<input class="rei" type = "submit" name = "reiniciar" value = "Reiniciar"/>
</form>
</div>
</center>
</body>
</html>