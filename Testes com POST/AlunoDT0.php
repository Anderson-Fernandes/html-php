<?php

/*
* Objeto Aluno para trabalhar com os dados
*/
class AlunoDT0
{
	private $nome="";
	private $email="";
	public function setNome($pNome)
	{
		$this->nome = $pNome;
	}
	public function getNome()
	{
		return $this ->nome;
	}
	public function setEmail ($pEmail)
	{
		$this->email = $pEmail;
	}
	public function getEmail()
	{
		return $this->email;
	}

	public function mostrarDados()
	{
		echo "O nome do Aluno é: ". $this->nome."<br>";
		echo "O E-mail do Aluno é: ". $this->email;
	}
}