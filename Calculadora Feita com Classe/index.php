<DOCTYPE html>
<html lang="pt-br">
	<head>
	<meta charset="UTF-8">
	<title>Calculadora</title>
	</head>
	<!-- usando viewport vw para deixar fontes responsivas -->
	<style type="text/css">
	#caixa
	{
		font-family: "Comic Sans MS", cursive;
		margin-top: 5%;
		margin-left: 25%;
		padding-left: 5%;
		padding-top: 10%;
		padding-bottom: 10%;
		padding-right: 5%;
		width:40%;
		background-color: #00A;
		color: #ffffff;
		font-size: 2vw;
		border-radius: 2vw;
	}
	#caixa_botao
	{
		padding-left: 15%;
		background-color: #00A;
		font-size: 2vw;
	}
	input
	{
		font-family: "Comic Sans MS", cursive;
		font-size: 1.5vw;
	}
	</style>
	<body>
	<!-- quando queremos que o formulario envie as variaveis para a mesma pagina, não especificamos o atributo action, do comando form -->
	<div id="caixa">
		<form name="form1" method="post">
			Numero 1: <input type="text" name="numero1"><br>
			Numero 2: <input type="text" name="numero2"><br>
			<p>
			<div id="caixa_botao">
			<input type="submit" style="height:5vw;width:5vw" name="operacao" value="+">
			<input type="submit" style="height:5vw;width:5vw" name="operacao" value="-">
			<input type="submit" style="height:5vw;width:5vw" name="operacao" value="/">
			<input type="submit" style="height:5vw;width:5vw" name="operacao" value="*">
			<input type="submit" style="height:5vw;width:5vw" name="operacao" value="%">
			</div></p>
		</form>
		<?php
		//verifica se existe a variavel numero1
		if(isset($_POST['numero1']))
		{
		//cria novas variaveis com base nas variaveis de navegador
		$num1=$_POST['numero1'];
		$num2=$_POST['numero2'];
		$operacao = $_POST['operacao'];
		if ($operacao=='+')
		{
			//verifica se as variaveis possuem valor diferente de vazio
		if(!empty($num1)&&!empty($num2))
		{
			//inclui o arquivo que possui a estrutura da classe para permitir a criação do objeto
			require_once("class.soma.php");
			//cria o objeto:
			$objSoma = new Soma();
			// a partir de agora a variavel $objOperacao é um objeto que possui a estrutura que existe na classe operação.
			//vamos chamar o método adição e obter o resultado da soma e mstrar o resultado na tela
			print "O resultado da soma é: ".$objSoma->adicao($num1,$num2)."<br>";
			//como já nao precisamos do objeto $objOperacao, vamos destruí-lo
			Unset($objSoma);
		}
		else
		{
			echo "Preencha os campos solicitados.";
		}
		}
		else if ($operacao=='-')
		{
			if(!empty($num1)&&!empty($num2))
		{
			require_once("class.subtracao.php");
			$objSubtração = new Subtracao();
			print "O resultado da subtração é: ".$objSubtração->subtração($num1,$num2)."<br>";
			Unset($objSubtração);
		}
		else
		{
			echo "Preencha os campos solicitados.";
		}
		}
		else if ($operacao=='/')
		{
			if(!empty($num1)&&!empty($num2))
		{
			require_once("class.divisao.php");
			$objDivisão = new Divisao();
			print "O resultado da divisão é: ".$objDivisão->Divisão($num1,$num2)."<br>";
			Unset($objDivisão);
		}
		else
		{
			echo "Preencha os campos solicitados.";
		}
		}
		else if ($operacao=='*')
		{
			if(!empty($num1)&&!empty($num2))
		{
			require_once("class.multiplicacao.php");
			$objMultiplicação = new Multiplicacao();
			print "O resultado da multiplicação é: ".$objMultiplicação->Multiplicação($num1,$num2)."<br>";
			Unset($objMultiplicação);
		}
		else
		{
			echo "Preencha os campos solicitados.";
		}
		}
		else if ($operacao=='%')
		{
			if(!empty($num1)&&!empty($num2))
		{
			require_once("class.mod.php");
			$objMod = new Mod();
			print "O resultado do mod é: ".$objMod->modal($num1,$num2)."<br>";
			Unset($objMod);
		}
		else
		{
			echo "Preencha os campos solicitados.";
		}
		}
		}
		?>
	</div>
	</body>
</html>