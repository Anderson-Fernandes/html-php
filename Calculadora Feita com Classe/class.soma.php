<?php

//declara a classe
//o objeto que for criado por meio desta classe assumira a visibilidade e nome da classe
class Soma
{
//declara um atributo
private $resultado;
// declara o metodo adição. o metodo receve dois numeros para executar o calculo e retornar o resultado
Public function adicao($numero1, $numero2)
{
	$this->resultado=$numero1+$numero2;
	return $this->resultado;
}
}
?>